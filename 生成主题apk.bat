@echo off

:: replace config
xcopy /R /s /e /Y "%~dp0"ThemeReplacement\configs\*	"%~dp0"AppLocker\locktheme\configs\

:: replace drawable
rmdir /s /q "%~dp0"AppLocker\locktheme\src\main\assets
xcopy /R /s /e /Y "%~dp0"ThemeReplacement\assets\*	"%~dp0"AppLocker\locktheme\src\release\main\
xcopy /R /s /e /Y "%~dp0"ThemeReplacement\res\drawable-mdpi\* 	"%~dp0"AppLocker\locktheme\src\release\res\drawable-mdpi\
xcopy /R /s /e /Y "%~dp0"ThemeReplacement\res\drawable-hdpi\* 	"%~dp0"AppLocker\locktheme\src\release\res\drawable-hdpi\
xcopy /R /s /e /Y "%~dp0"ThemeReplacement\res\drawable-xhdpi\* 	"%~dp0"AppLocker\locktheme\src\release\res\drawable-xhdpi\
xcopy /R /s /e /Y "%~dp0"ThemeReplacement\res\drawable-xxhdpi\* 	"%~dp0"AppLocker\locktheme\src\release\res\drawable-xxhdpi\
xcopy /R /s /e /Y "%~dp0"ThemeReplacement\res\drawable-xxxhdpi\* 	"%~dp0"AppLocker\locktheme\src\release\res\drawable-xxxhdpi\

:: start build
cd %~dp0AppLocker\locktheme
call gradle assembleRelease

:: copy output files
xcopy /R /s /e /Y "%~dp0"AppLocker\locktheme\build\outputs\apk\*release.apk	"%~dp0"FinalThemeApks\

::pause
