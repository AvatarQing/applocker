package com.funnyapps.privacy.applock.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by AvatarQing on 2016/8/22.
 */
public class ThemeUtils {

    public static SharedPreferences getPres(Context context) {
        return context.getSharedPreferences(Const.PrefFileName.THEME, Context.MODE_PRIVATE);
    }

    public static void applyTheme(Context context, String themePackageName) {
        getPres(context)
                .edit()
                .putString(Const.PrefKey.APPLIED_THEME_PACKAGE_NAME, themePackageName)
                .apply();
    }

    public static String getAppliedThemePackageName(Context context) {
        return getPres(context).getString(Const.PrefKey.APPLIED_THEME_PACKAGE_NAME, "");
    }

}