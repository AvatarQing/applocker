package com.funnyapps.privacy.applock.ui.bindingadpaters;

import android.databinding.BindingAdapter;
import android.support.v4.view.ViewPager;

import com.funnyapps.privacy.applock.ui.bindingadpaters.command.ReplyCommand;


/**
 * Created by lq on 2017/1/11.
 */

public class ViewPagerBindingAdapters {

    @BindingAdapter(value = {"onPageScrolledCommand", "onPageSelectedCommand", "onPageScrollStateChangedCommand"}, requireAll = false)
    public static void addOnPageChangeListener(ViewPager viewPager, final ReplyCommand<OnPageScrolledDataWrapper> onPageScrolledCommand,
                                               final ReplyCommand<Integer> onPageSelectedCommand, final ReplyCommand<Integer> onPageScrollStateChangedCommand) {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private OnPageScrolledDataWrapper onPageScrolledDataWrapper = new OnPageScrolledDataWrapper();

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (onPageScrolledCommand != null) {
                    onPageScrolledDataWrapper.position = position;
                    onPageScrolledDataWrapper.positionOffset = positionOffset;
                    onPageScrolledDataWrapper.positionOffsetPixels = positionOffsetPixels;
                    onPageScrolledCommand.execute(onPageScrolledDataWrapper);
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (onPageSelectedCommand != null) {
                    onPageSelectedCommand.execute(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (onPageScrollStateChangedCommand != null) {
                    onPageScrollStateChangedCommand.execute(state);
                }
            }
        });
    }

    public static class OnPageScrolledDataWrapper {
        public int position;
        public float positionOffset;
        public int positionOffsetPixels;
    }
}
