package com.funnyapps.privacy.applock.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ui.adapter.GeneralFragmentPagerAdapter;
import com.funnyapps.privacy.applock.ui.fragment.LocalThemeListFragment;
import com.funnyapps.privacy.applock.ui.fragment.RemoteThemeListFragment;
import com.funnyapps.privacy.applock.ui.widget.PagerSlidingTabStrip;
import com.funnyapps.privacy.applock.utils.Const;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lq on 2016/8/22.
 */
public class ThemeListActivity extends BaseActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.view_pager)
    ViewPager view_pager;
    @BindView(R.id.sliding_tabs)
    PagerSlidingTabStrip sliding_tabs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_list);
        ButterKnife.bind(this);
        initViewPager();
        parseArguments(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        parseArguments(intent);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        $(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavUtils.navigateUpFromSameTask(ThemeListActivity.this);
            }
        });
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.THEME;
    }

    void initViewPager() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new RemoteThemeListFragment());
        fragmentList.add(new LocalThemeListFragment());

        String[] tabTitles = new String[]{
                getString(R.string.tab_remote_theme),
                getString(R.string.tab_local_theme),
        };

        GeneralFragmentPagerAdapter adapter = new GeneralFragmentPagerAdapter(getSupportFragmentManager(), fragmentList, Arrays.asList(tabTitles));
        view_pager.setAdapter(adapter);
        sliding_tabs.setViewPager(view_pager);

        title.setText(R.string.title_theme);
    }

    void parseArguments(Intent intent) {
        if (TextUtils.equals(intent.getAction(), Const.ACTIVITY_ACTION_LOCAL_THEME_LIST)) {
            view_pager.setCurrentItem(1);
        }
    }
}