package com.funnyapps.privacy.applock.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.funnyapps.privacy.applock.databinding.ItemAppBinding;
import com.funnyapps.privacy.applock.entity.AppInfo;
import com.funnyapps.privacy.applock.ui.viewmodel.GuideAppListItemViewModel;
import com.funnyapps.privacy.applock.ui.viewmodel.IAppListItemViewModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by AvatarQing on 2016/6/13.
 */
public class AppListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<AppInfo> mDataList = new ArrayList<>();
    GuideAppListItemViewModel mAppViewModel;

    public AppListAdapter() {
        mAppViewModel = new GuideAppListItemViewModel();
    }

    IAppListItemViewModel getAppListItemViewModel() {
        return mAppViewModel;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemAppBinding binding = ItemAppBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new AppViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder rawHolder, int position) {
        AppInfo appInfo = getAppInfo(position);
        AppViewHolder holder = (AppViewHolder) rawHolder;
        holder.binding.setAppInfo(appInfo);
        holder.binding.setViewModel(getAppListItemViewModel());
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    AppInfo getAppInfo(int position) {
        return mDataList.get(position);
    }

    public void setData(List<AppInfo> dataList) {
        mDataList.clear();
        if (dataList != null && !dataList.isEmpty()) {
            mDataList.addAll(dataList);
        }
        notifyDataSetChanged();
    }

    public Set<String> getSelectedApps() {
        List<AppInfo> data = mDataList;
        Set<String> appSet = new HashSet<>();
        for (AppInfo item : data) {
            if (item.isChecked()) {
                appSet.add(item.getPackageName());
            }
        }
        return appSet;
    }

    static class AppViewHolder extends RecyclerView.ViewHolder {
        ItemAppBinding binding;

        AppViewHolder(ItemAppBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}