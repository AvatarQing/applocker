package com.funnyapps.privacy.applock.service;

import android.app.Notification;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.funnyapps.privacy.applock.utils.Const;

/**
 * 给 API >= 18 的平台上用的灰色保活手段
 */
public class GrayService extends BaseService {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            notification = new Notification();
        } else {
            notification = new NotificationCompat.Builder(this, Const.NT_CHANNEL_MONITOR).build();
        }
        startForeground(DaemonService.DAEMON_NOTIFICATION_ID, notification);
        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}