package com.funnyapps.privacy.applock.ui.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.LocalTheme;
import com.funnyapps.privacy.applock.ui.widget.DynamicHeightImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by AvatarQing on 2016/8/21.
 */
public class LocalThemeListAdapter extends BaseAdapter implements View.OnClickListener {

    List<LocalTheme> mDataList = new ArrayList<>();
    View.OnClickListener mOnThemeClickListener;
    String mAppliedTheme;

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public LocalTheme getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_theme, parent, false);
            holder = new ViewHolder(convertView);
            holder.theme_preview.setOnClickListener(this);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LocalTheme item = getItem(position);
        holder.theme_preview.setTag(R.id.position, position);
        holder.theme_preview.setTag(R.id.data, item);

        holder.theme_name.setText(item.name);
        double heightRatio = 1.0 * item.preview.getIntrinsicHeight() / item.preview.getIntrinsicWidth();
        holder.theme_preview.setHeightRatio(heightRatio);
        holder.theme_preview.setImageDrawable(item.preview);

        boolean isApplied = TextUtils.equals(item.packageName, mAppliedTheme);
        holder.applied.setVisibility(isApplied ? View.VISIBLE : View.GONE);
        holder.preview_container.setForeground(isApplied ? parent.getResources().getDrawable(R.drawable.selected_theme_stroke) : null);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.theme_preview:
                if (mOnThemeClickListener != null) {
                    mOnThemeClickListener.onClick(v);
                }
                break;
        }
    }

    public void setAppliedTheme(String theme) {
        this.mAppliedTheme = theme;
    }

    public String getAppliedTheme() {
        return mAppliedTheme;
    }

    public void setOnThemeClickListener(View.OnClickListener listener) {
        this.mOnThemeClickListener = listener;
    }

    public void appendData(List<LocalTheme> list) {
        if (list != null && !list.isEmpty()) {
            this.mDataList.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clearData() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.theme_preview)
        DynamicHeightImageView theme_preview;
        @BindView(R.id.theme_name)
        TextView theme_name;
        @BindView(R.id.applied)
        View applied;
        @BindView(R.id.preview_container)
        FrameLayout preview_container;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}