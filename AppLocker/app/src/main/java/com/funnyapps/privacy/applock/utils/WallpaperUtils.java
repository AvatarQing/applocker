package com.funnyapps.privacy.applock.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.abc.common.entity.CloudResult;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.entity.WallPaper;
import com.funnyapps.privacy.applock.enums.WallpaperType;
import com.funnyapps.privacy.applock.event.WallpaperAppliedEvent;
import com.funnyapps.privacy.applock.web.WebClient;

import java.util.concurrent.Callable;

import de.greenrobot.event.EventBus;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by AvatarQing on 2016/8/22.
 */
public class WallpaperUtils {

    private static SharedPreferences getPres(Context context) {
        return context.getSharedPreferences(Const.PrefFileName.WALLPAPER, Context.MODE_PRIVATE);
    }

    public static void applyWallpaper(Context context, String imageUrl, WallpaperType type) {
        getPres(context)
                .edit()
                .putString(Const.PrefKey.APPLIED_WALLPAPER_URL, imageUrl)
                .putInt(Const.PrefKey.APPLIED_WALLPAPER_TYPE, type.ordinal())
                .apply();
        EventBus.getDefault().post(new WallpaperAppliedEvent(imageUrl, type));
    }

    public static WallpaperType getAppliedWallpaperType(Context context) {
        int type = getPres(context).getInt(Const.PrefKey.APPLIED_WALLPAPER_TYPE, -1);
        if (type == -1) {
            return null;
        } else {
            return WallpaperType.values()[type];
        }
    }

    public static String getAppliedWallpaperUrl(Context context) {
        return getPres(context).getString(Const.PrefKey.APPLIED_WALLPAPER_URL, "");
    }

    public static Observable<CloudResult<WallPaper>> getRemoteWallpapers() {
        Func1<CloudResult<WallPaper>, Boolean> filter = new Func1<CloudResult<WallPaper>, Boolean>() {
            @Override
            public Boolean call(CloudResult<WallPaper> result) {
                return result != null && result.results != null && !result.results.isEmpty();
            }
        };
        Observable<CloudResult<WallPaper>> diskCache = Observable
                .fromCallable(new Callable<CloudResult<WallPaper>>() {
                    @Override
                    public CloudResult<WallPaper> call() throws Exception {
                        return DataUtils.getCachedWallPapers();
                    }
                })
                .filter(filter)
                .doOnNext(new Action1<CloudResult<WallPaper>>() {
                    @Override
                    public void call(CloudResult<WallPaper> result) {
                        Debug.li(getLogTag(), "read diskCache:" + result.results.size());
                    }
                });
        Observable<CloudResult<WallPaper>> network = WebClient.getLeanCloudService()
                .loadWallPaperList()
                .subscribeOn(Schedulers.io())
                .filter(filter)
                .doOnNext(new Action1<CloudResult<WallPaper>>() {
                    @Override
                    public void call(CloudResult<WallPaper> data) {
                        Debug.li(getLogTag(), "read network data and cache it");
                        DataUtils.cacheWallPapers(data);
                    }
                });
        return Observable.concat(diskCache, network)
                .observeOn(Schedulers.computation())
                .takeFirst(new Func1<CloudResult<WallPaper>, Boolean>() {
                    @Override
                    public Boolean call(CloudResult<WallPaper> result) {
                        return true;
                    }
                });
    }

    private static String getLogTag() {
        return WallpaperUtils.class.getSimpleName();
    }
}