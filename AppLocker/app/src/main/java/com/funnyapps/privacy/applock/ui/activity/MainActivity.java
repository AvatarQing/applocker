package com.funnyapps.privacy.applock.ui.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.abc.common.entity.CloudResult;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ad.AppLovinAds;
import com.funnyapps.privacy.applock.entity.UpgradeConfig;
import com.funnyapps.privacy.applock.ui.adapter.GeneralFragmentPagerAdapter;
import com.funnyapps.privacy.applock.ui.fragment.ExitDialogFragment;
import com.funnyapps.privacy.applock.ui.fragment.MainAppListFragment;
import com.funnyapps.privacy.applock.ui.fragment.RemoteWallpaperListFragment;
import com.funnyapps.privacy.applock.ui.fragment.RequestDeviceAdminDialogFragment;
import com.funnyapps.privacy.applock.ui.fragment.UpgradeDialogFragment;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.DataUtils;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by AvatarQing on 2016/6/21.
 */
public class MainActivity extends BaseActivity {

    @BindView(R.id.tab_bar)
    SmartTabLayout tabBar;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    AlertDialog mRequestPermissionDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        PatternLockUtils.increaseAppLaunchTime(this);
        loadAd();
        initFragment();
        showUpgradeDialogIfNeed();
        showRequestDeviceAdminDialogIfNeed();
        registerUsageStatsListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.needPermissionForUsageStats(this)) {
            showRequestPermissionDialog();
        } else if (mRequestPermissionDialog != null) {
            mRequestPermissionDialog.dismiss();
            mRequestPermissionDialog = null;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onBackPressed() {
        showExitDialog();
    }

    void showExitDialog() {
        String tag = "ExitDialogFragment";
        if (getSupportFragmentManager().findFragmentByTag(tag) == null) {
            new ExitDialogFragment.Builder()
                    .setNativeAdView(AppLovinAds.get().getNewNativeAdView())
                    .create()
                    .show(getSupportFragmentManager(), tag);
        }
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.MAIN;
    }

    @OnClick(R.id.btn_settings)
    public void goToSettingsPage() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_theme)
    public void goToThemeListPage() {
        Intent intent = new Intent(this, ThemeListActivity.class);
        startActivity(intent);
    }

    private void loadAd() {
        AppLovinAds.get().loadNativeAd();
    }

    private void initFragment() {
        MainAppListFragment appListFragment = new MainAppListFragment();
        RemoteWallpaperListFragment wallpaperListFragment = new RemoteWallpaperListFragment();

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(appListFragment);
        fragmentList.add(wallpaperListFragment);

        List<String> tabTitleList = new ArrayList<>();
        tabTitleList.add(getString(R.string.tab_title_app_list));
        tabTitleList.add(getString(R.string.tab_title_wallpaper));

        GeneralFragmentPagerAdapter adapter = new GeneralFragmentPagerAdapter(getSupportFragmentManager(), fragmentList, tabTitleList);

        viewPager.setAdapter(adapter);
        tabBar.setViewPager(viewPager);
    }

    private void showRequestPermissionDialog() {
        if (mRequestPermissionDialog != null) {
            return;
        }
        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(R.string.lock_permission_title)
                .setMessage(R.string.lock_permission_msg)
                .setPositiveButton(R.string.pl_continue, null)
                .create();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mRequestPermissionDialog = null;
                if (Utils.needPermissionForUsageStats(MainActivity.this)) {
                    finish();
                }
            }
        });
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Utils.requestUsageAccessPermission(MainActivity.this);
                            }
                        });
            }
        });
        alertDialog.show();
        mRequestPermissionDialog = alertDialog;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    void registerUsageStatsListener() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }
        final AppOpsManager appOps = (AppOpsManager) getSystemService(APP_OPS_SERVICE);
        if (AppOpsManager.MODE_ALLOWED == appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, android.os.Process.myUid(), getPackageName())) {
            return;
        }
        appOps.startWatchingMode(AppOpsManager.OPSTR_GET_USAGE_STATS, getPackageName(), new AppOpsManager.OnOpChangedListener() {
            @Override
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            public void onOpChanged(String op, String packageName) {
                int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                        android.os.Process.myUid(), getPackageName());
                if (mode != AppOpsManager.MODE_ALLOWED) {
                    return;
                }
                appOps.stopWatchingMode(this);
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showRequestDeviceAdminDialogIfNeed() {
        boolean canShow = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canShow = false;
            AppOpsManager appOps = (AppOpsManager) getSystemService(APP_OPS_SERVICE);
            int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, android.os.Process.myUid(), getPackageName());
            if (mode == AppOpsManager.MODE_ALLOWED) {
                canShow = true;
            }
        }
        // 暂时禁用设备管理器权限申请
        canShow = false;
        if (canShow) {
            RequestDeviceAdminDialogFragment.showIfNeed(MainActivity.this);
        }
    }

    void showUpgradeDialogIfNeed() {
        Observable
                .fromCallable(new Callable<CloudResult<UpgradeConfig>>() {
                    @Override
                    public CloudResult<UpgradeConfig> call() throws Exception {
                        return DataUtils.getCacheUpgradeConfig();
                    }
                })
                .filter(new Func1<CloudResult<UpgradeConfig>, Boolean>() {
                    @Override
                    public Boolean call(CloudResult<UpgradeConfig> data) {
                        return data != null && data.results != null && !data.results.isEmpty();
                    }
                })
                .flatMap(new Func1<CloudResult<UpgradeConfig>, Observable<UpgradeConfig>>() {
                    @Override
                    public Observable<UpgradeConfig> call(CloudResult<UpgradeConfig> data) {
                        return Observable.from(data.results);
                    }
                })
                .filter(new Func1<UpgradeConfig, Boolean>() {
                    @Override
                    public Boolean call(UpgradeConfig config) {
                        return config.versionCode > Utils.getVersionCode(getApplicationContext());
                    }
                })
                .first()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UpgradeConfig>() {
                    @Override
                    public void call(UpgradeConfig config) {
                        Debug.li(getLogTag(), "弹出升级对话框");
                        showUpgradeDialog(config.message);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Debug.lw(getLogTag(), throwable.getMessage());
                        throwable.printStackTrace();
                    }
                });
    }

    void showUpgradeDialog(String msg) {
        String tag = "UpgradeDialogFragment";
        UpgradeDialogFragment dialogFragment = (UpgradeDialogFragment) getSupportFragmentManager().findFragmentByTag(tag);
        if (dialogFragment == null) {
            new UpgradeDialogFragment.Builder()
                    .setMessage(msg)
                    .create()
                    .show(getSupportFragmentManager(), tag);
        }
    }
}