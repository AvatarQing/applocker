package com.funnyapps.privacy.applock.ui.viewmodel;

import android.databinding.BindingAdapter;
import android.view.View;

import com.funnyapps.privacy.applock.entity.AppInfo;
import com.abc.common.tool.Debug;
import com.mixiaoxiao.smoothcompoundbutton.SmoothCompoundButton;
import com.mixiaoxiao.smoothcompoundbutton.SmoothSwitch;

/**
 * Created by AvatarQing on 2016/11/9.
 */

public class GuideAppListItemViewModel implements IAppListItemViewModel {

    @Override
    public void onItemClick(View itemView, AppInfo item) {
        Debug.li(getLogTag(), "onItemClick()" + "\n"
                + "getAppName:" + item.getAppName() + "\n"
                + "getPackageName:" + item.getPackageName() + "\n"
                + "isChecked:" + item.isChecked());

        item.setChecked(!item.isChecked());
    }

    @Override
    public void onCheckedChanged(SmoothCompoundButton checkbox, boolean isChecked, AppInfo item) {
        Debug.li(getLogTag(), "onCheckedChanged()" + "\n"
                + "getAppName:" + item.getAppName() + "\n"
                + "getPackageName:" + item.getPackageName() + "\n"
                + "isChecked:" + item.isChecked());

        item.setChecked(isChecked);
    }

    @BindingAdapter("pureChecked")
    public static void setChecked(SmoothSwitch smoothSwitch, boolean checked) {
        smoothSwitch.setChecked(checked, true, false);
    }

    String getLogTag() {
        return getClass().getSimpleName();
    }
}