/*
 * Copyright (c) 2015 Zhang Hai <Dreaming.in.Code.ZH@Gmail.com>
 * All Rights Reserved.
 */

package com.funnyapps.privacy.applock.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.PermissionHelper;
import com.haibison.android.lockpattern.widget.LockPatternView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 每次打开本应用时，需要进行手势验证的页面
 */
public class ConfirmPatternActivity extends BaseActivity
        implements LockPatternView.OnPatternListener {

    public static final int REQUEST_CODE_ANSWER_SECURITY_QUESTION = 111;
    public static final int REQUEST_CODE_SET_PATTERN = 112;
    private static final int CLEAR_PATTERN_DELAY_MILLI = 2000;

    @BindView(R.id.lock_pattern)
    LockPatternView mPatternView;
    @BindView(R.id.btn_forget_password)
    View btn_forget_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pattern);
        ButterKnife.bind(this);
        mPatternView.setInStealthMode(isStealthModeEnabled());
        mPatternView.setOnPatternListener(this);
        if (PatternLockUtils.hasSetSecurityQuestion(this)) {
            btn_forget_password.setVisibility(View.VISIBLE);
        } else {
            btn_forget_password.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_ANSWER_SECURITY_QUESTION: {
                if (resultCode == RESULT_OK) {
                    goToSetPatternPage();
                }
            }
            break;
            case REQUEST_CODE_SET_PATTERN: {
                if (resultCode == RESULT_OK && data != null) {
                    goToMainPage();
                    setResult(RESULT_OK);
                    finish();
                }
            }
            break;
            default:
                break;
        }
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.CONFIRM_PATTERN;
    }

    @Override
    public void onPatternStart() {

        removeClearPatternRunnable();

        // Set display mode to correct to ensure that pattern can be in stealth mode.
        mPatternView.setDisplayMode(LockPatternView.DisplayMode.Correct);
    }

    @Override
    public void onPatternCellAdded(List<LockPatternView.Cell> pattern) {
    }

    @Override
    public void onPatternDetected(List<LockPatternView.Cell> pattern) {
        if (isPatternCorrect(pattern)) {
            onConfirmed();
        } else {
            mPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
            postClearPatternRunnable();
        }
    }

    @Override
    public void onPatternCleared() {
        removeClearPatternRunnable();
    }

    protected boolean isStealthModeEnabled() {
        return PatternLockUtils.isStealthModeEnabled(this);
    }

    protected boolean isPatternCorrect(List<LockPatternView.Cell> pattern) {
        return PatternLockUtils.isPatternCorrect(pattern, this);
    }

    protected void onConfirmed() {
        setResult(RESULT_OK);
        finish();
    }

    protected void onCancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.btn_forget_password)
    protected void onForgotPassword() {
        Intent intent = new Intent(this, AnswerSecurityQuestionActivity.class);
        startActivityForResult(intent, REQUEST_CODE_ANSWER_SECURITY_QUESTION);
    }

    private void goToSetPatternPage() {
        Intent intent = new Intent(this, ChangePatternActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SET_PATTERN);
    }

    private void goToMainPage() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private final Runnable clearPatternRunnable = new Runnable() {
        public void run() {
            // clearPattern() resets display mode to DisplayMode.Correct.
            mPatternView.clearPattern();
        }
    };

    protected void removeClearPatternRunnable() {
        mPatternView.removeCallbacks(clearPatternRunnable);
    }

    protected void postClearPatternRunnable() {
        removeClearPatternRunnable();
        mPatternView.postDelayed(clearPatternRunnable, CLEAR_PATTERN_DELAY_MILLI);
    }

}