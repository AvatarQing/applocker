package com.funnyapps.privacy.applock.entity;

import android.databinding.ObservableBoolean;
import android.text.TextUtils;

/**
 * Created by lq on 2016/11/19.
 */

public class LocalWallPaper {

    public String imageFilePath;
    public ObservableBoolean isApplied = new ObservableBoolean(false);
    public ObservableBoolean isChecked = new ObservableBoolean(false);

    @Override
    public boolean equals(Object o) {
        if (o instanceof LocalWallPaper) {
            LocalWallPaper another = (LocalWallPaper) o;
            return TextUtils.equals(imageFilePath, another.imageFilePath);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "LocalWallPaper{" +
                "imageFilePath='" + imageFilePath + '\'' +
                ", isApplied=" + isApplied +
                ", isChecked=" + isChecked +
                '}';
    }
}