package com.funnyapps.privacy.applock.ui.bindingadpaters;

import android.databinding.BindingAdapter;

import com.funnyapps.privacy.applock.ui.bindingadpaters.command.TaskCommand;
import com.github.clans.fab.FloatingActionButton;

import rx.functions.Action1;

/**
 * Created by lq on 2017/1/9.
 */

public class FloatingActionButtonBindingAdapters {

    @BindingAdapter("showCommand")
    public static void show(final FloatingActionButton fab, TaskCommand<Boolean> showCommand) {
        showCommand.setTask1(new Action1<Boolean>() {
            @Override
            public void call(Boolean shown) {
                if (shown) {
                    fab.show(true);
                } else {
                    fab.hide(true);
                }
            }
        });
    }
}
