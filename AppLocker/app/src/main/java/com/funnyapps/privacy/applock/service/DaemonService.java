package com.funnyapps.privacy.applock.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ui.activity.LauncherActivity;
import com.funnyapps.privacy.applock.utils.Const;

/**
 * Created by AvatarQing on 2016/6/1.
 */
public class DaemonService extends BaseService {

    /**
     * 定时唤醒的时间间隔，5分钟
     */
    private final static int ALARM_INTERVAL = 5 * 60 * 1000;
    private final static int WAKE_REQUEST_CODE = 6666;
    public static final int DAEMON_NOTIFICATION_ID = 1;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        makeServiceToBeDaemon();
        return START_STICKY;
    }

    private void makeServiceToBeDaemon() {
        Debug.li(getLogTag(), "makeServiceToBeDaemon");
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
                startForeground(DAEMON_NOTIFICATION_ID, new Notification());
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                Intent innerIntent = new Intent(this, GrayService.class);
                startService(innerIntent);
                startForeground(DAEMON_NOTIFICATION_ID, new Notification());
            } else {
                Intent intent = new Intent(this, LauncherActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                Notification notification = new NotificationCompat.Builder(this, Const.NT_CHANNEL_MONITOR)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.app_description))
                        .setContentIntent(pendingIntent)
                        .build();
                startForeground(DAEMON_NOTIFICATION_ID, notification);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //发送唤醒广播来促使挂掉的UI进程重新启动起来
//        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        Intent alarmIntent = new Intent();
//        alarmIntent.setAction(WakeReceiver.ACTION_WAKE);
//        PendingIntent operation = PendingIntent.getBroadcast(this, WAKE_REQUEST_CODE, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), ALARM_INTERVAL, operation);
    }
}