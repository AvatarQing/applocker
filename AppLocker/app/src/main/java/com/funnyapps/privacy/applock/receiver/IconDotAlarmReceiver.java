package com.funnyapps.privacy.applock.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.funnyapps.privacy.applock.utils.BadgeUtil;
import com.funnyapps.privacy.applock.utils.IconDotAlarmUtils;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/10/18 </li>
 * </ul>
 */

public class IconDotAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        BadgeUtil.setBadgeCount(context, 1);
        IconDotAlarmUtils.setAlarm(context);
    }
}
