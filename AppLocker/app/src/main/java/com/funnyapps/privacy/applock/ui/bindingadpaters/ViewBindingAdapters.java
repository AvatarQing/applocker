package com.funnyapps.privacy.applock.ui.bindingadpaters;

import android.databinding.BindingAdapter;
import android.view.View;

import com.funnyapps.privacy.applock.ui.bindingadpaters.command.ReplyCommand;
import com.funnyapps.privacy.applock.ui.bindingadpaters.command.ResponseCommand;


/**
 * Created by lq on 2017/1/7.
 */

public class ViewBindingAdapters {

    @BindingAdapter({"clickCommand"})
    public static void clickCommand(View view, final ReplyCommand clickCommand) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickCommand != null) {
                    clickCommand.execute();
                }
            }
        });
    }

    @BindingAdapter({"longClickCommand"})
    public static void longClickCommand(View view, final ResponseCommand<Void, Boolean> clickCommand) {
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (clickCommand != null) {
                    return clickCommand.execute();
                }
                return false;
            }
        });
    }

}