package com.funnyapps.privacy.applock.analytics;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.analytics.StandardExceptionParser;

import java.util.Collection;

/**
 * @author LiuQing
 * @version 2017/3/20
 */

public class AppExceptionParser extends StandardExceptionParser {

    public AppExceptionParser(Context context) {
        super(context, null);
    }

    public AppExceptionParser(Context context, Collection<String> collection) {
        super(context, collection);
    }

    @Override
    protected String getDescription(Throwable throwable, StackTraceElement stackTraceElement, String threadName) {
        return "{" + threadName + "}" + Log.getStackTraceString(throwable);
    }

}