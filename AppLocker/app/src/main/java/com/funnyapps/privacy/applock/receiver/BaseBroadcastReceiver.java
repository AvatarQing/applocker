package com.funnyapps.privacy.applock.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.abc.common.tool.Debug;


/**
 * Created by AvatarQing on 2016/6/1.
 */
public class BaseBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Debug.li(getLogTag(), "context:" + context + "\n"
                + "intent:" + intent + "\n"
                + (intent != null ? "action:" + intent.getAction() + "\n" : "")
                + (intent != null ? "extras:" + intent.getExtras() + "\n" : "")
        );
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }
}