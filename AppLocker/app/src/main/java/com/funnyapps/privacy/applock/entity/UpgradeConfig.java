package com.funnyapps.privacy.applock.entity;

/**
 * Created by lq on 2016/11/21.
 */

public class UpgradeConfig extends BaseCloudItem {
    public int versionCode;
    public String message;

    @Override
    public String toString() {
        return "UpgradeConfig{" +
                "versionCode=" + versionCode +
                ", message='" + message + '\'' +
                '}';
    }
}