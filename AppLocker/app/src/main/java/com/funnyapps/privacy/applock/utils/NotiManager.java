package com.funnyapps.privacy.applock.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.NotificationCompat;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.UpgradeConfig;
import com.funnyapps.privacy.applock.ui.activity.LauncherActivity;

import static android.content.pm.PackageManager.GET_UNINSTALLED_PACKAGES;

/**
 * Created by lq on 2016/11/23.
 */

public class NotiManager {
    private static final int NOTIFICATION_ID_UPGRADE_APP = 1;
    private static final int NOTIFICATION_ID_ASK_LOCK_NEW_INSTALLED_APP = 2;
    private static final int REQUEST_CODE_OPEN_MAIN_PAGE_BY_UPGRADE_APP = 0x111;

    public static void sendUpgradeAppNotification(Context context, UpgradeConfig config) {
        Intent intent = new Intent(context, LauncherActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, REQUEST_CODE_OPEN_MAIN_PAGE_BY_UPGRADE_APP, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(context, Const.NT_CHANNEL_IMPROVE)
                .setSmallIcon(R.drawable.ic_launcher)
                .setTicker(context.getString(R.string.title_upgrade_app))
                .setContentTitle(context.getString(R.string.title_upgrade_app))
                .setContentText(config.message)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setAutoCancel(true)
                .build();
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(NOTIFICATION_ID_UPGRADE_APP, notification);
    }

    public static void cancelUpgradeNotification(Context context) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(NOTIFICATION_ID_UPGRADE_APP);
    }

    public static void sendAskLockNewInstalledAppNotification(Context context, String packageName) {
        if (packageName.equals(context.getPackageName())) {
            return;
        }
        String appName = packageName;
        int requestCode = packageName.hashCode();
        try {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo appInfo = pm.getApplicationInfo(packageName, GET_UNINSTALLED_PACKAGES);
            appName = pm.getApplicationLabel(appInfo).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(context, LauncherActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(context, Const.NT_CHANNEL_IMPROVE)
                .setSmallIcon(R.drawable.ic_launcher)
                .setTicker(context.getString(R.string.title_ask_lock_new_install_app, appName))
                .setContentTitle(context.getString(R.string.title_tips))
                .setContentText(context.getString(R.string.title_ask_lock_new_install_app, appName))
                .setContentIntent(pendingIntent)
                .setOngoing(false)
                .setAutoCancel(true)
                .build();
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(NOTIFICATION_ID_ASK_LOCK_NEW_INSTALLED_APP, notification);
    }
}