package com.funnyapps.privacy.applock.ui.bindingadpaters;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.ViewConfiguration;

import com.funnyapps.privacy.applock.ui.bindingadpaters.command.ReplyCommand;


/**
 * Created by lq on 2017/1/7.
 */

public class RecyclerViewBindAdapters {

    @BindingAdapter("itemDecoration")
    public static void addItemDecoration(RecyclerView recyclerView, RecyclerView.ItemDecoration itemDecoration) {
        if (itemDecoration == null) {
            return;
        }
        recyclerView.addItemDecoration(itemDecoration);
    }

    @BindingAdapter("itemDecorations")
    public static void addItemDecorations(RecyclerView recyclerView, RecyclerView.ItemDecoration[] itemDecorations) {
        if (itemDecorations == null) {
            return;
        }
        for (RecyclerView.ItemDecoration decoration : itemDecorations) {
            recyclerView.addItemDecoration(decoration);
        }
    }

    @BindingAdapter("scrollDirectionChangeCommand")
    public static void addScrollDirectionChangeCommand(RecyclerView recyclerView, final ReplyCommand<Boolean> scrollDirectionChangeCommand) {
        final ViewConfiguration vc = ViewConfiguration.get(recyclerView.getContext());
        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            boolean isPullDown = true;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (Math.abs(dy) < vc.getScaledTouchSlop()) {
                    return;
                }
                boolean isScrollFromUpToDown = dy < 0;
                if (isPullDown != isScrollFromUpToDown) {
                    isPullDown = isScrollFromUpToDown;
                    scrollDirectionChangeCommand.execute(isPullDown);
                }
            }
        };
        recyclerView.addOnScrollListener(onScrollListener);
    }
}