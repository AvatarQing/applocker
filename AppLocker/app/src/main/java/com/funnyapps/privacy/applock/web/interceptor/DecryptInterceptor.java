package com.funnyapps.privacy.applock.web.interceptor;

import android.text.TextUtils;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.MCrypt;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by AvatarQing on 2016/8/21.
 */
public class DecryptInterceptor implements Interceptor {

    MCrypt mCrypt = new MCrypt();

    public DecryptInterceptor() {
        this.mCrypt = new MCrypt();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        String bodyString = response.body().string();
        if (TextUtils.isEmpty(bodyString)) {
            return response;
        }
        String decrypted = bodyString;
        try {
            decrypted = new String(mCrypt.decrypt(bodyString));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Debug.li(getLogTag(), "Original Response Text : \n" + bodyString + "\n"
                + "-----------------------\n"
                + "Decrypted Response Text : \n" +
                decrypted);
        return response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), decrypted))
                .build();
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }
}
