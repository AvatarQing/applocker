package com.funnyapps.privacy.applock.utils;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ViewConfiguration;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Cancellable;

public class Utils {

    public static boolean isJsonString(String text) {
        if (!TextUtils.isEmpty(text)) {
            try {
                if (text.startsWith("{")) {
                    new JSONObject(text);
                    return true;
                }
                if (text.startsWith("[")) {
                    new JSONArray(text);
                    return true;
                }
            } catch (JSONException e) {
            }
        }
        return false;
    }

    public static String getPackageName(Context context) {
        String retPackString = "";
        if (null != context) {
            retPackString = context.getPackageName();
        }
        return retPackString;
    }

    public static final int getVersionCode(Context context) {
        int verCode = 0;
        try {
            PackageInfo appInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            verCode = appInfo.versionCode;
        } catch (Exception e) {
        }
        return verCode;
    }

    public static final String getVersionName(Context context) {
        String versionName = null;
        try {
            PackageInfo appInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            versionName = appInfo.versionName;
        } catch (Exception e) {
        }
        return versionName;
    }

    /**
     * 获取语言
     */
    public static String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    /**
     * 获取国家
     */
    public static String getCountry() {
        return Locale.getDefault().getCountry();
    }

    public static boolean isChinese() {
        String language = getLanguage();
        return TextUtils.isEmpty(language) && language.contains("zh");
    }

    public static boolean isSelf(Context context, String packageName) {
        return context.getPackageName().equals(packageName);
    }

    public boolean isLauncher(Context context, String packageName) {
        // get Launcher App Package Names
        List<String> names = new ArrayList<String>();
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        List<ResolveInfo> resolveInfo = packageManager.queryIntentActivities(
                intent, PackageManager.MATCH_DEFAULT_ONLY);
        Collections.sort(resolveInfo, new ResolveInfo.DisplayNameComparator(
                context.getPackageManager()));
        for (ResolveInfo ri : resolveInfo) {
            if (!context.getPackageName().equals(ri.activityInfo.packageName)) {
                names.add(ri.activityInfo.packageName);
            }
        }
        return names.contains(packageName);
    }

    public static String getTopAppPackageNameCompat(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return getTopAppPackageNameBeforeLollipop(context);
        } else {
            return getTopAppPackageNameAfterLollipop(context);
        }
    }

    public static String getTopAppPackageNameBeforeLollipop(Context context) {
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> runningTasks = am.getRunningTasks(1);
            if (runningTasks != null && !runningTasks.isEmpty()) {
                return runningTasks.get(0).topActivity.getPackageName();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return "";
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static String getTopAppPackageNameAfterLollipop(Context context) {
        UsageStatsManager usm = (UsageStatsManager) context.getSystemService("usagestats");
        long time = System.currentTimeMillis();
        List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 10000, time);
        if (appList != null && !appList.isEmpty()) {
            SortedMap<Long, UsageStats> sortedMap = new TreeMap<>();
            for (UsageStats usageStats : appList) {
                sortedMap.put(usageStats.getLastTimeUsed(), usageStats);
            }
            if (!sortedMap.isEmpty()) {
                return sortedMap.get(sortedMap.lastKey()).getPackageName();
            }
        }
        return "";
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static boolean needPermissionForUsageStats(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return false;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName);
            return (mode != AppOpsManager.MODE_ALLOWED);
        } catch (Exception e) {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void requestUsageAccessPermission(Context context) {
        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        context.startActivity(intent);
    }

    public static String formatMinuteAndSeconds(final long intervalInMills) {
        final long min = TimeUnit.MILLISECONDS.toMinutes(intervalInMills);
        final long sec = TimeUnit.MILLISECONDS.toSeconds(intervalInMills - TimeUnit.MINUTES.toMillis(min));
        return String.format(Locale.CHINA, "%02d:%02d", min, sec);
    }

    /**
     * 用第三方程序打开指定链接
     */
    public static void openLink(Context context, String link) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(link));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // 判断是否是GooglePlay的链接
        if (link.startsWith(Const.GOOGLE_PLAY_PREFIX_HTTP)
                || link.startsWith(Const.GOOGLE_PLAY_PREFIX_HTTPS)
                || link.startsWith(Const.GOOGLE_PLAY_PREFIX_MARKET)) {
            // 如果安装了GooglePlay
            if (isAppInstall(context, Const.GOOGLE_PLAY_PACKAGE_NAME)) {
                // 就用GooglePlay打开链接
                intent.setPackage(Const.GOOGLE_PLAY_PACKAGE_NAME);
            }
        }

        try {
            context.startActivity(intent);
        } catch (Exception e) {
            // 做一个异常捕获，防止没有第三方程序可以打开这个链接
            e.printStackTrace();
        }
    }

    /**
     * 判断应用是否已经安装
     */
    public static boolean isAppInstall(Context context, String packageName) {
        boolean installed = false;
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, 0);
            if (packageInfo != null) {
                installed = true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return installed;
    }

    public static void installApk(Context context, File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW)
                .setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive")
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 判断网络是否已经连接
     */
    public static final boolean isNetworkConnected(Context context) {
        boolean result = false;

        int ansPermission = context
                .checkCallingOrSelfPermission(android.Manifest.permission.ACCESS_NETWORK_STATE);
        int internetPermission = context
                .checkCallingOrSelfPermission(android.Manifest.permission.INTERNET);
        if (ansPermission == PackageManager.PERMISSION_GRANTED
                && internetPermission == PackageManager.PERMISSION_GRANTED) {
            if (context != null) {
                ConnectivityManager cm = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                if (networkInfo != null) {
                    int type = networkInfo.getType();
                    switch (type) {
                        case ConnectivityManager.TYPE_MOBILE:
                        case ConnectivityManager.TYPE_WIFI:
                        case ConnectivityManager.TYPE_WIMAX:
                            if (networkInfo.isAvailable()
                                    && networkInfo.isConnected()) {
                                result = true;
                            }
                            break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * 分享一段文字
     *
     * @param context 上下文环境，启动Activity用
     * @param subject 分享文案的主题
     * @param text    分享的文字内容
     */
    public static void shareText(Context context, String subject, String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            intent = Intent.createChooser(intent, subject);
            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 分享图片
     *
     * @param context 上下文环境
     * @param uri     图片地址
     */
    public static void shareImage(Context context, Uri uri, String subject) {
        if (uri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            try {
                context.startActivity(Intent.createChooser(shareIntent, subject));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 调用第三方邮件程序发送文本内容的邮件
     *
     * @param context   上下文环境，用于启动Activity
     * @param receivers 邮件接收者，可以是多个
     * @param subject   邮件主题
     * @param content   邮件的文本内容
     */
    public static void sendTextEmail(Context context, String[] receivers,
                                     String subject, String content) {
        // 将反馈内容发送邮件给作者
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        // 设置邮件收件人
        intent.putExtra(Intent.EXTRA_EMAIL, receivers);
        // 设置邮件标题
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        // 设置邮件内容
        intent.putExtra(Intent.EXTRA_TEXT, content);
        // 调用系统的邮件系统
        intent = Intent.createChooser(intent, subject);
        try {
            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        } catch (Exception e) {
            // 如果设备没有安装可以发送邮件的程序，就会出错，所以手动捕获以下
            e.printStackTrace();
        }
    }

    public static int calculateMemoryCacheSize(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        boolean largeHeap = (context.getApplicationInfo().flags & ApplicationInfo.FLAG_LARGE_HEAP) != 0;
        int memoryClass = am.getMemoryClass();
        if (largeHeap && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            memoryClass = am.getLargeMemoryClass();
        }
        // Target ~15% of the available heap.
        return 1024 * 1024 * memoryClass / 7;
    }

    public static void loadImage(ImageView imageView, String imageUrl) {
        ImageLoader.getInstance().displayImage(imageUrl, imageView);
    }

    public static void loadImageFromFile(ImageView imageView, String filePath) {
        ImageLoader.getInstance().displayImage(Uri.fromFile(new File(filePath)).toString(), imageView);
    }

    public static Bitmap loadImageSync(String imageUrl) {
        return ImageLoader.getInstance().loadImageSync(imageUrl);
    }

    public static Bitmap loadImageSyncFromFile(String filePath) {
        return ImageLoader.getInstance().loadImageSync(Uri.fromFile(new File(filePath)).toString());
    }

    public static Observable<Boolean> observeRecyclerViewScrollDirection(final RecyclerView recyclerView) {
        return Observable.create(new Action1<Emitter<Boolean>>() {
            @Override
            public void call(final Emitter<Boolean> emitter) {
                final ViewConfiguration vc = ViewConfiguration.get(recyclerView.getContext());
                final RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
                    boolean isPullDown = true;

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        if (Math.abs(dy) < vc.getScaledTouchSlop()) {
                            return;
                        }
                        boolean isScrollFromUpToDown = dy < 0;
                        if (isPullDown != isScrollFromUpToDown) {
                            isPullDown = isScrollFromUpToDown;
                            emitter.onNext(isPullDown);
                        }
                    }
                };
                recyclerView.addOnScrollListener(onScrollListener);
                emitter.setCancellation(new Cancellable() {
                    @Override
                    public void cancel() throws Exception {
                        recyclerView.removeOnScrollListener(onScrollListener);
                    }
                });
            }
        }, Emitter.BackpressureMode.LATEST);
    }
}