package com.funnyapps.privacy.applock.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.funnyapps.privacy.applock.service.MonitorService;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;

public class LauncherActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MonitorService.start(this);
        loadUpgradeInfo();
        if (PatternLockUtils.hasPattern(this)) {
            PatternLockUtils.confirmPatternIfHas(this);
        } else {
            goToSetNewPatternPage();
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (PatternLockUtils.checkConfirmPatternResult(requestCode, resultCode)) {
            Debug.li(getLogTag(), "通过密码验证，进入主页");
            goToMainPage();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
        finish();
    }

    private void goToMainPage() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void goToSetNewPatternPage() {
        Intent intent = new Intent(this, GuideActivity.class);
        startActivity(intent);
    }

    private void loadUpgradeInfo() {
        MonitorService.requestFetchUpgradeInfoIfNeed(this);
    }
}