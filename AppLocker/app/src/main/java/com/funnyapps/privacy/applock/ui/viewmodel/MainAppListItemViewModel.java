package com.funnyapps.privacy.applock.ui.viewmodel;

import android.content.Context;
import android.view.View;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.AppInfo;
import com.funnyapps.privacy.applock.service.MonitorService;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.ToastUtils;
import com.mixiaoxiao.smoothcompoundbutton.SmoothCompoundButton;

/**
 * Created by AvatarQing on 2016/11/9.
 */

public class MainAppListItemViewModel extends GuideAppListItemViewModel {

    @Override
    public void onItemClick(View itemView, AppInfo item) {
        super.onItemClick(itemView, item);
        lockApp(itemView.getContext(), item, item.isChecked());
    }

    @Override
    public void onCheckedChanged(SmoothCompoundButton checkbox, boolean isChecked, AppInfo item) {
        super.onCheckedChanged(checkbox, isChecked, item);
        lockApp(checkbox.getContext(), item, isChecked);
    }

    void lockApp(Context context, AppInfo item, boolean lock) {
        String themePackageName = item.getPackageName();
        PatternLockUtils.lockApp(context, themePackageName, lock);
        String text = lock
                ? context.getString(R.string.xx_locked, item.getAppName())
                : context.getString(R.string.xx_unlocked, item.getAppName());
        ToastUtils.showToast(text);
        MonitorService.requestRefreshReadLockedApps(context);

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.LOCK_APP,
                Const.Analytic.Action.APP,
                themePackageName
        );
    }

}