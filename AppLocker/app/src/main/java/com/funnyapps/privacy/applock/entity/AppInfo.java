package com.funnyapps.privacy.applock.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.drawable.Drawable;

import com.funnyapps.privacy.applock.BR;

/**
 * Created by AvatarQing on 2016/11/8.
 */

public class AppInfo extends BaseObservable {
    private String appName;
    private String packageName;
    private Drawable appIcon;
    private boolean isChecked = false;

    @Bindable
    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
        notifyPropertyChanged(BR.appName);
    }

    @Bindable
    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
        notifyPropertyChanged(BR.appIcon);
    }

    @Bindable
    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
        notifyPropertyChanged(BR.checked);
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}