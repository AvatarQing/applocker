package com.funnyapps.privacy.applock.ui.activity;

import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.abc.common.entity.CloudResult;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.WallPaper;
import com.funnyapps.privacy.applock.utils.Const;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.ToastUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.utils.WallpaperUtils;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by lq on 2016/12/6.
 */

public class SwitchLauncherWallpaperActivity extends BaseActivity {

    private boolean isLoadingWallPaper = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_launcher_wallpaper);
        switchLaunchWallpaper();
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.SWITCH_DESKTOP_WALLPAPER;
    }

    private void switchLaunchWallpaper() {
        if (isLoadingWallPaper) {
            ToastUtils.showToast(R.string.toast_switching_wallpaper);
            return;
        }
        WallpaperUtils.getRemoteWallpapers()
                .zipWith(Observable.timer(3, TimeUnit.SECONDS), new Func2<CloudResult<WallPaper>, Long, CloudResult<WallPaper>>() {
                    @Override
                    public CloudResult<WallPaper> call(CloudResult<WallPaper> wallPaperCloudResult, Long aLong) {
                        return wallPaperCloudResult;
                    }
                })
                .switchMap(new Func1<CloudResult<WallPaper>, Observable<String>>() {
                    @Override
                    public Observable<String> call(CloudResult<WallPaper> result) {
                        int randomIndex = new Random().nextInt(result.results.size());
                        String imageUrl = result.results.get(randomIndex).imageUrl;
                        Debug.li(getLogTag(), "Set wallpaper " + imageUrl);
                        return Observable.just(imageUrl);
                    }
                })
                .observeOn(Schedulers.io())
                .map(new Func1<String, Bitmap>() {
                    @Override
                    public Bitmap call(String wallPaperUrl) {
                        return Utils.loadImageSync(wallPaperUrl);
                    }
                })
                .filter(new Func1<Bitmap, Boolean>() {
                    @Override
                    public Boolean call(Bitmap bitmap) {
                        return bitmap != null;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        isLoadingWallPaper = true;
                        ToastUtils.showToast(R.string.toast_switching_wallpaper);
                    }
                })
                .doAfterTerminate(new Action0() {
                    @Override
                    public void call() {
                        isLoadingWallPaper = false;
                        finish();
                    }
                })
                .subscribe(new Subscriber<Bitmap>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Debug.lw(getLogTag(), e.toString());
                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
                        try {
                            wallpaperManager.setBitmap(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}