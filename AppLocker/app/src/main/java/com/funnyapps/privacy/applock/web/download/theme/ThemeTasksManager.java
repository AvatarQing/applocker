package com.funnyapps.privacy.applock.web.download.theme;

import android.util.SparseArray;

import com.funnyapps.privacy.applock.ui.adapter.viewholder.TaskItemViewHolder;
import com.funnyapps.privacy.applock.ui.fragment.RemoteThemeListFragment;
import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadConnectListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;

import java.lang.ref.WeakReference;

/**
 * Created by AvatarQing on 2016/8/31.
 */
public class ThemeTasksManager {
    private final static class HolderClass {
        private final static ThemeTasksManager INSTANCE = new ThemeTasksManager();
    }

    public static ThemeTasksManager getImpl() {
        return HolderClass.INSTANCE;
    }

    private ThemeTasksManager() {
    }

    private SparseArray<BaseDownloadTask> taskSparseArray = new SparseArray<>();

    public void addTaskForViewHolder(final BaseDownloadTask task) {
        taskSparseArray.put(task.getId(), task);
    }

    public void removeTaskForViewHolder(final int id) {
        taskSparseArray.remove(id);
    }

    public void updateViewHolder(final int id, final TaskItemViewHolder holder) {
        final BaseDownloadTask task = taskSparseArray.get(id);
        if (task == null) {
            return;
        }
        task.setTag(holder);
    }

    public void releaseTask() {
        taskSparseArray.clear();
    }

    private FileDownloadConnectListener listener;

    private void registerServiceConnectionListener(final WeakReference<RemoteThemeListFragment> fragmentWeakReference) {
        if (listener != null) {
            FileDownloader.getImpl().removeServiceConnectListener(listener);
        }
        listener = new FileDownloadConnectListener() {
            @Override
            public void connected() {
                if (fragmentWeakReference == null
                        || fragmentWeakReference.get() == null) {
                    return;
                }

                fragmentWeakReference.get().postNotifyDataChanged();
            }

            @Override
            public void disconnected() {
                if (fragmentWeakReference == null
                        || fragmentWeakReference.get() == null) {
                    return;
                }

                fragmentWeakReference.get().postNotifyDataChanged();
            }
        };
        FileDownloader.getImpl().addServiceConnectListener(listener);
    }

    private void unregisterServiceConnectionListener() {
        FileDownloader.getImpl().removeServiceConnectListener(listener);
        listener = null;
    }

    public void onCreate(final WeakReference<RemoteThemeListFragment> activityWeakReference) {
        if (!FileDownloader.getImpl().isServiceConnected()) {
            FileDownloader.getImpl().bindService();
            registerServiceConnectionListener(activityWeakReference);
        }
    }

    public void onDestroy() {
        unregisterServiceConnectionListener();
        releaseTask();
    }

    public boolean isReady() {
        return FileDownloader.getImpl().isServiceConnected();
    }

    /**
     * @param status Download Status
     * @return has already downloaded
     * @see FileDownloadStatus
     */
    public boolean isDownloaded(final int status) {
        return status == FileDownloadStatus.completed;
    }

    public int getStatus(final int id, String path) {
        return FileDownloader.getImpl().getStatus(id, path);
    }

    public long getTotal(final int id) {
        return FileDownloader.getImpl().getTotal(id);
    }

    public long getSoFar(final int id) {
        return FileDownloader.getImpl().getSoFar(id);
    }

}
