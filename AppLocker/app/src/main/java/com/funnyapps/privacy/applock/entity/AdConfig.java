/*
 * Copyright (c) 2018 Huami Inc. All Rights Reserved.
 */

package com.funnyapps.privacy.applock.entity;

import com.abc.common.entity.BaseCloudItem;

/**
 * @author AvatarQing
 */

public class AdConfig extends BaseCloudItem {

    /**
     * 广告id
     */
    public String adId;
    /**
     * 广告位
     */
    public String adPlace;
    /**
     * 广告平台，如admob、facebook、applovin
     */
    public String platform;
    /**
     * 是否启用广告
     */
    public boolean enable;
    /**
     * 每天展示的次数
     */
    public int frequency;
    /**
     * 每天多次展示时，相邻两次展示的时间间隔，单位毫秒。
     */
    public long interval;

    @Override
    public String toString() {
        return "AdConfig{" +
                "adId='" + adId + '\'' +
                ", adPlace='" + adPlace + '\'' +
                ", platform='" + platform + '\'' +
                ", enable=" + enable +
                ", frequency=" + frequency +
                ", interval=" + interval +
                '}';
    }

}