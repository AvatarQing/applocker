package com.funnyapps.privacy.applock.ad;

import com.abc.common.tool.Debug;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;

import java.util.Locale;

/**
 * @author AvatarQing
 */
public class LogNativePrecacheListener implements AppLovinNativeAdPrecacheListener {
    @Override
    public void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd) {
        Debug.ld(AppLovinAds.TAG, "Native ad precached images");
    }

    @Override
    public void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd) {
        Debug.ld(AppLovinAds.TAG, "Native ad precached videos");
    }

    @Override
    public void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int errorCode) {
        Debug.lw(AppLovinAds.TAG, String.format(Locale.CHINA, "Native ad failed to precache images with error code %d", errorCode));
    }

    @Override
    public void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int errorCode) {
        Debug.lw(AppLovinAds.TAG, String.format(Locale.CHINA, "Native ad failed to precache videos with error code %d", errorCode));
    }
}
