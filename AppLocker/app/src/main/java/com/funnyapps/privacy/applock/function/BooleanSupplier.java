package com.funnyapps.privacy.applock.function;

/**
 * @author AvatarQing
 */
@FunctionalInterface
public interface BooleanSupplier {

    /**
     * Gets a result.
     *
     * @return a result
     */
    boolean getAsBoolean();
}
