package com.funnyapps.privacy.applock.ui.bindingadpaters;

import android.databinding.BindingAdapter;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.funnyapps.privacy.applock.utils.Utils;


/**
 * Created by lq on 2017/1/6.
 */

public class ImageViewBindingAdapters {

    @BindingAdapter(value = {"uri", "placeholderImageRes"}, requireAll = false)
    public static void loadImage(final ImageView imageView, String uri, @DrawableRes int placeholderImageRes) {
//        if (placeholderImageRes == 0) {
//            placeholderImageRes = R.drawable.placeholder;
//        }
        Utils.loadImage(imageView, uri);
    }

    @BindingAdapter(value = {"imageFilePath"}, requireAll = false)
    public static void loadImageFile(final ImageView imageView, String imageFilePath) {
        Utils.loadImageFromFile(imageView, imageFilePath);
    }

}
