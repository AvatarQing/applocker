package com.funnyapps.privacy.applock.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by AvatarQing on 2016/8/21.
 */
public class LocalTheme {
    public Drawable preview;
    public String name;
    public String packageName;
}