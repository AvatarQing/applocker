package com.funnyapps.privacy.applock.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.funnyapps.privacy.applock.MyApplication;

/**
 * Created by AvatarQing on 2016/7/6.
 */
public class ToastUtils {

    private static Toast sLastToast;

    public static void showToast(@StringRes int info) {
        Context appContext = MyApplication.getInstance();
        showToast(appContext.getString(info));
    }

    public static void showToast(String info) {
        Context appContext = MyApplication.getInstance();
        Toast toast = sLastToast;
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(appContext, info, Toast.LENGTH_LONG);
        toast.show();
        sLastToast = toast;
    }

    public static void hideToast() {
        if (sLastToast != null) {
            sLastToast.cancel();
        }
    }
}