package com.funnyapps.privacy.applock.web.interceptor;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.Utils;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Created by lq on 2016/8/20.
 */
public class LogInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        long startTime = System.nanoTime();
        String requestLog = String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers());
        if (request.method().compareToIgnoreCase("post") == 0) {
            requestLog = "\n" + requestLog + "\n" + bodyToString(request);
        }
        Debug.li(getLogTag(), "OkHttp Request" + "\n" + requestLog);

        Response response = chain.proceed(chain.request());

        long endTime = System.nanoTime();
        String bodyString = response.body().string();
        String responseLog = String.format(Locale.CHINA, "Received response for %s in %.1fms%n%s",
                response.request().url(), (endTime - startTime) / 1e6d, response.headers());
        Debug.li(getLogTag(), responseLog);
        if (Utils.isJsonString(bodyString)) {
            Debug.json(getLogTag(), bodyString);
        } else {
            Debug.li(getLogTag(), "OkHttp Response String -> \n" + bodyString);
        }
        return response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), bodyString))
                .build();
    }

    public static String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }

}