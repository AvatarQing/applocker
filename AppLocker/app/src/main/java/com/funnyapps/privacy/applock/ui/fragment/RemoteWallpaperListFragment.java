package com.funnyapps.privacy.applock.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abc.common.entity.CloudResult;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.MyApplication;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ad.AppLovinAds;
import com.funnyapps.privacy.applock.entity.WallPaper;
import com.funnyapps.privacy.applock.enums.WallpaperType;
import com.funnyapps.privacy.applock.event.ShowInterstitialAdAfterSetWallpaperEvent;
import com.funnyapps.privacy.applock.event.WallpaperAppliedEvent;
import com.funnyapps.privacy.applock.ui.activity.LocalWallpaperListActivity;
import com.funnyapps.privacy.applock.ui.adapter.RemoteWallpaperListAdapter;
import com.funnyapps.privacy.applock.ui.decoration.SpacingDecoration;
import com.funnyapps.privacy.applock.ui.viewmodel.RemoteWallpaperViewModel;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PermissionHelper;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.utils.WallpaperUtils;
import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.trello.rxlifecycle.android.FragmentEvent;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import rx.Observable;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by lq on 2016/11/28.
 */

public class RemoteWallpaperListFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.loading_progress)
    View loading_progress;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    RemoteWallpaperListAdapter mAdapter;
    RemoteWallpaperViewModel mViewModel;
    PermissionHelper mPermissionHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mPermissionHelper = new PermissionHelper(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_remote_wallpaper_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadWallpapers();
        loadAd();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Tracker tracker = MyApplication.getInstance().getDefaultTracker();
            tracker.setScreenName(Const.Analytic.ScreenName.REMOTE_WALLPAPER_LIST);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    public void onEventMainThread(ShowInterstitialAdAfterSetWallpaperEvent event) {
        if (event.wallpaperType == WallpaperType.REMOTE) {
            AppLovinAds.get().showInterstitialAd();
        }
    }

    public void onEventMainThread(WallpaperAppliedEvent event) {
        mViewModel.clearAppliedWallpaper();
        if (event.type == WallpaperType.REMOTE) {
            mViewModel.refreshAppliedWallpaper(getActivity(), mAdapter.getData());
        }
    }

    private void loadAd() {
        AppLovinAds.get().loadInterstitialAd();
        AppLovinAds.get().loadNativeAd();
        AppLovinAds.get().getNativeAdLoadResultLiveData().observe(this, loaded -> {
            if (loaded != null && loaded) {
                View adView = AppLovinAds.get().getNewNativeAdView();
                if (adView != null) {
                    mAdapter.setNativeAdView(adView);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void initViews() {
        // 设置列表
        mViewModel = new RemoteWallpaperViewModel();
        mAdapter = new RemoteWallpaperListAdapter(mViewModel);
        recycler_view.setAdapter(mAdapter);

        final int columnCount = 2;
        GridLayoutManager lm = new GridLayoutManager(getContext(), columnCount);
        lm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 && AppLovinAds.get().isNativeAdLoaded()) {
                    return columnCount;
                }
                return 1;
            }
        });
        recycler_view.setLayoutManager(lm);

        int itemSpacing = getResources().getDimensionPixelOffset(R.dimen.item_spacing);
        recycler_view.addItemDecoration(new SpacingDecoration(itemSpacing, itemSpacing, true));

        Utils.observeRecyclerViewScrollDirection(recycler_view)
                .compose(this.<Boolean>bindUntilEvent(FragmentEvent.DESTROY))
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        fab.show(false);
                    }
                })
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean isPullDown) {
                        if (isPullDown) {
                            fab.show(true);
                        } else {
                            fab.hide(true);
                        }
                    }
                });
    }

    private void loadWallpapers() {
        final String appliedWallpaperUrl = WallpaperUtils.getAppliedWallpaperUrl(getContext());
        WallpaperUtils.getRemoteWallpapers()
                .compose(this.<CloudResult<WallPaper>>bindUntilEvent(FragmentEvent.DESTROY))
                .switchMap(new Func1<CloudResult<WallPaper>, Observable<List<WallPaper>>>() {
                    @Override
                    public Observable<List<WallPaper>> call(CloudResult<WallPaper> result) {
                        for (WallPaper wallPaper : result.results) {
                            if (TextUtils.equals(wallPaper.imageUrl, appliedWallpaperUrl)) {
                                wallPaper.isApplied.set(true);
                            }
                        }
                        return Observable.just(result.results);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        showLoadingView(true);
                    }
                })
                .toSingle()
                .subscribe(new SingleSubscriber<List<WallPaper>>() {
                    @Override
                    public void onSuccess(List<WallPaper> data) {
                        // 暂时没做分页
                        if (data != null && !data.isEmpty()) {
                            showLoadingView(false);
                            mAdapter.appendData(data);
                        } else {
                            showEmptyView();
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        showEmptyView();
                        error.printStackTrace();
                        Debug.lw(getLogTag(), error.getMessage());
                    }
                });
    }

    private void showLoadingView(boolean shown) {
        recycler_view.setVisibility(!shown ? View.VISIBLE : View.GONE);
        loading_progress.setVisibility(shown ? View.VISIBLE : View.GONE);
    }

    private void showEmptyView() {
        recycler_view.setVisibility(View.GONE);
        loading_progress.setVisibility(View.GONE);
        empty_text.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.fab)
    void goToLocalWallpaperPage() {
        if (!mPermissionHelper.hasGrantedPermissions()) {
            mPermissionHelper.showRequestPermissions();
            return;
        }
        Intent intent = new Intent(getActivity(), LocalWallpaperListActivity.class);
        startActivity(intent);
    }

}