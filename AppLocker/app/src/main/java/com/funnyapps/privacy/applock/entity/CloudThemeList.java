package com.funnyapps.privacy.applock.entity;

import java.util.List;

/**
 * Created by lq on 2016/8/30.
 */
public class CloudThemeList {
    public List<CloudTheme> results;

    @Override
    public String toString() {
        return "CloudThemeList{" +
                "results=" + results +
                '}';
    }
}