package com.funnyapps.privacy.applock.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by AvatarQing on 2016/6/28.
 */
public class SetSecurityQuestionActivity extends BaseActivity {


    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.et_question)
    EditText etQuestion;
    @BindView(R.id.et_answer)
    EditText etAnswer;
    @BindView(R.id.btn_ok)
    Button btnOk;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_security_question);
        ButterKnife.bind(this);
        findViews();
        initViews();
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.SET_SECURITY_QUESTION;
    }

    void findViews() {
        etQuestion = $(R.id.et_question);
        etAnswer = $(R.id.et_answer);
        btnOk = $(R.id.btn_ok);
    }

    void initViews() {
        title.setText(R.string.set_pattern_step_4);
    }

    @OnTextChanged(callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED, value = {R.id.et_question, R.id.et_answer})
    void checkCanGoToNext() {
        String question = getInputText(etQuestion);
        String answer = getInputText(etAnswer);
        boolean filled = !TextUtils.isEmpty(question) && !TextUtils.isEmpty(answer);
        btnOk.setEnabled(filled);
    }

    @OnClick(R.id.btn_ok)
    void goToNext() {
        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.SETTINGS,
                Const.Analytic.Action.SET_SECURITY_QUESTION
        );

        String question = getInputText(etQuestion);
        String answer = getInputText(etAnswer);
        PatternLockUtils.setSecurityQuestion(this, question);
        PatternLockUtils.setSecurityAnswer(this, answer);

        setResult(RESULT_OK);
        finish();
    }

    String getInputText(EditText editText) {
        return editText.getText().toString();
    }

}