package com.funnyapps.privacy.applock.ui.activity;

import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.enums.WallpaperType;
import com.funnyapps.privacy.applock.event.ShowInterstitialAdAfterSetWallpaperEvent;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.ToastUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.utils.WallpaperUtils;

import java.io.IOException;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import rx.Observable;
import rx.Subscriber;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by lq on 2016/12/5.
 */

public class LockedPreviewActivity extends LockedActivity {

    public static final String EXTRA_PREVIEW_WALLPAPER_URL = "EXTRA_PREVIEW_WALLPAPER_URL";
    public static final String EXTRA_PREVIEW_WALLPAPER_TYPE = "EXTRA_PREVIEW_WALLPAPER_TYPE";

    String mPreviewWallpaperUrl;
    WallpaperType mWallpaperType;

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.WALLPAPER_PREVIEW;
    }

    @Override
    void parseArgument() {
        mPreviewWallpaperUrl = getIntent().getStringExtra(EXTRA_PREVIEW_WALLPAPER_URL);
        mWallpaperType = WallpaperType.values()[getIntent().getIntExtra(EXTRA_PREVIEW_WALLPAPER_TYPE, WallpaperType.LOCAL.ordinal())];
        mLockedAppPkgName = getPackageName();
        Debug.li(getLogTag(), "mPreviewWallpaperUrl:" + mPreviewWallpaperUrl + "\n"
                + "mWallpaperType:" + mWallpaperType + "\n");
    }

    @Override
    void initViews() {
        super.initViews();
        bottom_button_bar.setVisibility(View.GONE);
        banner_ad_container_large.setVisibility(View.GONE);
        banner_ad_container_small.setVisibility(View.GONE);
        addFooterView();
    }

    @Override
    void loadAd() {
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    String getWallpaperUrl() {
        return mPreviewWallpaperUrl;
    }

    @Override
    WallpaperType getWallpaperType() {
        return mWallpaperType;
    }

    void addFooterView() {
        FrameLayout rootView = ButterKnife.findById(this, android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.footer_wallpaper, rootView, false);
        rootView.addView(view);

        ButterKnife.findById(view, R.id.btn_set_as_lock_wallpaper)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        applyLockWallpaper();

                        AnalyticUtils.sendGoogleAnalyticEvent(
                                Const.Analytic.Category.WALLPAPER,
                                Const.Analytic.Action.SET_AS_LOCK_WALLPAPER,
                                mWallpaperType == WallpaperType.REMOTE ? mPreviewWallpaperUrl : null
                        );
                    }
                });
        ButterKnife.findById(view, R.id.btn_set_as_launcher_wallpaper)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        applyDesktopWallpaper();

                        AnalyticUtils.sendGoogleAnalyticEvent(
                                Const.Analytic.Category.WALLPAPER,
                                Const.Analytic.Action.SET_AS_DESKTOP_WALLPAPER,
                                mWallpaperType == WallpaperType.REMOTE ? mPreviewWallpaperUrl : null
                        );
                    }
                });
    }

    void applyLockWallpaper() {
        WallpaperUtils.applyWallpaper(this, mPreviewWallpaperUrl, mWallpaperType);
        EventBus.getDefault().post(new ShowInterstitialAdAfterSetWallpaperEvent(mWallpaperType));
        finish();
    }

    void applyDesktopWallpaper() {
        Observable.just(mPreviewWallpaperUrl)
                .observeOn(Schedulers.io())
                .map(new Func1<String, Bitmap>() {
                    @Override
                    public Bitmap call(String wallPaperUrl) {
                        WallpaperType wallpaperType = mWallpaperType;
                        if (wallpaperType == null) {
                            return null;
                        }
                        try {
                            switch (wallpaperType) {
                                case LOCAL:
                                    Debug.li(getLogTag(), "正在加载图片" + wallPaperUrl);
                                    return Utils.loadImageSyncFromFile(wallPaperUrl);
                                case REMOTE:
                                    Debug.li(getLogTag(), "正在下载图片" + wallPaperUrl);
                                    return Utils.loadImageSync(wallPaperUrl);
                            }
                        } catch (Exception e) {
                            throw Exceptions.propagate(e);
                        }
                        return null;
                    }
                })
                .filter(new Func1<Bitmap, Boolean>() {
                    @Override
                    public Boolean call(Bitmap bitmap) {
                        return bitmap != null;
                    }
                })
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        ToastUtils.showToast(R.string.toast_switching_wallpaper);
                    }
                })
                .subscribe(new Subscriber<Bitmap>() {
                    @Override
                    public void onCompleted() {
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Debug.lw(getLogTag(), e.toString());
                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
                        try {
                            wallpaperManager.setBitmap(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new ShowInterstitialAdAfterSetWallpaperEvent(mWallpaperType));
                    }
                });
    }
}