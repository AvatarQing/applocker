package com.funnyapps.privacy.applock.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by AvatarQing on 2016/8/14.
 */
public class GeneralFragmentPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> mFragmentList;
    List<String> mTabTitleList;

    public GeneralFragmentPagerAdapter(FragmentManager fm, List<Fragment> fragmentList, List<String> tabTitleList) {
        super(fm);
        mFragmentList = fragmentList;
        mTabTitleList = tabTitleList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabTitleList.get(position);
    }
}