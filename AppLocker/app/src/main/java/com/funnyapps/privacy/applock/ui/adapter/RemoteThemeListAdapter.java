package com.funnyapps.privacy.applock.ui.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.CloudTheme;
import com.funnyapps.privacy.applock.ui.adapter.viewholder.TaskItemViewHolder;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.ToastUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.web.download.theme.ThemeTasksManager;
import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AvatarQing on 2016/8/31.
 */
public class RemoteThemeListAdapter extends RecyclerView.Adapter<TaskItemViewHolder> {

    List<CloudTheme> mDataList = new ArrayList<>();
    String mAppliedTheme;
    View.OnClickListener mOnThemeClickListener;

    @Override
    public TaskItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TaskItemViewHolder holder = new TaskItemViewHolder(
                LayoutInflater.from(
                        parent.getContext())
                        .inflate(R.layout.item_theme, parent, false));
        holder.theme_preview.setOnClickListener(taskActionOnClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(TaskItemViewHolder holder, int position) {
        Context context = holder.itemView.getContext();
        CloudTheme item = getItem(position);
        holder.theme = item;
        holder.theme_preview.setTag(R.id.position, position);
        holder.theme_preview.setTag(R.id.data, item);
        holder.theme_preview.setTag(holder);

        boolean isChinese = Utils.getLanguage().contains("zh");
        holder.theme_name.setText(isChinese ? item.themeNameZh : item.themeNameEn);
        holder.theme_preview.setHeightRatio(529f / 330f);
        Utils.loadImage(holder.theme_preview, item.preview.url);

        boolean isApplied = TextUtils.equals(item.packageName, mAppliedTheme);
        holder.applied.setVisibility(isApplied ? View.VISIBLE : View.GONE);
        holder.preview_container.setForeground(isApplied ? context.getResources().getDrawable(R.drawable.selected_theme_stroke) : null);

        // >>>>>>>>> 下载相关

        // TODO 没对应上
        int downloadId = item.getDownloadId();
        holder.update(downloadId, position);
        ThemeTasksManager tasksManager = ThemeTasksManager.getImpl();
        tasksManager.updateViewHolder(holder.id, holder);

        holder.theme_preview.setEnabled(true);
        if (tasksManager.isReady()) {
            final int status = tasksManager.getStatus(downloadId, item.getApkPath());
            if (status == FileDownloadStatus.pending || status == FileDownloadStatus.started ||
                    status == FileDownloadStatus.connected) {
                // 已经开始下载文件了，但是文件还没创建，先更新进度
                holder.updateDownloading(status, tasksManager.getSoFar(downloadId)
                        , tasksManager.getTotal(downloadId));
            } else if (!new File(item.getApkPath()).exists() &&
                    !new File(FileDownloadUtils.getTempPath(item.getApkPath())).exists()) {
                // 文件不存在，设置进度为0
                holder.updateNotDownloaded(status, 0, 0);
            } else if (tasksManager.isDownloaded(status)) {
                // 文件早已下载好，进度显示为满
                holder.updateDownloaded();
            } else if (status == FileDownloadStatus.progress) {
                // 下载中
                holder.updateDownloading(status, tasksManager.getSoFar(downloadId)
                        , tasksManager.getTotal(downloadId));
            } else {
                // 还没开始下载，先更新已下载的进度
                holder.updateNotDownloaded(status, tasksManager.getSoFar(downloadId)
                        , tasksManager.getTotal(downloadId));
            }
        } else {
            holder.theme_preview.setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public CloudTheme getItem(int position) {
        return mDataList.get(position);
    }

    public void appendData(List<CloudTheme> list) {
        if (list != null && !list.isEmpty()) {
            this.mDataList.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void setAppliedTheme(String appliedTheme) {
        mAppliedTheme = appliedTheme;
    }

    public String getAppliedTheme() {
        return mAppliedTheme;
    }

    public void startDownloadTask(TaskItemViewHolder holder) {
        final CloudTheme theme = getItem(holder.position);
        final BaseDownloadTask task = FileDownloader.getImpl().create(theme.getApkUrl())
                .setPath(theme.getApkPath())
                .setCallbackProgressTimes(100)
                .setListener(taskDownloadListener);
        ThemeTasksManager.getImpl().addTaskForViewHolder(task);
        ThemeTasksManager.getImpl().updateViewHolder(holder.id, holder);
        task.start();
    }

    public void setOnThemeClickListener(View.OnClickListener listener) {
        this.mOnThemeClickListener = listener;
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }

    private View.OnClickListener taskActionOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() == null) {
                return;
            }
            if (mOnThemeClickListener != null) {
                mOnThemeClickListener.onClick(v);
            }
        }
    };

    private FileDownloadListener taskDownloadListener = new FileDownloadSampleListener() {

        private TaskItemViewHolder checkCurrentHolder(final BaseDownloadTask task) {
            final TaskItemViewHolder tag = (TaskItemViewHolder) task.getTag();
            if (tag.id != task.getId()) {
                return null;
            }
            return tag;
        }

        @Override
        protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.pending(task, soFarBytes, totalBytes);
            final TaskItemViewHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }
            tag.updateDownloading(FileDownloadStatus.pending, soFarBytes, totalBytes);
        }

        @Override
        protected void started(BaseDownloadTask task) {
            super.started(task);
            Debug.li(getLogTag(), "start download " + task.getUrl());
            final TaskItemViewHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }
            Context context = tag.itemView.getContext();
            ToastUtils.showToast(context.getString(R.string.toast_start_download_theme_xx,
                    Utils.isChinese() ? tag.theme.themeNameEn : tag.theme.themeNameEn));
        }

        @Override
        protected void connected(BaseDownloadTask task, String etag, boolean isContinue, int soFarBytes, int totalBytes) {
            super.connected(task, etag, isContinue, soFarBytes, totalBytes);
            final TaskItemViewHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }
            tag.updateDownloading(FileDownloadStatus.connected, soFarBytes, totalBytes);
        }

        @Override
        protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.progress(task, soFarBytes, totalBytes);
            int percent = 100 * soFarBytes / totalBytes;
            Debug.i(getLogTag(), "downloading " + percent + "% of " + task.getUrl());
            final TaskItemViewHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }
            tag.updateDownloading(FileDownloadStatus.progress, soFarBytes, totalBytes);
        }

        @Override
        protected void error(BaseDownloadTask task, Throwable e) {
            super.error(task, e);
            Debug.li(getLogTag(), "Download error: " + e + " of " + task.getUrl());
            final TaskItemViewHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }
            tag.updateNotDownloaded(FileDownloadStatus.error, task.getLargeFileSoFarBytes(), task.getLargeFileTotalBytes());
            ThemeTasksManager.getImpl().removeTaskForViewHolder(task.getId());
        }

        @Override
        protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.paused(task, soFarBytes, totalBytes);
            Debug.li(getLogTag(), "Download paused: " + task.getUrl());
            final TaskItemViewHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }

            tag.updateNotDownloaded(FileDownloadStatus.paused, soFarBytes, totalBytes);
            ThemeTasksManager.getImpl().removeTaskForViewHolder(task.getId());
        }

        @Override
        protected void completed(BaseDownloadTask task) {
            super.completed(task);
            Debug.li(getLogTag(), "Download completed: " + task.getUrl());
            final TaskItemViewHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }
            Context context = tag.itemView.getContext();
            ToastUtils.showToast(context.getString(R.string.toast_theme_download_finish,
                    Utils.isChinese() ? tag.theme.themeNameEn : tag.theme.themeNameEn));
            tag.updateDownloaded();
            ThemeTasksManager.getImpl().removeTaskForViewHolder(task.getId());

            File apkFile = tag.theme.getApkFile();
            if (apkFile.exists()
                    && context.getPackageManager().getPackageArchiveInfo(apkFile.getAbsolutePath(), PackageManager.GET_ACTIVITIES) != null) {
                // 下载了主题包，但没安装就去安装
                Utils.installApk(context, apkFile);
            }
        }
    };

}