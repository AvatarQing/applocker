package com.funnyapps.privacy.applock.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.service.MonitorService;
import com.funnyapps.privacy.applock.ui.adapter.GeneralFragmentPagerAdapter;
import com.funnyapps.privacy.applock.ui.fragment.SelectAppsToProtectFragment;
import com.funnyapps.privacy.applock.ui.fragment.SetPatternFragment;
import com.funnyapps.privacy.applock.ui.fragment.SetSecurityQuestionFragment;
import com.funnyapps.privacy.applock.ui.widget.NoScrollViewPager;
import com.funnyapps.privacy.applock.ui.widget.SetPatternStepView;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.PatternUtils;
import com.haibison.android.lockpattern.widget.LockPatternView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by AvatarQing on 2016/6/21.
 */
public class GuideActivity extends BaseActivity {

    static final int PAGE_SELECT_APPS = 0;
    static final int PAGE_SET_OR_CONFIRM_PATTERN = 1;
    static final int PAGE_SET_SECURITY_QUESTION = 2;

    static final int STEP_SELECT_APPS = 1;
    static final int STEP_SET_PATTERN = 2;
    static final int STEP_CONFIRM_PATTERN = 3;
    static final int STEP_SET_SECURITY_QUESTION = 4;

    @BindView(R.id.step_view)
    SetPatternStepView step_view;
    @BindView(R.id.view_pager)
    NoScrollViewPager view_pager;

    Set<String> mSelectedApps;
    int mCurrentStep = STEP_SELECT_APPS;
    SetPatternFragment mSetPatternFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_new_pattern);
        ButterKnife.bind(this);
        initViewPager();
    }

    @Override
    public void onBackPressed() {
        if (!returnPreviousPage()) {
            super.onBackPressed();
        }
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.SET_NEW_PATTERN_GUIDE;
    }

    boolean returnPreviousPage() {
        if (mCurrentStep > 1) {
            switch (mCurrentStep) {
                case STEP_SET_SECURITY_QUESTION:
                case STEP_CONFIRM_PATTERN:
                    goToSetPatternPage();
                    PatternLockUtils.clearPattern(this);
                    PatternLockUtils.setLockedApps(this, null);
                    mSetPatternFragment.reset();
                    break;
                case STEP_SET_PATTERN:
                    goToSelectAppsPage();
                    mSetPatternFragment.reset();
                    break;
                default:
                    break;
            }
            return true;
        }
        return false;
    }

    void initViewPager() {
        SelectAppsToProtectFragment appsFragment = new SelectAppsToProtectFragment();
        appsFragment.setSelectAppsCallback(new SelectAppsToProtectFragment.SelectAppsCallback() {
            @Override
            public void onAppsSelected(Set<String> apps) {
                mSelectedApps = apps;
                goToSetPatternPage();
            }
        });

        SetPatternFragment setPatternFragment = mSetPatternFragment = new SetPatternFragment();
        setPatternFragment.setPatternSetCallback(new SetPatternFragment.PatternSetCallback() {
            @Override
            public void onRedraw() {
                setCurrentStep(STEP_SET_PATTERN);
            }

            @Override
            public void tryToConfirmPattern() {
                setCurrentStep(STEP_CONFIRM_PATTERN);
            }

            @Override
            public void onPatternConfirmed(List<LockPatternView.Cell> pattern) {
                goToSetSecurityQuestionPage();

                PatternLockUtils.setPattern(getApplicationContext(), PatternUtils.patternToSha1String(pattern));
                PatternLockUtils.setLockedApps(getApplicationContext(), mSelectedApps);
                MonitorService.requestRefreshReadLockedApps(getApplicationContext());
            }
        });

        SetSecurityQuestionFragment setSecurityQuestionFragment = new SetSecurityQuestionFragment();
        setSecurityQuestionFragment.setSetSecurityQuestionCallback(new SetSecurityQuestionFragment.SetSecurityQuestionCallback() {
            @Override
            public void onGoToNext() {
                goToMainPage();
            }
        });

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(appsFragment);
        fragmentList.add(setPatternFragment);
        fragmentList.add(setSecurityQuestionFragment);

        GeneralFragmentPagerAdapter adapter = new GeneralFragmentPagerAdapter(getSupportFragmentManager(), fragmentList, null);
        view_pager.setAdapter(adapter);
        view_pager.enableScroll(false);
        view_pager.setOffscreenPageLimit(fragmentList.size());
    }

    private void setCurrentStep(int step) {
        mCurrentStep = step;
        step_view.setCurrentStep(step);
    }

    private void goToSelectAppsPage() {
        setCurrentStep(STEP_SELECT_APPS);
        view_pager.setCurrentItem(PAGE_SELECT_APPS);
    }

    private void goToSetPatternPage() {
        setCurrentStep(STEP_SET_PATTERN);
        view_pager.setCurrentItem(PAGE_SET_OR_CONFIRM_PATTERN);
    }

    private void goToSetSecurityQuestionPage() {
        setCurrentStep(STEP_SET_SECURITY_QUESTION);
        view_pager.setCurrentItem(PAGE_SET_SECURITY_QUESTION);
    }

    private void goToMainPage() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}