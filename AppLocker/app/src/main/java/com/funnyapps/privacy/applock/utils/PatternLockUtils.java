/*
 * Copyright (c) 2015 Zhang Hai <Dreaming.in.Code.ZH@Gmail.com>
 * All Rights Reserved.
 */

package com.funnyapps.privacy.applock.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.ui.activity.ConfirmPatternActivity;
import com.haibison.android.lockpattern.widget.LockPatternView;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class PatternLockUtils {

    public static final int REQUEST_CODE_CONFIRM_PATTERN = 1214;

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(Const.PrefFileName.LOCK_PATTERN_INFO, Context.MODE_PRIVATE);
    }

    public static void setPattern(Context context, String patternInSha1String) {
        getPrefs(context)
                .edit()
                .putString(Const.PrefKey.LOCK_PATTERN_SHA1, patternInSha1String)
                .apply();
    }

    public static void setPattern(List<LockPatternView.Cell> pattern, Context context) {
        getPrefs(context)
                .edit()
                .putString(Const.PrefKey.LOCK_PATTERN_SHA1, PatternUtils.patternToSha1String(pattern))
                .apply();
    }

    private static String getPatternSha1(Context context) {
        return getPrefs(context)
                .getString(Const.PrefKey.LOCK_PATTERN_SHA1, null);
    }

    public static void clearPattern(Context context) {
        getPrefs(context)
                .edit()
                .remove(Const.PrefKey.LOCK_PATTERN_SHA1)
                .apply();
    }

    public static boolean hasPattern(Context context) {
        return !TextUtils.isEmpty(getPatternSha1(context));
    }

    public static boolean isPatternCorrect(List<LockPatternView.Cell> pattern, Context context) {
        return TextUtils.equals(PatternUtils.patternToSha1String(pattern), getPatternSha1(context));
    }

    // NOTE: Should only be called when there is a pattern for this account.
    public static void confirmPattern(Activity activity, int requestCode) {
        activity.startActivityForResult(new Intent(activity, ConfirmPatternActivity.class),
                requestCode);
    }

    public static void confirmPattern(Activity activity) {
        confirmPattern(activity, REQUEST_CODE_CONFIRM_PATTERN);
    }

    public static void confirmPatternIfHas(Activity activity) {
        if (hasPattern(activity)) {
            confirmPattern(activity);
        }
    }

    public static boolean checkConfirmPatternResult(int requestCode, int resultCode) {
        if (requestCode == REQUEST_CODE_CONFIRM_PATTERN && resultCode == Activity.RESULT_OK) {
            return true;
        } else {
            return false;
        }
    }

    public static void setLockedApps(Context context, Set<String> apps) {
        getPrefs(context)
                .edit()
                .putStringSet(Const.PrefKey.LOCK_APPS, apps)
                .apply();
    }

    public static Set<String> getLockedApps(Context context) {
        return getPrefs(context)
                .getStringSet(Const.PrefKey.LOCK_APPS, null);
    }

    public static void lockApp(Context context, String packageName, boolean lock) {
        Set<String> apps = getLockedApps(context);
        if (apps == null) {
            apps = new HashSet<>();
        }
        if (lock) {
            Debug.li(getLogTag(), "lock : " + packageName);
            apps.add(packageName);
        } else {
            Debug.li(getLogTag(), "unlock : " + packageName);
            Iterator<String> iterator = apps.iterator();
            while (iterator.hasNext()) {
                String pkgName = iterator.next();
                if (TextUtils.equals(pkgName, packageName)) {
                    iterator.remove();
                }
            }
        }
        setLockedApps(context, apps);
    }

    public static boolean isAskLockNewInstalledAppEnabled(Context context) {
        return getPrefs(context)
                .getBoolean(Const.PrefKey.ASK_LOCK_NEW_INSTALLED_APP, true);
    }

    public static void setAskLockNewInstalledAppEnabled(Context context, boolean enabled) {
        getPrefs(context)
                .edit()
                .putBoolean(Const.PrefKey.ASK_LOCK_NEW_INSTALLED_APP, enabled)
                .apply();
    }

    public static boolean isStealthModeEnabled(Context context) {
        return getPrefs(context)
                .getBoolean(Const.PrefKey.PATTERN_VISIBLE, false);
    }

    public static void setStealthModeEnabled(Context context, boolean enabled) {
        getPrefs(context)
                .edit()
                .putBoolean(Const.PrefKey.PATTERN_VISIBLE, enabled)
                .apply();
    }

    public static void setHapticFeedbackEnabled(Context context, boolean enabled) {
        getPrefs(context)
                .edit()
                .putBoolean(Const.PrefKey.HAPTIC_FEEDBACK, enabled)
                .apply();
    }

    public static boolean isHapticFeedbackEnabled(Context context) {
        return getPrefs(context)
                .getBoolean(Const.PrefKey.HAPTIC_FEEDBACK, true);
    }

    public static void setSecurityQuestion(Context context, String question) {
        getPrefs(context)
                .edit()
                .putString(Const.PrefKey.SECURITY_QUESTION, question)
                .apply();
    }

    public static void setSecurityAnswer(Context context, String answer) {
        getPrefs(context)
                .edit()
                .putString(Const.PrefKey.SECURITY_ANSWER, answer)
                .apply();
    }

    public static String getSecurityQuestion(Context context) {
        return getPrefs(context).getString(Const.PrefKey.SECURITY_QUESTION, null);
    }

    public static String getSecurityAnswer(Context context) {
        return getPrefs(context).getString(Const.PrefKey.SECURITY_ANSWER, null);
    }

    public static boolean hasSetSecurityQuestion(Context context) {
        return !TextUtils.isEmpty(getSecurityQuestion(context)) &&
                !TextUtils.isEmpty(getSecurityAnswer(context));
    }

    public static void setLockFrequency(Context context, int frequency) {
        getPrefs(context)
                .edit()
                .putInt(Const.PrefKey.LOCK_FREQUENCY, frequency)
                .apply();
    }

    public static int getLockFrequency(Context context) {
        return Const.LockFrequency.IMMEDIATELY;
//        return getPrefs(context)
//                .getInt(Const.PrefKey.LOCK_FREQUENCY, Const.LockFrequency.IMMEDIATELY);
    }


    public static boolean needShowRatingHeader(Context context) {
        return getPrefs(context)
                .getBoolean(Const.PrefKey.NEED_SHOW_RATING_HEADER, true);
    }

    public static void setIfShowRatingHeader(Context context, boolean shown) {
        getPrefs(context)
                .edit()
                .putBoolean(Const.PrefKey.NEED_SHOW_RATING_HEADER, shown)
                .apply();
    }

    public static void increaseAppLaunchTime(Context context) {
        int times = getAppLaunchTime(context);
        setAppLaunchTime(context, ++times);
    }

    public static void setAppLaunchTime(Context context, int time) {
        getPrefs(context)
                .edit()
                .putInt(Const.PrefKey.LAUNCH_TIME, time)
                .apply();
    }

    public static int getAppLaunchTime(Context context) {
        return getPrefs(context)
                .getInt(Const.PrefKey.LAUNCH_TIME, 0);
    }

    private static String getLogTag() {
        return PatternLockUtils.class.getSimpleName();
    }

}