package com.funnyapps.privacy.applock.ui.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableList;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.BR;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ad.AppLovinAds;
import com.funnyapps.privacy.applock.databinding.FragmentLocalWallpaperListBinding;
import com.funnyapps.privacy.applock.entity.LocalWallPaper;
import com.funnyapps.privacy.applock.enums.WallpaperType;
import com.funnyapps.privacy.applock.event.RefreshLocalWallpaperEvent;
import com.funnyapps.privacy.applock.ui.activity.LockedPreviewActivity;
import com.funnyapps.privacy.applock.ui.bindingadpaters.command.ReplyCommand;
import com.funnyapps.privacy.applock.ui.bindingadpaters.command.TaskCommand;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.WallpaperUtils;
import com.trello.rxlifecycle.LifecycleProvider;
import com.trello.rxlifecycle.android.ActivityEvent;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import de.greenrobot.event.EventBus;
import me.tatarka.bindingcollectionadapter.BaseItemViewSelector;
import me.tatarka.bindingcollectionadapter.ItemView;
import me.tatarka.bindingcollectionadapter.ItemViewSelector;
import rx.Observable;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by lq on 2016/11/29.
 */

public class LocalWallpaperViewModel {

    // >>>>>>>>>>>>>> Binding Data >>>>>>>>>>>>>>>>>>>>>
    public final ObservableBoolean showLoadingView = new ObservableBoolean(false);
    public final ObservableBoolean showEmptyView = new ObservableBoolean(false);
    public final ObservableBoolean isInSelectMode = new ObservableBoolean(false);

    public final ObservableList<Object> dataList = new ObservableArrayList<>();
    public final ItemViewSelector<Object> itemView = new BaseItemViewSelector<Object>() {
        @Override
        public void select(ItemView itemView, int position, Object item) {
            if (item instanceof NativeAdItemViewModel) {
                itemView.set(BR.viewModel, R.layout.item_native_ad);
            } else {
                itemView.set(BR.viewModel, R.layout.item_local_wallpaper);
            }
        }

        @Override
        public int viewTypeCount() {
            if (AppLovinAds.get().isNativeAdLoaded()) {
                return 2;
            }
            return 1;
        }
    };
    public final ReplyCommand<Boolean> scrollDirectionChangeCommand = new ReplyCommand<>(new Action1<Boolean>() {
        @Override
        public void call(Boolean isPullDown) {
            fabShowCommand.execute(isPullDown);
        }
    });

    public final GridLayoutManager layoutManager(RecyclerView recyclerView) {
        final int spanSize = 2;
        GridLayoutManager lm = new GridLayoutManager(recyclerView.getContext(), spanSize);
        lm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position < 0 || position >= dataList.size()) {
                    return 0;
                }
                if (dataList.get(position) instanceof NativeAdItemViewModel) {
                    return spanSize;
                } else {
                    return 1;
                }
            }
        });
        return lm;
    }

    public final TaskCommand<Boolean> fabShowCommand = new TaskCommand<>();
    // <<<<<<<<<<<<<< Binding Data <<<<<<<<<<<<<<<<<<<<<

    private LocalWallPaper mLastAppliedWallpaper;
    private Set<LocalWallPaper> mSelectedWallpaperSet = new HashSet<>();
    private FragmentActivity mContext;
    private LifecycleProvider<ActivityEvent> mLifecycleProvider;
    private FragmentLocalWallpaperListBinding mViewBinding;

    public LocalWallpaperViewModel(FragmentActivity context, FragmentLocalWallpaperListBinding viewBinding, LifecycleProvider<ActivityEvent> lifecycleProvider) {
        this.mContext = context;
        this.mViewBinding = viewBinding;
        this.mLifecycleProvider = lifecycleProvider;
    }

    public File getWallpaperImageDir() {
        String relativePath = mContext.getPackageName() + File.separator + "wallpaper";
        return new File(Environment.getExternalStorageDirectory(), relativePath);
    }

    public void loadWallpapers() {
        final String appliedWallpaperUrl = WallpaperUtils.getAppliedWallpaperUrl(mContext);
        Observable
                .fromCallable(new Callable<List<File>>() {
                    @Override
                    public List<File> call() throws Exception {
                        File dir = getWallpaperImageDir();
                        if (dir.exists()) {
                            return Arrays.asList(dir.listFiles());
                        } else {
                            return null;
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .filter(new Func1<List<File>, Boolean>() {
                    @Override
                    public Boolean call(List<File> files) {
                        return files != null && !files.isEmpty();
                    }
                })
                .flatMap(new Func1<List<File>, Observable<File>>() {
                    @Override
                    public Observable<File> call(List<File> files) {
                        return Observable.from(files);
                    }
                })
                .sorted(new Func2<File, File, Integer>() {
                    @Override
                    public Integer call(File file, File file2) {
                        if (file.lastModified() - file2.lastModified() > 0) {
                            return -1;
                        } else {
                            return 1;
                        }
                    }
                })
                .map(new Func1<File, LocalWallPaper>() {
                    @Override
                    public LocalWallPaper call(File file) {
                        LocalWallPaper wallPaper = new LocalWallPaper();
                        wallPaper.imageFilePath = file.getAbsolutePath();
                        wallPaper.isApplied.set(TextUtils.equals(wallPaper.imageFilePath, appliedWallpaperUrl));
                        return wallPaper;
                    }
                })
                .compose(mLifecycleProvider.<LocalWallPaper>bindUntilEvent(ActivityEvent.DESTROY))
                .map(new Func1<LocalWallPaper, LocalWallpaperItemViewModel>() {
                    @Override
                    public LocalWallpaperItemViewModel call(LocalWallPaper localWallPaper) {
                        return new LocalWallpaperItemViewModel(localWallPaper, isInSelectMode, LocalWallpaperViewModel.this);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<LocalWallpaperItemViewModel>() {
                    @Override
                    public void call(LocalWallpaperItemViewModel viewModel) {
                        if (viewModel.wallpaper.isApplied.get()) {
                            mLastAppliedWallpaper = viewModel.wallpaper;
                        }
                    }
                })
                .toList()
                .toSingle()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        showLoadingView.set(true);
                        dataList.clear();
                    }
                })
                .subscribe(new SingleSubscriber<List<LocalWallpaperItemViewModel>>() {
                    @Override
                    public void onSuccess(List<LocalWallpaperItemViewModel> wallPaperList) {
                        showLoadingView.set(false);
                        if (wallPaperList != null && !wallPaperList.isEmpty()) {
                            dataList.addAll(wallPaperList);
                            loadNativeAd();
                        } else {
                            showEmptyView.set(true);
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        error.printStackTrace();
                        Debug.lw(getLogTag(), error.toString());
                        showLoadingView.set(false);
                    }
                });
    }

    public void deleteSelectedWallpapers(final Context context) {
        final WallpaperType wallpaperType = WallpaperUtils.getAppliedWallpaperType(context);
        final String appliedWallpaperUrl = WallpaperUtils.getAppliedWallpaperUrl(context);
        Observable
                .from(mSelectedWallpaperSet)
                .observeOn(Schedulers.io())
                .subscribe(new Subscriber<LocalWallPaper>() {
                               StringBuilder logSb = new StringBuilder();

                               @Override
                               public void onCompleted() {
                                   Debug.li(getLogTag(), logSb.toString());
                                   Observable.empty()
                                           .observeOn(AndroidSchedulers.mainThread())
                                           .subscribe(new Subscriber<Object>() {
                                               @Override
                                               public void onCompleted() {
                                                   mSelectedWallpaperSet.clear();
                                                   isInSelectMode.set(false);
                                                   EventBus.getDefault().post(new RefreshLocalWallpaperEvent());
                                               }

                                               @Override
                                               public void onError(Throwable e) {
                                               }

                                               @Override
                                               public void onNext(Object o) {
                                               }
                                           });
                               }

                               @Override
                               public void onError(Throwable e) {
                                   e.printStackTrace();
                                   Debug.lw(getLogTag(), e.toString());
                               }

                               @Override
                               public void onNext(LocalWallPaper localWallPaper) {
                                   if (wallpaperType == WallpaperType.LOCAL
                                           && TextUtils.equals(appliedWallpaperUrl, localWallPaper.imageFilePath)) {
                                       WallpaperUtils.applyWallpaper(context, null, WallpaperType.LOCAL);
                                   }
                                   File file = new File(localWallPaper.imageFilePath);
                                   boolean deleteSuccess = file.delete();
                                   logSb.append(localWallPaper.imageFilePath).append(" , ")
                                           .append("deleteSuccess :").append(deleteSuccess)
                                           .append("\n");
                               }
                           }

                );
    }

    public boolean onItemLongClick(LocalWallPaper wallpaper) {
        if (isInSelectMode.get()) {
            clearSelection();
        } else {
            isInSelectMode.set(true);
            selectWallpaper(wallpaper, true);
        }
        return true;
    }

    public void onItemClick(LocalWallPaper wallpaper) {
        if (isInSelectMode.get()) {
            Debug.li(getLogTag(), "select wallpaper : " + wallpaper);
            selectWallpaper(wallpaper, !wallpaper.isChecked.get());
        } else {
            Debug.li(getLogTag(), "apply wallpaper : " + wallpaper);
            goToPreviewPage(mContext, wallpaper);

            AnalyticUtils.sendGoogleAnalyticEvent(
                    Const.Analytic.Category.WALLPAPER,
                    Const.Analytic.Action.PREVIEW_LOCAL_WALLPAPER,
                    null
            );
        }
    }

    public void clearSelection() {
        isInSelectMode.set(false);
        for (LocalWallPaper wallPaper : mSelectedWallpaperSet) {
            wallPaper.isChecked.set(false);
        }
    }

    private void selectWallpaper(LocalWallPaper wallpaper, boolean selected) {
        wallpaper.isChecked.set(selected);
        if (selected) {
            mSelectedWallpaperSet.add(wallpaper);
        } else {
            mSelectedWallpaperSet.remove(wallpaper);
            if (mSelectedWallpaperSet.isEmpty()) {
                clearSelection();
            }
        }
    }

    private void goToPreviewPage(Context context, LocalWallPaper wallpaper) {
        Intent intent = new Intent(context, LockedPreviewActivity.class)
                .putExtra(LockedPreviewActivity.EXTRA_PREVIEW_WALLPAPER_TYPE, WallpaperType.LOCAL.ordinal())
                .putExtra(LockedPreviewActivity.EXTRA_PREVIEW_WALLPAPER_URL, wallpaper.imageFilePath);
        context.startActivity(intent);
    }

    public void clearAppliedWallpaper() {
        if (mLastAppliedWallpaper != null) {
            mLastAppliedWallpaper.isApplied.set(false);
            mLastAppliedWallpaper = null;
        }
    }

    public void loadNativeAd() {
        AppLovinAds.get().loadNativeAd();
        AppLovinAds.get().getNativeAdLoadResultLiveData().observe(mContext, loaded -> {
            if (loaded != null && loaded) {
                View adView = AppLovinAds.get().getNewNativeAdView();
                if (adView != null) {
                    NativeAdItemViewModel viewModel = new NativeAdItemViewModel(adView);
                    dataList.add(0, viewModel);
                    mViewBinding.recyclerView.smoothScrollToPosition(0);
                }
            }
        });
    }

    public void refreshAppliedWallpaper() {
        final String appliedWallpaperUrl = WallpaperUtils.getAppliedWallpaperUrl(mContext);
        Observable.from(dataList)
                .filter(new Func1<Object, Boolean>() {
                    @Override
                    public Boolean call(Object o) {
                        return o instanceof LocalWallpaperItemViewModel;
                    }
                })
                .map(new Func1<Object, LocalWallPaper>() {
                    @Override
                    public LocalWallPaper call(Object o) {
                        return ((LocalWallpaperItemViewModel) o).wallpaper;
                    }
                })
                .filter(new Func1<LocalWallPaper, Boolean>() {
                    @Override
                    public Boolean call(LocalWallPaper wallPaper) {
                        return TextUtils.equals(appliedWallpaperUrl, wallPaper.imageFilePath);
                    }
                })
                .first()
                .subscribe(
                        new Action1<LocalWallPaper>() {
                            @Override
                            public void call(LocalWallPaper wallPaper) {
                                wallPaper.isApplied.set(true);
                                mLastAppliedWallpaper = wallPaper;
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
    }

    private String getLogTag() {
        return getClass().getSimpleName();
    }

}