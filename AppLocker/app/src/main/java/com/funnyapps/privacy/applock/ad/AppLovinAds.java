package com.funnyapps.privacy.applock.ad;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.common.tool.Debug;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ad.carouselui.cards.InlineCarouselCardMediaView;
import com.funnyapps.privacy.applock.ad.carouselui.cards.InlineCarouselCardState;
import com.funnyapps.privacy.applock.function.BooleanSupplier;
import com.funnyapps.privacy.applock.function.Consumer;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author AvatarQing
 */
public class AppLovinAds {

    static final String TAG = "AppLovinAds";

    private static class SingletonHolder {
        private final static AppLovinAds INSTANCE = new AppLovinAds();
    }

    public static AppLovinAds get() {
        return SingletonHolder.INSTANCE;
    }

    private AppLovinAds() {
    }

    private final Handler mainHandler = new Handler(Looper.getMainLooper());
    private Context appContext;
    private AppLovinAd bannerAd;
    private AppLovinAd interstitialAd;
    private AppLovinNativeAd nativeAd;
    private MutableLiveData<Boolean> bannerAdLoadResultLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> interstitialAdLoadResultLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> nativeAdLoadResultLiveData = new MutableLiveData<>();
    private Map<String, Boolean> loadingStates = new HashMap<>();

    public void init(Context context) {
        appContext = context.getApplicationContext();
    }

    public boolean isBannerAdLoaded() {
        return bannerAd != null;
    }

    public boolean isInterstitialAdLoaded() {
        return interstitialAd != null;
    }

    public boolean isNativeAdLoaded() {
        return nativeAd != null;
    }

    public LiveData<Boolean> getDisposableBannerAdLoadResultLiveData() {
        return getDisposableAdLoadResultLiveData("Banner", bannerAdLoadResultLiveData, this::isBannerAdLoaded);
    }

    public LiveData<Boolean> getDisposableInterstitialAdLoadResultLiveData() {
        return getDisposableAdLoadResultLiveData("Interstitial", interstitialAdLoadResultLiveData, this::isInterstitialAdLoaded);
    }

    public LiveData<Boolean> getNativeAdLoadResultLiveData() {
        return getDisposableAdLoadResultLiveData("Native", nativeAdLoadResultLiveData, this::isNativeAdLoaded);
    }

    public void loadBannerAd() {
        loadSingleAdIfNeed(
                AppLovinAdSize.BANNER,
                this::isBannerAdLoaded,
                ad -> {
                    bannerAd = ad;
                    Debug.ld(TAG, "Updated banner ad to new one");
                },
                bannerAdLoadResultLiveData::setValue
        );
    }

    public void loadInterstitialAd() {
        loadSingleAdIfNeed(
                AppLovinAdSize.INTERSTITIAL,
                this::isInterstitialAdLoaded,
                ad -> {
                    interstitialAd = ad;
                    Debug.ld(TAG, "Updated interstitial ad to new one");
                },
                interstitialAdLoadResultLiveData::setValue
        );
    }

    public void loadNativeAd() {
        if (isNativeAdLoaded()) {
            Debug.ld(TAG, "Native ad is already loaded");
            return;
        }
        forceLoadNextNativeAd();
    }

    private void forceLoadNextNativeAd() {
        Boolean isLoading = loadingStates.get(AppLovinAdSize.NATIVE.getLabel());
        if (isLoading != null && isLoading) {
            Debug.li(TAG, String.format(Locale.CHINA, "%s ad is loading now", AppLovinAdSize.NATIVE.getLabel()));
            return;
        }
        Debug.ld(TAG, "Start loading native ad");
        loadingStates.put(AppLovinAdSize.NATIVE.getLabel(), true);
        AppLovinSdk.getInstance(appContext)
                .getNativeAdService()
                .loadNativeAds(1, new AppLovinNativeAdLoadListener() {
                    @Override
                    public void onNativeAdsLoaded(List<AppLovinNativeAd> adList) {
                        postToMainThread(() -> {
                            Debug.li(AppLovinAds.TAG, String.format(Locale.CHINA, "Native ad loaded, ad count: %d", adList.size()));
                            loadingStates.put(AppLovinAdSize.NATIVE.getLabel(), false);
                            AppLovinNativeAd nativeAd = adList.get(0);
                            AppLovinSdk.getInstance(appContext)
                                    .getNativeAdService()
                                    .precacheResources(nativeAd, new LogNativePrecacheListener());
                            AppLovinAds.this.nativeAd = nativeAd;
                            Debug.ld(TAG, "Updated native ad to new one");
                            nativeAdLoadResultLiveData.setValue(true);
                        });
                    }

                    @Override
                    public void onNativeAdsFailedToLoad(int errorCode) {
                        postToMainThread(() -> {
                            Debug.lw(AppLovinAds.TAG, String.format(Locale.CHINA, "Native ad failed to load with error code %d", errorCode));
                            loadingStates.put(AppLovinAdSize.NATIVE.getLabel(), false);
                            nativeAdLoadResultLiveData.setValue(false);
                        });
                    }
                });
    }

    @Nullable
    public View getNewBannerAdView() {
        if (!isBannerAdLoaded()) {
            Debug.lw(TAG, "Banner ad not loaded yet");
            return null;
        }
        AppLovinAdView adView = new AppLovinAdView(AppLovinAdSize.BANNER, appContext);
        adView.setAdDisplayListener(new AppLovinAdDisplayListener() {
            @Override
            public void adDisplayed(AppLovinAd appLovinAd) {
                Debug.ld(TAG, "Banner ad view is displayed");
            }

            @Override
            public void adHidden(AppLovinAd appLovinAd) {
                Debug.ld(TAG, "Banner ad view is hidden");
            }
        });
        adView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                Debug.ld(TAG, "Banner ad view is attached to window");
                postToMainThread(() -> {
                    clearBannerAd();
                    loadBannerAd();
                });
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                Debug.ld(TAG, "Banner ad view is detached to window");
            }
        });
        adView.renderAd(bannerAd);
        return adView;
    }

    public void showInterstitialAd() {
        if (!isInterstitialAdLoaded()) {
            Debug.lw(TAG, "Interstitial ad not loaded yet");
            return;
        }
        AppLovinInterstitialAdDialog interstitialAdDialog = AppLovinInterstitialAd.create(AppLovinSdk.getInstance(appContext), appContext);
        interstitialAdDialog.setAdDisplayListener(new AppLovinAdDisplayListener() {
            @Override
            public void adDisplayed(AppLovinAd appLovinAd) {
                Debug.ld(TAG, "Interstitial ad is displayed");
            }

            @Override
            public void adHidden(AppLovinAd appLovinAd) {
                Debug.ld(TAG, "Interstitial ad is hidden");
                postToMainThread(() -> {
                    clearInterstitialAd();
                    loadInterstitialAd();
                });
            }
        });
        interstitialAdDialog.showAndRender(interstitialAd);
    }

    @Nullable
    public View getNewNativeAdView() {
        if (!isNativeAdLoaded()) {
            Debug.lw(TAG, "Native ad not loaded yet");
            return null;
        }
        return bindNativeAdView(nativeAd);
    }

    @NonNull
    private View bindNativeAdView(@NonNull AppLovinNativeAd nativeAd) {
        View adView = LayoutInflater.from(appContext).inflate(R.layout.native_ad, null);
        ImageView appIcon = adView.findViewById(R.id.appIcon);
        ImageView appRating = adView.findViewById(R.id.appRating);
        TextView appTitleTextView = adView.findViewById(R.id.appTitleTextView);
        TextView appDescriptionTextView = adView.findViewById(R.id.appDescriptionTextView);
        FrameLayout mediaViewPlaceholder = adView.findViewById(R.id.mediaViewPlaceholder);
        Button appDownloadButton = adView.findViewById(R.id.appDownloadButton);

        appTitleTextView.setText(nativeAd.getTitle());
        appDescriptionTextView.setText(nativeAd.getDescriptionText());
        AppLovinSdkUtils.safePopulateImageView(
                appIcon,
                Uri.parse(nativeAd.getIconUrl()),
                AppLovinSdkUtils.dpToPx(appContext, 50)
        );
        Drawable starRatingDrawable = getStarRatingDrawable(nativeAd.getStarRating());
        appRating.setImageDrawable(starRatingDrawable);
        appDownloadButton.setText(nativeAd.getCtaText());
        appDownloadButton.setOnClickListener(v -> nativeAd.launchClickTarget(appContext));

        InlineCarouselCardMediaView mediaView = new InlineCarouselCardMediaView(appContext);
        mediaView.setAd(nativeAd);
        mediaView.setCardState(new InlineCarouselCardState());
        mediaView.setSdk(AppLovinSdk.getInstance(appContext));
        mediaView.setUiHandler(new Handler(Looper.getMainLooper()));
        mediaView.setUpView();
        mediaView.autoplayVideo();

        mediaViewPlaceholder.removeAllViews();
        mediaViewPlaceholder.addView(mediaView);

        adView.addOnAttachStateChangeListener(new TrackImpressionViewHelper(nativeAd) {
            @Override
            public void onViewAttachedToWindow(View v) {
                super.onViewAttachedToWindow(v);
                Debug.ld(TAG, "Native ad view is attached to window");
                forceLoadNextNativeAd();
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                super.onViewDetachedFromWindow(v);
                Debug.ld(TAG, "Native ad view is detached to window");
            }
        });

        return adView;
    }

    private Drawable getStarRatingDrawable(final float starRating) {
        final String sanitizedRating = Float.toString(starRating).replace(".", "_");
        final String resourceName = "applovin_star_sprite_" + sanitizedRating;
        final int drawableId = appContext.getResources().getIdentifier(resourceName, "drawable", appContext.getPackageName());
        return appContext.getResources().getDrawable(drawableId);
    }

    private LiveData<Boolean> getDisposableAdLoadResultLiveData(String adType, MutableLiveData<Boolean> sourceLiveData, BooleanSupplier isLoadedAd) {
        Debug.ld(TAG, String.format(Locale.CHINA, "Create new disposable live data for %s ad", adType));
        MediatorLiveData<Boolean> newLiveData = new MediatorLiveData<>();
        if (isLoadedAd.getAsBoolean()) {
            Debug.ld(TAG, String.format(Locale.CHINA, "Already loaded %s ad, set loaded state", adType));
            newLiveData.setValue(true);
        } else {
            boolean hasSourceValue = sourceLiveData.getValue() != null;
            boolean[] needSkip = new boolean[]{hasSourceValue};
            newLiveData.addSource(sourceLiveData, result -> {
                if (needSkip[0]) {
                    needSkip[0] = false;
                    Debug.ld(TAG, String.format(Locale.CHINA, "Skip %s ad old load state", adType));
                } else {
                    Debug.ld(TAG, String.format(Locale.CHINA, "%s ad load finish, set new state, loaded: %s", adType, result));
                    newLiveData.removeSource(sourceLiveData);
                    newLiveData.setValue(result);
                }
            });
        }
        return newLiveData;
    }

    private void loadSingleAdIfNeed(AppLovinAdSize adType, BooleanSupplier hasLoaded, Consumer<AppLovinAd> updateAdTask, Consumer<Boolean> updateLoadResultTask) {
        if (hasLoaded.getAsBoolean()) {
            Debug.ld(TAG, String.format(Locale.CHINA, "%s ad is already loaded", adType.getLabel()));
            return;
        }
        loadSingleAd(adType, updateAdTask, updateLoadResultTask);
    }

    private void loadSingleAd(AppLovinAdSize adType, Consumer<AppLovinAd> updateAdTask, Consumer<Boolean> updateLoadResultTask) {
        Boolean isLoading = loadingStates.get(adType.getLabel());
        if (isLoading != null && isLoading) {
            Debug.li(TAG, String.format(Locale.CHINA, "%s ad is loading now", adType.getLabel()));
            return;
        }
        loadingStates.put(adType.getLabel(), true);
        Debug.ld(TAG, String.format(Locale.CHINA, "Start loading %s ad", adType.getLabel()));
        AppLovinSdk.getInstance(appContext)
                .getAdService()
                .loadNextAd(adType, new AppLovinAdLoadListener() {
                    @Override
                    public void adReceived(AppLovinAd appLovinAd) {
                        postToMainThread(() -> {
                            Debug.li(AppLovinAds.TAG, String.format(Locale.CHINA, "%s ad loaded", adType.getLabel()));
                            loadingStates.put(adType.getLabel(), false);
                            updateAdTask.accept(appLovinAd);
                            updateLoadResultTask.accept(true);
                        });
                    }

                    @Override
                    public void failedToReceiveAd(int errorCode) {
                        postToMainThread(() -> {
                            Debug.lw(AppLovinAds.TAG, String.format(Locale.CHINA, "%s ad failed to load with error code %d", adType.getLabel(), errorCode));
                            loadingStates.put(adType.getLabel(), false);
                            updateLoadResultTask.accept(false);
                        });
                    }
                });
    }

    private void clearBannerAd() {
        bannerAd = null;
    }

    private void clearInterstitialAd() {
        interstitialAd = null;
    }

    private void postToMainThread(Runnable runnable) {
        mainHandler.post(runnable);
    }

}
