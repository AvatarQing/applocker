package com.funnyapps.privacy.applock.ui.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.AppInfo;
import com.funnyapps.privacy.applock.ui.adapter.AppListAdapter;
import com.funnyapps.privacy.applock.utils.Const;
import com.abc.common.tool.Debug;
import com.trello.rxlifecycle.android.FragmentEvent;

import java.text.Collator;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by AvatarQing on 2016/6/13.
 */
public class AppListFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_progress)
    ProgressBar loadingProgress;

    AppListAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_app_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initRecyclerView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadAppList();
    }

    void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(getAdapter());
    }

    AppListAdapter getAdapter() {
        if (mAdapter == null) {
            mAdapter = new AppListAdapter();
        }
        return mAdapter;
    }

    void showListContent(boolean shown) {
        recyclerView.setVisibility(shown ? View.VISIBLE : View.GONE);
        loadingProgress.setVisibility(!shown ? View.VISIBLE : View.GONE);
    }

    void loadAppList() {
        final PackageManager pm = getActivity().getPackageManager();
        String thisPackageName = getActivity().getPackageName();
        final Set<String> lockedApps = getLockedApps();
        Observable
                .create(new Observable.OnSubscribe<ResolveInfo>() {
                    @Override
                    public void call(Subscriber<? super ResolveInfo> subscriber) {
                        // 获取本机已安装应用
                        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
                        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        List<ResolveInfo> appList = pm.queryIntentActivities(mainIntent, 0);
                        Debug.li(getLogTag(), "OnSubscribe->infoList.size:" + appList.size());

                        for (ResolveInfo info : appList) {
                            subscriber.onNext(info);
                        }
                        subscriber.onCompleted();
                    }
                })
                .onBackpressureBuffer()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .distinct(new Func1<ResolveInfo, String>() {
                    @Override
                    public String call(ResolveInfo resolveInfo) {
                        return resolveInfo.activityInfo.packageName;
                    }
                })
                .filter(resolveInfo -> !resolveInfo.activityInfo.packageName.equals(thisPackageName))
                .observeOn(Schedulers.io())
                .map(new Func1<ResolveInfo, AppInfo>() { // 转换成需要的实体类
                    @Override
                    public AppInfo call(ResolveInfo resolveInfo) {
                        AppInfo info = new AppInfo();
                        info.setAppIcon(resolveInfo.loadIcon(pm));
                        info.setAppName(resolveInfo.loadLabel(pm).toString());
                        info.setPackageName(resolveInfo.activityInfo.packageName);
                        if (lockedApps != null) {
                            info.setChecked(lockedApps.contains(info.getPackageName()));
                        }
                        return info;
                    }
                })
                .observeOn(Schedulers.computation())
                .sorted(new Func2<AppInfo, AppInfo, Integer>() { // 按名称首字母排序
                    private final Collator collator = Collator.getInstance();

                    @Override
                    public Integer call(AppInfo object1, AppInfo object2) {
                        return collator.compare(object1.getAppName(), object2.getAppName());
                    }
                })
                .sorted(new Func2<AppInfo, AppInfo, Integer>() { // 勾选的排在前面
                    @Override
                    public Integer call(AppInfo object1, AppInfo object2) {
                        if (object1.isChecked() && !object2.isChecked()) {
                            return -1;
                        }
                        if (!object1.isChecked() && object2.isChecked()) {
                            return 1;
                        }
                        return 0;
                    }
                })
                .toList() // 转换为列表
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Action0() { // 订阅前做初始化工作
                    @Override
                    public void call() {
                        showListContent(false);
                    }
                })
                .compose(this.<List<AppInfo>>bindUntilEvent(FragmentEvent.DESTROY))
                .subscribe(new Subscriber<List<AppInfo>>() {
                    @Override
                    public void onCompleted() {
                        showListContent(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        showListContent(true);
                    }

                    @Override
                    public void onNext(List<AppInfo> appList) {
                        getAdapter().setData(appList);
                    }
                });
    }

    Set<String> getLockedApps() {
        Set<String> lockedApps = new HashSet<>();
        lockedApps.addAll(Arrays.asList(Const.DEFAULT_LOCKED_APPS));
        return lockedApps;
    }

    public Set<String> getSelectedApps() {
        return getAdapter().getSelectedApps();
    }

}