package com.funnyapps.privacy.applock.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.abc.common.tool.Debug;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ad.AppLovinAds;
import com.funnyapps.privacy.applock.databinding.FragmentLocalWallpaperListBinding;
import com.funnyapps.privacy.applock.enums.WallpaperType;
import com.funnyapps.privacy.applock.event.RefreshLocalWallpaperEvent;
import com.funnyapps.privacy.applock.event.ShowInterstitialAdAfterSetWallpaperEvent;
import com.funnyapps.privacy.applock.event.WallpaperAppliedEvent;
import com.funnyapps.privacy.applock.ui.viewmodel.LocalWallpaperViewModel;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.imageloader.UILImageLoader;
import com.github.clans.fab.FloatingActionButton;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by lq on 2016/11/30.
 */

public class LocalWallpaperListActivity extends BaseActivity {

    public static final int REQUEST_CODE_PICK_IMAGE = 1;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.loading_progress)
    View loading_progress;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.btn_delete)
    TextView btn_delete;

    ImagePicker imagePicker;
    LocalWallpaperViewModel mViewModel;
    FragmentLocalWallpaperListBinding mViewBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewBinding = DataBindingUtil.setContentView(this, R.layout.fragment_local_wallpaper_list);
        mViewModel = new LocalWallpaperViewModel(this, mViewBinding, this);
        mViewBinding.setViewModel(mViewModel);
        ButterKnife.bind(this);
        loadAd();
        initImagePicker();
        mViewModel.loadWallpapers();
        mViewModel.isInSelectMode.addOnPropertyChangedCallback(mSelectModeChangeListener);
        mViewModel.showLoadingView.addOnPropertyChangedCallback(showLoadingPropertyChangedCallback);
        mViewModel.showEmptyView.addOnPropertyChangedCallback(showEmptyViewPropertyChangedCallback);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        mViewModel.isInSelectMode.removeOnPropertyChangedCallback(mSelectModeChangeListener);
        mViewModel.showLoadingView.removeOnPropertyChangedCallback(showLoadingPropertyChangedCallback);
        mViewModel.showEmptyView.removeOnPropertyChangedCallback(showEmptyViewPropertyChangedCallback);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_PICK_IMAGE: {
                if (resultCode == ImagePicker.RESULT_CODE_ITEMS && data != null) {
                    ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                    ImageItem pickedImage = images.get(0);
                    startCropActivity(pickedImage.path);
                }
            }
            break;
            case UCrop.REQUEST_CROP: {
                switch (resultCode) {
                    case RESULT_OK: {
                        final Uri resultUri = UCrop.getOutput(data);
                        Debug.li(getLogTag(), "cropped image uri:" + resultUri);
                        mViewModel.loadWallpapers();

                        AnalyticUtils.sendGoogleAnalyticEvent(
                                Const.Analytic.Category.WALLPAPER,
                                Const.Analytic.Action.ADDED_LOCAL_WALLPAPER,
                                null
                        );
                    }
                    break;
                    case UCrop.RESULT_ERROR: {
                        Throwable e = UCrop.getError(data);
                        if (e != null) {
                            e.printStackTrace();
                        }
                    }
                    break;
                    default:
                        break;
                }
            }
            break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewModel.isInSelectMode.get()) {
            mViewModel.isInSelectMode.set(false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.LOCAL_WALLPAPER_LIST;
    }

    public void onEventMainThread(ShowInterstitialAdAfterSetWallpaperEvent event) {
        if (event.wallpaperType == WallpaperType.LOCAL) {
            AppLovinAds.get().showInterstitialAd();
        }
    }

    public void onEventMainThread(RefreshLocalWallpaperEvent event) {
        mViewModel.loadWallpapers();
    }

    public void onEventMainThread(WallpaperAppliedEvent event) {
        mViewModel.clearAppliedWallpaper();
        if (event.type == WallpaperType.LOCAL) {
            mViewModel.refreshAppliedWallpaper();
        }
    }

    private void loadAd() {
        AppLovinAds.get().loadInterstitialAd();
    }

    private void startCropActivity(String path) {
        File sourceFile = new File(path);
        File destFile = getCropImageFile(sourceFile.getName());
        destFile.getParentFile().mkdirs();
        Uri sourceUri = Uri.fromFile(sourceFile);
        Uri destinationUri = Uri.fromFile(destFile);
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        int height = wm.getDefaultDisplay().getHeight();
        UCrop.of(sourceUri, destinationUri)
                .withAspectRatio(width, height)
                .withMaxResultSize(width, height)
                .start(this);
        Debug.li(getLogTag(), "sourceUri:" + sourceUri + "\n"
                + "destinationUri:" + destinationUri + "\n");
    }

    private File getCropImageFile(String fileName) {
        return new File(mViewModel.getWallpaperImageDir(), System.currentTimeMillis() + "_" + fileName);
    }

    private void showLoadingView(boolean shown) {
        recycler_view.setVisibility(!shown ? View.VISIBLE : View.GONE);
        loading_progress.setVisibility(shown ? View.VISIBLE : View.GONE);
        empty_text.setVisibility(View.GONE);
    }

    private void showEmptyView() {
        recycler_view.setVisibility(View.GONE);
        loading_progress.setVisibility(View.GONE);
        empty_text.setVisibility(View.VISIBLE);
    }

    private void initImagePicker() {
        ImagePicker imagePicker = ImagePicker.getInstance();
        imagePicker.setMultiMode(false);//单选
        imagePicker.setImageLoader(new UILImageLoader());//设置图片加载器
        imagePicker.setShowCamera(false);//显示拍照按钮
        imagePicker.setCrop(false);//允许裁剪（单选才有效）
        this.imagePicker = imagePicker;
    }

    @OnClick(R.id.btn_delete)
    void deleteSelectedWallpaper() {
        new MaterialDialog.Builder(this)
                .content(R.string.dialog_msg_confirm_delete_wallpaper)
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        AnalyticUtils.sendGoogleAnalyticEvent(
                                Const.Analytic.Category.WALLPAPER,
                                Const.Analytic.Action.DELETE_LOCAL_WALLPAPER,
                                null
                        );
                        mViewModel.deleteSelectedWallpapers(getApplicationContext());
                    }
                })
                .show();
    }

    @OnClick(R.id.fab)
    void chooseWallpaperFromGallery() {
        Intent intent = new Intent(this, ImageGridActivity.class);
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.WALLPAPER,
                Const.Analytic.Action.ADD_LOCAL_WALLPAPER,
                null
        );
    }

    private android.databinding.Observable.OnPropertyChangedCallback mSelectModeChangeListener = new android.databinding.Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(android.databinding.Observable sender, int propertyId) {
            btn_delete.setVisibility(mViewModel.isInSelectMode.get() ? View.VISIBLE : View.GONE);
        }
    };

    private android.databinding.Observable.OnPropertyChangedCallback showLoadingPropertyChangedCallback = new android.databinding.Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(android.databinding.Observable observable, int i) {
            showLoadingView(mViewModel.showLoadingView.get());
        }
    };
    private android.databinding.Observable.OnPropertyChangedCallback showEmptyViewPropertyChangedCallback = new android.databinding.Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(android.databinding.Observable observable, int i) {
            if (mViewModel.showEmptyView.get()) {
                showEmptyView();
            }
        }
    };
}