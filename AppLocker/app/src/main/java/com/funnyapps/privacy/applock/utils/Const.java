package com.funnyapps.privacy.applock.utils;

public class Const {

    public static final String NT_CHANNEL_MONITOR = "Monitor";
    public static final String NT_CHANNEL_IMPROVE = "Improve";

    public static final String ACTIVITY_ACTION_LOCAL_THEME_LIST = "com.funny.tools.applocker.ui.LOCAL_THEME_LIST";
    public static final String THEME_MAIN_ACTIVITY_ACTION = "com.funny.tools.applocker.theme";

    /**
     * GooglePlay包名
     */
    public static final String GOOGLE_PLAY_PACKAGE_NAME = "com.android.vending";
    /**
     * GooglePlay地址http前缀
     */
    public static final String GOOGLE_PLAY_PREFIX_HTTP = "http://play.google.com/store/apps/details?id=";
    /**
     * GooglePlay地址https前缀
     */
    public static final String GOOGLE_PLAY_PREFIX_HTTPS = "https://play.google.com/store/apps/details?id=";
    /**
     * GooglePlay地址market前缀
     */
    public static final String GOOGLE_PLAY_PREFIX_MARKET = "market://details?id=";

    public static final String CLOUD_BASE_URL = "https://api.leancloud.cn/1.1/";
    public static final String THEME_CONFIG_FILE_NAME = "index.json";
    public static final String LOCAL_WALLPAPER_DIR_NAME = "wallpaper";

    public static final int CLEAR_PATTERN_DELAY_MILLI = 2000;

    public static final String[] DEFAULT_LOCKED_APPS = new String[]{
            "com.whatsapp", // WhatsApp Messenger
            "com.instagram.android", // Instagram
            "com.twitter.android", // Twitter
            "com.facebook.orca", // Messenger
            "com.facebook.katana", // Facebook
            "com.snapchat.android", // Snapchat
            "com.android.mms", // Messages
            "com.android.messaging", // Messages in Android 6.0
            "com.android.browser", // Browser
    };

    public static class ThemeDrawableName {
        public static final String PREVIEW = "preview";
    }

    public static class PrefFileName {
        public static final String LOCK_PATTERN_INFO = "lock_pattern_info";
        public static final String THEME = "theme_info";
        public static final String WALLPAPER = "wallpaper_info";
    }

    public static class PrefKey {
        public static final String LOCK_PATTERN_SHA1 = "lock_pattern";
        public static final String LOCK_APPS = "lock_apps";
        public static final String ASK_LOCK_NEW_INSTALLED_APP = "ask_lock_new_installed_app";
        public static final String PATTERN_VISIBLE = "pattern_visible";
        public static final String HAPTIC_FEEDBACK = "haptic_feedback";
        public static final String SECURITY_QUESTION = "security_question";
        public static final String SECURITY_ANSWER = "security_answer";
        public static final String LOCK_FREQUENCY = "lock_frequency";
        public static final String NEED_SHOW_RATING_HEADER = "KEY_NEED_SHOW_RATING_HEADER";
        public static final String LAUNCH_TIME = "LAUNCH_TIME";
        public static final String APPLIED_THEME_PACKAGE_NAME = "APPLIED_THEME_PACKAGE_NAME";
        public static final String APPLIED_WALLPAPER_URL = "APPLIED_WALLPAPER_URL";
        public static final String APPLIED_WALLPAPER_TYPE = "APPLIED_WALLPAPER_TYPE";
    }

    public static class LockFrequency {
        public static final int IMMEDIATELY = 0;
        public static final int SCREEN_OFF = 1;
        public static final int AFTER_SCREEN_OFF_FOR_3MINS = 2;
    }

    public static class RequestKey {
        public static final String PACKAGE = "package";
        public static final String LANGUAGE = "language";
        public static final String COUNTRY = "country";
        public static final String VERSION_CODE = "version_code";
        public static final String VERSION_NAME = "version_name";
        public static final String DEBUG = "debug";
    }

    public static class Header {
        public static final String LC_ID = "X-LC-Id";
        public static final String LC_KEY = "X-LC-Key";
        public static final String CONTENT_TYPE = "Content-Type";
    }

    public static class BroadcastAction {
        public static final String THEME_APPLIED = "THEME_APPLIED";
        public static final String CLOSE_ALL_PAGE = "CLOSE_ALL_PAGE";
    }

    public static class Analytic {
        public static class ScreenName {
            public static final String SET_NEW_PATTERN_GUIDE = "设置密码引导页";
            public static final String CHANGE_PATTERN = "修改密码";
            public static final String CONFIRM_PATTERN = "确认密码";
            public static final String MAIN = "主页";
            public static final String SETTINGS = "设置";
            public static final String THEME = "主题";
            public static final String LOCK = "锁定";
            public static final String WALLPAPER_PREVIEW = "壁纸预览";
            public static final String SET_SECURITY_QUESTION = "设置安全问题";
            public static final String ANSWER_SECURITY_QUESTION = "回答安全问题";
            public static final String LOCAL_WALLPAPER_LIST = "本地壁纸列表";
            public static final String REMOTE_WALLPAPER_LIST = "远程壁纸列表";
            public static final String MAIN_APP_LIST = "主页应用列表";
            public static final String SWITCH_DESKTOP_WALLPAPER = "切换桌面壁纸";
        }

        public static class Category {
            public static final String THEME = "主题";
            public static final String LOCK_APP = "想锁定应用";
            public static final String LOCKED_APP = "锁定了应用";
            public static final String LOCKED_PAGE = "锁屏页面";
            public static final String MAIN_PAGE = "主页";
            public static final String GUIDE_PAGE = "引导页";
            public static final String SETTINGS = "设置";
            public static final String WALLPAPER = "壁纸";
        }

        public static class Action {
            public static final String APPLY_THEME = "应用主题";
            public static final String DOWNLOAD_THEME_FROM_GOOGLE_PLAY = "去GooglePlay下载主题";
            public static final String DOWNLOAD_THEME_FROM_CLOUD = "下载主题apk文件";
            public static final String APP = "应用";
            public static final String SWITCH_WALLPAPER = "切换壁纸";
            public static final String CHANGE_PATTERN = "修改图形密码";
            public static final String SKIP_SET_SECURITY_QUESTION = "跳过安全问题";
            public static final String SET_SECURITY_QUESTION = "设置安全问题";
            public static final String CHANGE_SECURITY_QUESTION = "修改安全问题";
            public static final String UPGRADE_APP_LATER = "推迟升级软件";
            public static final String UPGRADE_APP = "升级软件";
            public static final String RATE_APP = "给软件评分";
            public static final String SHARE_APP = "分享软件";
            public static final String OPEN_UNINSTALL_PROTECT = "开启卸载保护";
            public static final String CLOSE_UNINSTALL_PROTECT = "关闭卸载保护";
            public static final String OPEN_HAPTIC_FEEDBACK = "开启触摸震动";
            public static final String CLOSE_HAPTIC_FEEDBACK = "关闭触摸震动";
            public static final String SHOW_PATTERN_PATH = "显示图形轨迹";
            public static final String HIDE_PATTERN_PATH = "隐藏图形轨迹";
            public static final String LOCK_FREQUENCY = "锁定频率";
            public static final String PREVIEW_REMOTE_WALLPAPER = "预览远程壁纸";
            public static final String PREVIEW_LOCAL_WALLPAPER = "预览本地壁纸";
            public static final String ADDED_LOCAL_WALLPAPER = "本地壁纸添加成功";
            public static final String ADD_LOCAL_WALLPAPER = "点击添加本地壁纸按钮";
            public static final String DELETE_LOCAL_WALLPAPER = "删除本地壁纸";
            public static final String SET_AS_LOCK_WALLPAPER = "设为锁屏壁纸";
            public static final String SET_AS_DESKTOP_WALLPAPER = "设为桌面壁纸";
        }
    }

    public static class AdPlatform {
        public static final String APPLOVIN = "applovin";
    }

    public static class AdPlace {
        public static final String INTERSTITIAL_AFTER_UNLOCK = "interstitial_after_unlock";
    }
}