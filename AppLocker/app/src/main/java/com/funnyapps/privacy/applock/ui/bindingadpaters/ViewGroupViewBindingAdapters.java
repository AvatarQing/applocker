package com.funnyapps.privacy.applock.ui.bindingadpaters;

import android.databinding.BindingAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by lq on 2017/1/12.
 */

public class ViewGroupViewBindingAdapters {
    @BindingAdapter("childView")
    public static void addChild(ViewGroup parent, View childView) {
        parent.removeAllViews();
        if (childView != null) {
            parent.addView(childView);
        }
    }
}
