package com.funnyapps.privacy.applock.ui.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.CloudTheme;
import com.funnyapps.privacy.applock.entity.CloudThemeList;
import com.funnyapps.privacy.applock.ui.adapter.RemoteThemeListAdapter;
import com.funnyapps.privacy.applock.ui.adapter.viewholder.TaskItemViewHolder;
import com.funnyapps.privacy.applock.ui.decoration.SpacingDecoration;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.ThemeUtils;
import com.funnyapps.privacy.applock.utils.ToastUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.web.WebClient;
import com.funnyapps.privacy.applock.web.download.theme.ThemeTasksManager;
import com.funnyapps.privacy.applock.web.retrofit.LeanCloudService;
import com.liulishuo.filedownloader.FileDownloader;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lq on 2016/8/22.
 */
public class RemoteThemeListFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.loading_progress)
    View loading_progress;

    RemoteThemeListAdapter mAdapter;
    Handler mHandler = new Handler();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeTasksManager.getImpl().onCreate(new WeakReference<>(this));

        IntentFilter intentFilter = new IntentFilter(Const.BroadcastAction.THEME_APPLIED);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReceiver, intentFilter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_remote_theme_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadThemes();
    }

    @Override
    public void onDestroy() {
        ThemeTasksManager.getImpl().onDestroy();
        mAdapter = null;
        FileDownloader.getImpl().pauseAll();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    public void postNotifyDataChanged() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    void initViews() {
        // 设置列表
        mAdapter = new RemoteThemeListAdapter();
        mAdapter.setAppliedTheme(ThemeUtils.getAppliedThemePackageName(getActivity()));
        recycler_view.setLayoutManager(new GridLayoutManager(getContext(), 2));
        int itemSpacing = getResources().getDimensionPixelOffset(R.dimen.item_spacing);
        recycler_view.addItemDecoration(new SpacingDecoration(itemSpacing, itemSpacing, true));
        recycler_view.setAdapter(mAdapter);
        empty_text.setText(R.string.no_remote_theme);
        mAdapter.setOnThemeClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloudTheme item = (CloudTheme) v.getTag(R.id.data);
                TaskItemViewHolder holder = (TaskItemViewHolder) v.getTag();
                onThemeItemClicked(holder, item);
            }
        });
    }

    void loadThemes() {
        showLoadingView(true);
        LeanCloudService service = WebClient.getLeanCloudService();
        service.loadThemeList().enqueue(new Callback<CloudThemeList>() {
            @Override
            public void onResponse(Call<CloudThemeList> call, Response<CloudThemeList> response) {
                List<CloudTheme> themeList = response.body().results;
                if (themeList != null && !themeList.isEmpty()) {
                    Debug.li(getLogTag(), "First theme : " + themeList.get(0));
                    mAdapter.appendData(themeList);
                    showLoadingView(false);
                } else {
                    Debug.li(getLogTag(), "no online themes");
                    showEmptyView();
                }
            }

            @Override
            public void onFailure(Call<CloudThemeList> call, Throwable t) {
                t.printStackTrace();
                Debug.lw(getLogTag(), "get online theme error." + t);
                showEmptyView();
            }
        });
    }

    void onThemeItemClicked(TaskItemViewHolder holder, CloudTheme item) {
        if (Utils.isAppInstall(getActivity(), item.packageName)) { // 安装了主题包，就询问是否应用
            showConfirmApplyThemeDialog(item.packageName);
        } else if (Utils.isAppInstall(getContext(), Const.GOOGLE_PLAY_PACKAGE_NAME)) { // 没安装主题包，但装了GooglePlay，就进去下载
            String themeAppUrl = Const.GOOGLE_PLAY_PREFIX_HTTPS + item.packageName;
            Utils.openLink(getActivity(), themeAppUrl);
            AnalyticUtils.sendGoogleAnalyticEvent(
                    Const.Analytic.Category.THEME,
                    Const.Analytic.Action.DOWNLOAD_THEME_FROM_GOOGLE_PLAY,
                    item.packageName
            );
        } else if (item.getApkFile().exists()
                && getActivity().getPackageManager().getPackageArchiveInfo(item.getApkPath(), PackageManager.GET_ACTIVITIES) != null) {
            // 下载了主题包，但没安装就去安装
            Utils.installApk(getActivity(), item.getApkFile());
        } else {
            // 没下载主题包，也没装GooglePlay，就在程序里下载
            mAdapter.startDownloadTask(holder);
            AnalyticUtils.sendGoogleAnalyticEvent(
                    Const.Analytic.Category.THEME,
                    Const.Analytic.Action.DOWNLOAD_THEME_FROM_CLOUD,
                    item.packageName
            );
        }
    }

    void showConfirmApplyThemeDialog(final String themePackageName) {
        if (TextUtils.equals(themePackageName, mAdapter.getAppliedTheme())) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.dialog_msg_confirm_use_this_theme)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        applyTheme(themePackageName);
                    }
                })
                .create()
                .show();
    }


    void applyTheme(String themePackageName) {
        ThemeUtils.applyTheme(getActivity(), themePackageName);
        ToastUtils.showToast(R.string.toast_applied_theme);
        mAdapter.setAppliedTheme(themePackageName);
        mAdapter.notifyDataSetChanged();
        LocalBroadcastManager.getInstance(getContext())
                .sendBroadcast(
                        new Intent(Const.BroadcastAction.THEME_APPLIED)
                );
        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.THEME,
                Const.Analytic.Action.APPLY_THEME,
                themePackageName
        );
    }

    private void showLoadingView(boolean shown) {
        recycler_view.setVisibility(!shown ? View.VISIBLE : View.GONE);
        loading_progress.setVisibility(shown ? View.VISIBLE : View.GONE);
    }

    private void showEmptyView() {
        recycler_view.setVisibility(View.GONE);
        loading_progress.setVisibility(View.GONE);
        empty_text.setVisibility(View.VISIBLE);
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TextUtils.equals(action, Const.BroadcastAction.THEME_APPLIED)) {
                if (mAdapter != null) {
                    mAdapter.setAppliedTheme(ThemeUtils.getAppliedThemePackageName(context));
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    };
}