package com.funnyapps.privacy.applock.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.ToastUtils;

import java.util.Set;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by AvatarQing on 2016/8/14.
 */
public class SelectAppsToProtectFragment extends BaseFragment {

    Unbinder unbinder;
    AppListFragment mAppListFragment;
    SelectAppsCallback mSelectAppsCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_apps_to_protect, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        addFragment();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_next)
    void goToNext() {
        Set<String> appSet = mAppListFragment.getSelectedApps();
        if (appSet.isEmpty()) {
            ToastUtils.showToast(R.string.toast_must_selected_at_least_one_app);
        } else {
            if (mSelectAppsCallback != null) {
                mSelectAppsCallback.onAppsSelected(appSet);
            }
        }
    }

    void addFragment() {
        mAppListFragment = new AppListFragment();
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, mAppListFragment, mAppListFragment.getLogTag())
                .commit();
    }

    public void setSelectAppsCallback(SelectAppsCallback callback) {
        this.mSelectAppsCallback = callback;
    }

    public interface SelectAppsCallback {
        void onAppsSelected(Set<String> apps);
    }

}