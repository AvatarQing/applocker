package com.funnyapps.privacy.applock.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ui.fragment.SetPatternFragment;
import com.funnyapps.privacy.applock.ui.widget.SetPatternStepView;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.PatternUtils;
import com.haibison.android.lockpattern.widget.LockPatternView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lq on 2016/8/15.
 */
public class ChangePatternActivity extends BaseActivity {

    static final int STEP_SET_PATTERN = 1;
    static final int STEP_CONFIRM_PATTERN = 2;

    @BindView(R.id.step_view)
    SetPatternStepView step_view;

    int mCurrentStep = STEP_SET_PATTERN;
    SetPatternFragment mSetPatternFragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pattern);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    public void onBackPressed() {
        if (!returnPreviousPage()) {
            super.onBackPressed();
        }
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.CHANGE_PATTERN;
    }

    boolean returnPreviousPage() {
        if (mCurrentStep == STEP_CONFIRM_PATTERN) {
            setCurrentStep(STEP_SET_PATTERN);
            mSetPatternFragment.reset();
            return true;
        }
        return false;
    }

    void initViews() {
        SetPatternFragment setPatternFragment = mSetPatternFragment = new SetPatternFragment();
        setPatternFragment.setPatternSetCallback(new SetPatternFragment.PatternSetCallback() {
            @Override
            public void onRedraw() {
                setCurrentStep(STEP_SET_PATTERN);
            }

            @Override
            public void tryToConfirmPattern() {
                setCurrentStep(STEP_CONFIRM_PATTERN);
            }

            @Override
            public void onPatternConfirmed(List<LockPatternView.Cell> pattern) {
                String patternInSha1String = PatternUtils.patternToSha1String(pattern);
                PatternLockUtils.setPattern(getApplicationContext(), patternInSha1String);
                setResult(RESULT_OK);
                finish();
            }
        });
        setPatternFragment.setConfirmOkBtnText(R.string.btn_finish);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, setPatternFragment, setPatternFragment.getLogTag())
                .commitAllowingStateLoss();
    }

    void setCurrentStep(int step) {
        mCurrentStep = step;
        step_view.setCurrentStep(step);
    }

}