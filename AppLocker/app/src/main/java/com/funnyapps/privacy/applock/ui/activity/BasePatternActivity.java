/*
 * Copyright (c) 2015 Zhang Hai <Dreaming.in.Code.ZH@Gmail.com>
 * All Rights Reserved.
 */

package com.funnyapps.privacy.applock.ui.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ViewGroup;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.haibison.android.lockpattern.widget.LockPatternView;

public abstract class BasePatternActivity extends BaseActivity
        implements LockPatternView.OnPatternListener {

    private static final int CLEAR_PATTERN_DELAY_MILLI = 2000;

    private final Runnable clearPatternRunnable = new Runnable() {
        public void run() {
            // clearPattern() resets display mode to DisplayMode.Correct.
            getPatternView().clearPattern();
        }
    };

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initPatternView();
    }

    protected void initPatternView() {
        getPatternView().setInStealthMode(PatternLockUtils.isStealthModeEnabled(this));
        getPatternView().setHapticFeedbackEnabled(PatternLockUtils.isHapticFeedbackEnabled(this));
        getPatternView().setTactileFeedbackEnabled(PatternLockUtils.isHapticFeedbackEnabled(this));
        getPatternView().setOnPatternListener(this);

        // LOCK PATTERN VIEW
        switch (getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
            case Configuration.SCREENLAYOUT_SIZE_XLARGE: {
                final int size = getResources().getDimensionPixelSize(
                        R.dimen.alp_42447968_lockpatternview_size);
                ViewGroup.LayoutParams lp = getPatternView().getLayoutParams();
                lp.width = size;
                lp.height = size;
                getPatternView().setLayoutParams(lp);
                break;
            }// LARGE / XLARGE
        }
    }

    protected abstract LockPatternView getPatternView();

    protected void removeClearPatternRunnable() {
        getPatternView().removeCallbacks(clearPatternRunnable);
    }

    protected void postClearPatternRunnable() {
        removeClearPatternRunnable();
        getPatternView().postDelayed(clearPatternRunnable, CLEAR_PATTERN_DELAY_MILLI);
    }

}