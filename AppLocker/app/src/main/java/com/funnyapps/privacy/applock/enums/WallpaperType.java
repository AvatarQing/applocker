package com.funnyapps.privacy.applock.enums;

/**
 * Created by lq on 2016/12/2.
 */

public enum WallpaperType {
    REMOTE,
    LOCAL,
}