package com.funnyapps.privacy.applock.utils;

import android.text.TextUtils;

import com.funnyapps.privacy.applock.BuildConfig;
import com.funnyapps.privacy.applock.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by lq on 2016/11/20.
 */

public class AnalyticUtils {

    public static void sendGoogleAnalyticErrorEvent(String desc) {
        Tracker tracker = MyApplication.getInstance().getDefaultTracker();
        tracker.send(new HitBuilders.ExceptionBuilder()
                .setDescription(desc)
                .setFatal(false)
                .build());
    }

    public static void sendGoogleAnalyticEvent(String category, String action) {
        sendGoogleAnalyticEvent(category, action, null);
    }

    public static void sendGoogleAnalyticEvent(String category, String action, String label) {
        if (!needSendEvent()) {
            return;
        }
        Tracker tracker = MyApplication.getInstance().getDefaultTracker();
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

    private static boolean needSendEvent() {
        return TextUtils.equals(BuildConfig.BUILD_TYPE, "release");
    }

}