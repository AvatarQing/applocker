package com.funnyapps.privacy.applock.ui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.CloudTheme;
import com.funnyapps.privacy.applock.ui.widget.DynamicHeightImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by AvatarQing on 2016/8/31.
 */
public class TaskItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.theme_preview)
    public DynamicHeightImageView theme_preview;
    @BindView(R.id.theme_name)
    public TextView theme_name;
    @BindView(R.id.applied)
    public View applied;
    @BindView(R.id.preview_container)
    public FrameLayout preview_container;
    @BindView(R.id.download_progress)
    public ProgressBar taskPb;

    public int position;
    public int id;
    public CloudTheme theme;

    public TaskItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void update(final int id, final int position) {
        this.id = id;
        this.position = position;
    }

    /** 下载完成，更新UI状态 */
    public void updateDownloaded() {
        taskPb.setMax(1);
        taskPb.setProgress(1);
        taskPb.setVisibility(View.INVISIBLE);
    }

    public void updateNotDownloaded(final int status, final long sofar, final long total) {
        if (sofar > 0 && total > 0) {
            final float percent = sofar / (float) total;
            taskPb.setMax(100);
            taskPb.setProgress((int) (percent * 100));
        } else {
            taskPb.setMax(1);
            taskPb.setProgress(0);
        }
        taskPb.setVisibility(View.INVISIBLE);
    }

    public void updateDownloading(final int status, final long sofar, final long total) {
        final float percent = sofar / (float) total;
        taskPb.setMax(100);
        taskPb.setProgress((int) (percent * 100));
        taskPb.setVisibility(View.VISIBLE);
    }

}
