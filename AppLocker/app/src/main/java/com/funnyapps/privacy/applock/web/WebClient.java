package com.funnyapps.privacy.applock.web;

import android.content.Context;

import com.funnyapps.privacy.applock.MyApplication;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.web.interceptor.LogInterceptor;
import com.funnyapps.privacy.applock.web.retrofit.LeanCloudService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.fastjson.FastJsonConverterFactory;

/**
 * Created by AvatarQing on 2016/8/18.
 */
public class WebClient {

    private static LeanCloudService mLeanCloudService;

    public static LeanCloudService getLeanCloudService() {
        if (mLeanCloudService == null) {
            final Context context = MyApplication.getInstance();
            Interceptor headerInterceptor = new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request request = chain.request();
                    request = request.newBuilder()
                            .header(Const.Header.LC_ID, context.getString(R.string.lean_cloud_app_id))
                            .header(Const.Header.LC_KEY, context.getString(R.string.lean_cloud_app_key))
                            .header(Const.Header.CONTENT_TYPE, "application/json")
                            .build();
                    return chain.proceed(request);
                }
            };
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(headerInterceptor)
                    .addInterceptor(new LogInterceptor())
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Const.CLOUD_BASE_URL)
                    .client(client)
                    .addConverterFactory(FastJsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            mLeanCloudService = retrofit.create(LeanCloudService.class);
        }
        return mLeanCloudService;
    }

}