package com.funnyapps.privacy.applock.receiver;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.funnyapps.privacy.applock.service.MonitorService;

/**
 * Created by AvatarQing on 2016/6/1.
 */
public class WakeReceiver extends BaseBroadcastReceiver {

    public static final String ACTION_WAKE = "com.my.wake.gray";

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            return;
        }
        if (ACTION_WAKE.equals(action)) {
            MonitorService.start(context);
        }
    }
}