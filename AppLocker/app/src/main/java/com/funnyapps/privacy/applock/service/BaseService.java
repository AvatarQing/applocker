package com.funnyapps.privacy.applock.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.abc.common.tool.Debug;


/**
 * Created by AvatarQing on 2016/6/1.
 */
public class BaseService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        Debug.li(getLogTag(), this + "->onCreate()");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Debug.li(getLogTag(), this + "->onStartCommand()" + "\n"
                + "intent:" + intent + "\n"
                + "flags:" + flags + "\n"
                + "startId:" + startId + "\n"
        );
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Debug.li(getLogTag(), this + "->onDestroy()");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Debug.li(getLogTag(), this + "->onBind()");
        return null;
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }
}
