package com.funnyapps.privacy.applock.ui.viewmodel;

import android.view.View;

import com.funnyapps.privacy.applock.entity.AppInfo;
import com.mixiaoxiao.smoothcompoundbutton.SmoothCompoundButton;

/**
 * Created by lq on 2016/11/17.
 */

public interface IAppListItemViewModel {
    void onItemClick(View itemView, AppInfo item);

    void onCheckedChanged(SmoothCompoundButton checkbox, boolean isChecked, AppInfo item);
}
