package com.funnyapps.privacy.applock.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import com.afollestad.materialdialogs.MaterialDialog;
import com.funnyapps.privacy.applock.R;
import com.tbruyelle.rxpermissions.RxPermissions;

/**
 * @author AvatarQing
 */
public class PermissionHelper {
    private Activity activity;
    private RxPermissions rxPermissions;

    public PermissionHelper(Activity activity) {
        this.activity = activity;
    }

    public boolean hasGrantedPermissions() {
        if (rxPermissions == null) {
            rxPermissions = new RxPermissions(activity);
        }
        return rxPermissions.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public void showRequestPermissions() {
        if (rxPermissions == null) {
            rxPermissions = new RxPermissions(activity);
        }
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        rxPermissions.request(permission)
                .filter(granted -> !granted)
                .flatMap(ignored -> rxPermissions.shouldShowRequestPermissionRationale(activity, permission))
                .subscribe(
                        shouldShowRequestPermissionRationale -> {
                            if (shouldShowRequestPermissionRationale) {
                                // 用户选择了拒绝但没有选择不再提醒
                                new MaterialDialog.Builder(activity)
                                        .cancelable(false)
                                        .title(R.string.permission_denied)
                                        .content(R.string.permission_description)
                                        .positiveText(R.string.btn_retry_grant)
                                        .negativeText(R.string.btn_deny)
                                        .onPositive((dialog, which) -> showRequestPermissions())
                                        .build()
                                        .show();
                            } else {
                                // 用户选择了拒绝并且不再提醒
                                MaterialDialog reopenDialog = new MaterialDialog.Builder(activity)
                                        .cancelable(false)
                                        .title(R.string.permission_denied)
                                        .content(R.string.permission_reopen_method)
                                        .positiveText(R.string.btn_go)
                                        .negativeText(R.string.btn_deny)
                                        .onPositive((dialog, which) -> {
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                                            intent.setData(uri);
                                            activity.startActivityForResult(intent, 0);
                                        })
                                        .build();
                                reopenDialog.show();
                            }
                        },
                        Throwable::printStackTrace);
    }
}
