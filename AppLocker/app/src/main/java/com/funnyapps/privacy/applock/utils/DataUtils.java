package com.funnyapps.privacy.applock.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.abc.common.entity.CloudResult;
import com.abc.common.tool.Debug;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.funnyapps.privacy.applock.MyApplication;
import com.funnyapps.privacy.applock.entity.AdConfig;
import com.funnyapps.privacy.applock.entity.UpgradeConfig;
import com.funnyapps.privacy.applock.entity.WallPaper;

import java.util.concurrent.TimeUnit;

/**
 * Created by AvatarQing on 2016/4/27.
 */
public class DataUtils {

    private static final String PREF_NAME_CLOUD_DATA = "PREF_NAME_CLOUD_DATA";
    private static final String PREF_KEY_JSON_WALLPAPER = "PREF_KEY_JSON_WALLPAPER";
    private static final String PREF_KEY_JSON_AD_CONFIG = "PREF_KEY_JSON_AD_CONFIG";
    private static final String PREF_KEY_JSON_UPGRADE_CONFIG = "PREF_KEY_JSON_UPGRADE_CONFIG";
    private static final String PREF_KEY_CACHE_TIME_FOR_WALLPAPER = "PREF_KEY_CACHE_TIME_FOR_WALLPAPER";
    private static final String PREF_KEY_CACHE_TIME_FOR_AD_CONFIG = "PREF_KEY_CACHE_TIME_FOR_AD_CONFIG";
    private static final String PREF_KEY_CACHE_TIME_FOR_UPGRADE_CONFIG = "PREF_KEY_CACHE_TIME_FOR_UPGRADE_CONFIG";
    private static final String PREF_KEY_HAS_CREATED_SHORTCUT = "PREF_KEY_HAS_CREATED_SHORTCUT";
    private static final String PREF_KEY_AD_DISPLAY_COUNT = "AdDisplayCount_";
    private static final String PREF_KEY_AD_LAST_DISPLAY_TIME = "AdLastDisplayTime_";

    private static String getLogTag() {
        return DataUtils.class.getSimpleName();
    }

    public static boolean hasCreatedShortcut() {
        return getPrefs().getBoolean(PREF_KEY_HAS_CREATED_SHORTCUT, false);
    }

    public static void setHasCreatedShortcut(boolean created) {
        getPrefs().edit().putBoolean(PREF_KEY_HAS_CREATED_SHORTCUT, created).apply();
    }

    public static void cacheWallPapers(CloudResult<WallPaper> data) {
        cacheCloudData(PREF_KEY_JSON_WALLPAPER, PREF_KEY_CACHE_TIME_FOR_WALLPAPER, data);
    }

    public static CloudResult<WallPaper> getCachedWallPapers() {
        if (isCachedCloudDataValid(PREF_KEY_CACHE_TIME_FOR_WALLPAPER)) {
            String json = getStringFromPrefs(PREF_KEY_JSON_WALLPAPER);
            if (!TextUtils.isEmpty(json)) {
                CloudResult<WallPaper> result = JSON.parseObject(json, new TypeReference<CloudResult<WallPaper>>() {
                });
                Debug.li(getLogTag(), "getCachedWallPapers()-> " + result);
                return result;
            }
        }
        return null;
    }

    public static void cacheAdConfig(CloudResult<AdConfig> data) {
        cacheCloudData(PREF_KEY_JSON_AD_CONFIG, PREF_KEY_CACHE_TIME_FOR_AD_CONFIG, data);
    }

    public static CloudResult<AdConfig> getCacheAdConfig() {
        if (isCachedCloudDataValid(PREF_KEY_CACHE_TIME_FOR_AD_CONFIG)) {
            String json = getStringFromPrefs(PREF_KEY_JSON_AD_CONFIG);
            if (!TextUtils.isEmpty(json)) {
                CloudResult<AdConfig> result = JSON.parseObject(json, new TypeReference<CloudResult<AdConfig>>() {
                });
                Debug.li(getLogTag(), "getCacheAdConfig()-> " + result);
                return result;
            }
        }
        return null;
    }

    public static int getAdDisplayedCount(String adPlace) {
        String key = PREF_KEY_AD_DISPLAY_COUNT + adPlace;
        return getPrefs().getInt(key, 0);
    }

    public static void setAdDisplayCount(String adPlace, int count) {
        String key = PREF_KEY_AD_DISPLAY_COUNT + adPlace;
        getPrefs().edit().putInt(key, count).apply();
    }

    public static long getAdLastDisplayTime(String adPlace) {
        String key = PREF_KEY_AD_LAST_DISPLAY_TIME + adPlace;
        return getPrefs().getLong(key, 0);
    }

    public static void setAdLastDisplayTime(String adPlace, long time) {
        String key = PREF_KEY_AD_LAST_DISPLAY_TIME + adPlace;
        getPrefs().edit().putLong(key, time).apply();
    }

    public static void cacheUpgradeConfig(CloudResult<UpgradeConfig> data) {
        cacheCloudData(PREF_KEY_JSON_UPGRADE_CONFIG, PREF_KEY_CACHE_TIME_FOR_UPGRADE_CONFIG, data);
    }

    public static CloudResult<UpgradeConfig> getCacheUpgradeConfig() {
        if (isCachedCloudDataValid(PREF_KEY_CACHE_TIME_FOR_UPGRADE_CONFIG)) {
            String json = getStringFromPrefs(PREF_KEY_JSON_UPGRADE_CONFIG);
            if (!TextUtils.isEmpty(json)) {
                CloudResult<UpgradeConfig> result = JSON.parseObject(json, new TypeReference<CloudResult<UpgradeConfig>>() {
                });
                Debug.li(getLogTag(), "getCacheUpgradeConfig()-> " + result);
                return result;
            }
        }
        return null;
    }

    private static <T> void cacheCloudData(String dataKey, String timeKey, CloudResult<T> data) {
        if (data == null || data.results == null || data.results.isEmpty()) {
            return;
        }
        setLongToPrefs(timeKey, System.currentTimeMillis());
        setStringToPrefs(dataKey, JSON.toJSONString(data));
        Debug.li(getLogTag(), "cacheCloudData()->" + data);
    }

    private static boolean isCachedCloudDataValid(String timeKey) {
        long cachedTime = getLongFromPrefs(timeKey);
        long interval = System.currentTimeMillis() - cachedTime;
        return interval < TimeUnit.DAYS.toMillis(1);
    }

    private static SharedPreferences getPrefs() {
        return MyApplication.getInstance().getSharedPreferences(PREF_NAME_CLOUD_DATA, Context.MODE_PRIVATE);
    }

    private static String getStringFromPrefs(String key) {
        return getPrefs().getString(key, null);
    }

    private static long getLongFromPrefs(String key) {
        return getPrefs().getLong(key, 0);
    }

    private static void setStringToPrefs(String key, String value) {
        getPrefs().edit().putString(key, value).apply();
    }

    private static void setLongToPrefs(String key, long value) {
        getPrefs().edit().putLong(key, value).apply();
    }

}