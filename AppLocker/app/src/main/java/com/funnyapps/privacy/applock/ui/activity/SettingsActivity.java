package com.funnyapps.privacy.applock.ui.activity;

import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ad.AppLovinAds;
import com.funnyapps.privacy.applock.receiver.MyDeviceAdminReceiver;
import com.funnyapps.privacy.applock.ui.fragment.RequestDeviceAdminDialogFragment;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.mixiaoxiao.smoothcompoundbutton.SmoothCompoundButton;
import com.mixiaoxiao.smoothcompoundbutton.SmoothSwitch;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by AvatarQing on 2016/7/6.
 */
public class SettingsActivity extends BaseActivity {

    private static final int REQUEST_CODE_ANSWER_SECURITY_QUESTION = 2;
    private static final int REQUEST_CODE_CONFIRM_CLOSE_UNINSTALL_PROTECT = 3;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.switch_hide_pattern_path)
    SmoothSwitch switch_hide_pattern_path;
    @BindView(R.id.switch_haptic_feedback)
    SmoothSwitch switch_haptic_feedback;
    @BindView(R.id.switch_uninstall_protection)
    SmoothSwitch switch_uninstall_protection;
    @BindView(R.id.switch_ask_lock_new_installed_app)
    SmoothSwitch switch_ask_lock_new_installed_app;
    @BindView(R.id.tv_lock_frequency)
    TextView tv_lock_frequency;
    @BindView(R.id.banner_ad_container)
    FrameLayout banner_ad_container;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        initViews();
        registerBroadcastReceivers();
        loadAds();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterBroadcastReceivers();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_CONFIRM_CLOSE_UNINSTALL_PROTECT: {
                if (resultCode == RESULT_OK) {
                    ComponentName componentName = new ComponentName(this, MyDeviceAdminReceiver.class);
                    DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
                    devicePolicyManager.removeActiveAdmin(componentName);
                }
            }
            break;
            case REQUEST_CODE_ANSWER_SECURITY_QUESTION: {
                if (resultCode == RESULT_OK) {
                    AnalyticUtils.sendGoogleAnalyticEvent(
                            Const.Analytic.Category.SETTINGS,
                            Const.Analytic.Action.CHANGE_SECURITY_QUESTION
                    );
                    goToSetSecurityQuestionPage();
                }
            }
            break;
            default:
                break;
        }
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.SETTINGS;
    }

    private void loadAds() {
        AppLovinAds.get().getDisposableBannerAdLoadResultLiveData().observe(this, loaded -> {
            if (loaded != null && loaded) {
                View bannerAdView = AppLovinAds.get().getNewBannerAdView();
                if (bannerAdView != null) {
                    banner_ad_container.addView(bannerAdView);
                }
            }
        });
        AppLovinAds.get().getDisposableInterstitialAdLoadResultLiveData().observe(this, loaded -> AppLovinAds.get().showInterstitialAd());
        AppLovinAds.get().loadBannerAd();
        AppLovinAds.get().loadInterstitialAd();
    }

    private void registerBroadcastReceivers() {
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(MyDeviceAdminReceiver.ACTION_ADMIN_ENABLED);
        iFilter.addAction(MyDeviceAdminReceiver.ACTION_ADMIN_DISABLED);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mBroadcastReceiver, iFilter);
    }

    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mBroadcastReceiver);
    }

    private void initViews() {
        title.setText(R.string.title_settings);
        // 提醒锁定新安装的应用
        switch_ask_lock_new_installed_app.setChecked(PatternLockUtils.isAskLockNewInstalledAppEnabled(this));
        // 隐藏轨迹
        switch_hide_pattern_path.setChecked(PatternLockUtils.isStealthModeEnabled(this));
        // 触摸反馈
        switch_haptic_feedback.setChecked(PatternLockUtils.isHapticFeedbackEnabled(this));
        // 锁定频率
        switch (PatternLockUtils.getLockFrequency(this)) {
            case Const.LockFrequency.SCREEN_OFF: {
                tv_lock_frequency.setText(R.string.lock_mode_after_screen_off);
            }
            break;
            case Const.LockFrequency.AFTER_SCREEN_OFF_FOR_3MINS: {
                tv_lock_frequency.setText(R.string.lock_mode_after_screen_off_3m);
            }
            break;
            case Const.LockFrequency.IMMEDIATELY: {
                tv_lock_frequency.setText(R.string.lock_mode_immediately);
            }
            break;
            default:
                break;
        }
        // 卸载保护
        updateUninstallProtectionUi();
        switch_hide_pattern_path.setOnCheckedChangeListener(new SmoothCompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCompoundButton smoothCompoundButton, boolean isChecked) {
                PatternLockUtils.setStealthModeEnabled(getApplicationContext(), isChecked);
            }
        });
        switch_haptic_feedback.setOnCheckedChangeListener(new SmoothCompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCompoundButton smoothCompoundButton, boolean isChecked) {
                PatternLockUtils.setHapticFeedbackEnabled(getApplicationContext(), isChecked);
            }
        });
        switch_ask_lock_new_installed_app.setOnCheckedChangeListener(new SmoothCompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCompoundButton smoothCompoundButton, boolean isChecked) {
                PatternLockUtils.setAskLockNewInstalledAppEnabled(getApplicationContext(), isChecked);
            }
        });
        switch_uninstall_protection.setOnCheckedChangeListener(new SmoothCompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCompoundButton smoothCompoundButton, boolean checked) {
                smoothCompoundButton.setChecked(!checked, true, false);
                toggleUninstallProtection();
            }
        });
    }

    @OnClick(R.id.item_change_pattern)
    void changePattern() {
        Intent intent = new Intent(this, ChangePatternActivity.class);
        startActivity(intent);

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.SETTINGS,
                Const.Analytic.Action.CHANGE_PATTERN
        );
    }

    @OnClick(R.id.item_ask_lock_new_installed_app)
    void toggleAskLocknewInstalledApp() {
        boolean newState = !switch_ask_lock_new_installed_app.isChecked();
        switch_ask_lock_new_installed_app.setChecked(newState);
    }

    @OnClick(R.id.item_hide_pattern_path)
    void toggleHidePatternPath() {
        boolean newState = !switch_hide_pattern_path.isChecked();
        switch_hide_pattern_path.setChecked(newState);

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.SETTINGS,
                newState ? Const.Analytic.Action.SHOW_PATTERN_PATH : Const.Analytic.Action.HIDE_PATTERN_PATH
        );
    }

    @OnClick(R.id.item_haptic_feedback)
    void toggleHapticFeedback() {
        boolean newState = !switch_haptic_feedback.isChecked();
        switch_haptic_feedback.setChecked(newState);

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.SETTINGS,
                newState ? Const.Analytic.Action.OPEN_HAPTIC_FEEDBACK : Const.Analytic.Action.CLOSE_HAPTIC_FEEDBACK
        );
    }

    @OnClick(R.id.item_lock_frequency)
    void changeLockFrequency() {
        final String[] options = new String[]{
                getString(R.string.lock_mode_immediately),
                getString(R.string.lock_mode_after_screen_off),
                getString(R.string.lock_mode_after_screen_off_3m),
        };
        final int checkedItem = PatternLockUtils.getLockFrequency(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_simple_text, R.id.text, options) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View itemView = super.getView(position, convertView, parent);
                CheckBox checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                checkBox.setChecked(checkedItem == position);
                return itemView;
            }
        };
        new AlertDialog.Builder(this)
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AnalyticUtils.sendGoogleAnalyticEvent(
                                Const.Analytic.Category.SETTINGS,
                                Const.Analytic.Action.LOCK_FREQUENCY,
                                "" + which
                        );

                        tv_lock_frequency.setText(options[which]);
                        PatternLockUtils.setLockFrequency(SettingsActivity.this, which);
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    @OnClick(R.id.item_modify_security_question)
    void modifySecurityQuestion() {
        if (PatternLockUtils.hasSetSecurityQuestion(this)) {
            goToAnswerSecurityQuestionPage();
        } else {
            goToSetSecurityQuestionPage();
        }
    }

    private void goToAnswerSecurityQuestionPage() {
        Intent intent = new Intent(this, AnswerSecurityQuestionActivity.class);
        startActivityForResult(intent, REQUEST_CODE_ANSWER_SECURITY_QUESTION);
    }

    private void goToSetSecurityQuestionPage() {
        Intent intent = new Intent(this, SetSecurityQuestionActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.item_uninstall_protection)
    void toggleUninstallProtection() {
        ComponentName componentName = new ComponentName(this, MyDeviceAdminReceiver.class);
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        boolean isAdminActive = devicePolicyManager.isAdminActive(componentName);
        if (!isAdminActive) {
            RequestDeviceAdminDialogFragment.show(this);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.title_tips)
                    .setMessage(R.string.uninstall_protect_close_desc)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            PatternLockUtils.confirmPattern(SettingsActivity.this, REQUEST_CODE_CONFIRM_CLOSE_UNINSTALL_PROTECT);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .create()
                    .show();
        }
        updateUninstallProtectionUi();
    }

    @OnClick(R.id.item_rating)
    void rateApp() {
        String url = Const.GOOGLE_PLAY_PREFIX_HTTPS + getPackageName();
        Utils.openLink(this, url);

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.SETTINGS,
                Const.Analytic.Action.RATE_APP
        );
    }

    @OnClick(R.id.item_share_app)
    void shareApp() {
        String url = Const.GOOGLE_PLAY_PREFIX_HTTPS + getPackageName();
        String shareText = getString(R.string.share_content, url);
        Utils.shareText(this, getString(R.string.title_share_app), shareText);

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.SETTINGS,
                Const.Analytic.Action.SHARE_APP
        );
    }

    @OnClick(R.id.item_privacy_policy)
    void browsePrivacyPolicy() {
        String link = "https://funnycoolapps.blogspot.com/2016/10/applockfunny-cool-apps.html";
        Utils.openLink(this, link);
    }

    @OnLongClick(R.id.item_rating)
    boolean toggleLog() {
        boolean newState = !Debug.isLogEnabled();
        Debug.enable(newState);
        Debug.li(getLogTag(), newState ? "Opened Log" : "Closed Log");
        return true;
    }

    private void updateUninstallProtectionUi() {
        switch_uninstall_protection.setChecked(isAdminActive(), false, false);
    }

    private boolean isAdminActive() {
        ComponentName componentName = new ComponentName(this, MyDeviceAdminReceiver.class);
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        return devicePolicyManager.isAdminActive(componentName);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Debug.li(getLogTag(), "action:" + action);
            if (TextUtils.isEmpty(action)) {
                return;
            }
            if (action.equals(MyDeviceAdminReceiver.ACTION_ADMIN_ENABLED)
                    || action.equals(MyDeviceAdminReceiver.ACTION_ADMIN_DISABLED)) {
                boolean isOpen = action.equals(MyDeviceAdminReceiver.ACTION_ADMIN_ENABLED);
                AnalyticUtils.sendGoogleAnalyticEvent(
                        Const.Analytic.Category.SETTINGS,
                        isOpen ? Const.Analytic.Action.OPEN_UNINSTALL_PROTECT : Const.Analytic.Action.CLOSE_UNINSTALL_PROTECT
                );
                updateUninstallProtectionUi();
            }
        }
    };

}