package com.funnyapps.privacy.applock.entity;

import android.os.Environment;

import com.liulishuo.filedownloader.util.FileDownloadUtils;

import java.io.File;

/**
 * Created by lq on 2016/8/30.
 */
public class CloudTheme extends BaseCloudItem {

    public String themeNameEn;
    public String packageName;
    public ApkBean apk;
    public String themeId;
    public PreviewBean preview;
    public String themeNameZh;

    public static class ApkBean {
        public String __type;
        public String id;
        public String name;
        public String url;
    }

    public static class PreviewBean {
        public String __type;
        public String id;
        public String name;
        public String url;
    }

    public int getDownloadId() {
        return FileDownloadUtils.generateId(getApkUrl(), getApkPath());
    }

    public File getApkFile() {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        return new File(dir, packageName + ".apk");
    }

    public String getApkPath() {
        return getApkFile().getAbsolutePath();
    }

    public String getApkUrl() {
        return apk.url;
    }

    @Override
    public String toString() {
        return "CloudTheme{" +
                "themeNameEn='" + themeNameEn + '\'' +
                ", packageName='" + packageName + '\'' +
                ", apk=" + apk +
                ", themeId='" + themeId + '\'' +
                ", preview=" + preview +
                ", themeNameZh='" + themeNameZh + '\'' +
                ", objectId='" + objectId + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}