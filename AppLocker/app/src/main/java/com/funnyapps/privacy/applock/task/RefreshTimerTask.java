package com.funnyapps.privacy.applock.task;

import android.content.Context;
import android.text.TextUtils;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.Utils;

import java.util.TimerTask;

public class RefreshTimerTask extends TimerTask {

    public interface RefreshCallback {
        void onTopAppChanged(String preTopAppPkgName, String curTopAppPkgName);
    }

    private Context mContext;
    private RefreshCallback mRefreshCallback;
    private String mPreTopAppPkgName = "";

    public RefreshTimerTask(Context context) {
        mContext = context;
    }

    public void setRefreshCallback(RefreshCallback callback) {
        mRefreshCallback = callback;
    }

    @Override
    public void run() {
        String curTopAppPkgName = Utils.getTopAppPackageNameCompat(mContext);
        boolean topAppChanged = !TextUtils.isEmpty(curTopAppPkgName) && !mPreTopAppPkgName.equals(curTopAppPkgName);
        if (topAppChanged && mRefreshCallback != null) {
            Debug.li(getLogTag(), "[" + "preTopAppPkgName:" + mPreTopAppPkgName + "]"
                    + "[" + "curTopAppPkgName:" + curTopAppPkgName + "]"
            );
            mRefreshCallback.onTopAppChanged(mPreTopAppPkgName, curTopAppPkgName);
            mPreTopAppPkgName = curTopAppPkgName;
        }
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }
}