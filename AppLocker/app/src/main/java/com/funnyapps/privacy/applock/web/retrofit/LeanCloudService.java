package com.funnyapps.privacy.applock.web.retrofit;

import com.abc.common.entity.CloudResult;
import com.funnyapps.privacy.applock.entity.AdConfig;
import com.funnyapps.privacy.applock.entity.CloudThemeList;
import com.funnyapps.privacy.applock.entity.UpgradeConfig;
import com.funnyapps.privacy.applock.entity.WallPaper;

import retrofit2.Call;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by lq on 2016/8/30.
 */
public interface LeanCloudService {
    @GET("classes/theme?order=-createdAt")
    Call<CloudThemeList> loadThemeList();

    @GET("classes/WallPaper?order=-createdAt")
    Observable<CloudResult<WallPaper>> loadWallPaperList();

    @GET("classes/AdConfig")
    Observable<CloudResult<AdConfig>> loadAdConfig();

    @GET("classes/UpgradeConfig")
    Observable<CloudResult<UpgradeConfig>> loadUpgradeConfig();
}