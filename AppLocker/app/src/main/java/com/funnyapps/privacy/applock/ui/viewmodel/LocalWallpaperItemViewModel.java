package com.funnyapps.privacy.applock.ui.viewmodel;

import android.databinding.ObservableBoolean;

import com.funnyapps.privacy.applock.entity.LocalWallPaper;
import com.funnyapps.privacy.applock.ui.bindingadpaters.command.ReplyCommand;
import com.funnyapps.privacy.applock.ui.bindingadpaters.command.ResponseCommand;

import rx.functions.Action0;
import rx.functions.Func0;

/**
 * Created by Administrator on 2017/1/21.
 */

public class LocalWallpaperItemViewModel {

    public final LocalWallPaper wallpaper;
    public final ObservableBoolean isInSelectMode;
    public final ReplyCommand itemClickCommand = new ReplyCommand(new Action0() {
        @Override
        public void call() {
            mParentViewModel.onItemClick(wallpaper);
        }
    });
    public final ResponseCommand<Void, Boolean> itemLongClickCommand = new ResponseCommand<>(new Func0<Boolean>() {
        @Override
        public Boolean call() {
            mParentViewModel.onItemLongClick(wallpaper);
            return true;
        }
    });

    private LocalWallpaperViewModel mParentViewModel;

    public LocalWallpaperItemViewModel(LocalWallPaper wallPaper, ObservableBoolean isInSelectMode, LocalWallpaperViewModel parentViewModel) {
        this.wallpaper = wallPaper;
        this.isInSelectMode = isInSelectMode;
        this.mParentViewModel = parentViewModel;
    }

}