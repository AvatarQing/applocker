package com.funnyapps.privacy.applock.ui.viewmodel;

import android.content.Context;
import android.view.View;

import com.funnyapps.privacy.applock.ui.adapter.MainAppListAdapter;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.Utils;

/**
 * Created by AvatarQing on 2016/11/10.
 */

public class RateHeaderViewModel {

    public interface Callback {
        void onRateHeaderHidden();
    }

    boolean showRateHeader = true;
    MainAppListAdapter mAdapter;
    Callback mCallback;

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    public RateHeaderViewModel(Context context, MainAppListAdapter adapter) {
        this.showRateHeader = PatternLockUtils.needShowRatingHeader(context);
        this.mAdapter = adapter;
    }

    public boolean isShowRatingHeader() {
        return showRateHeader;
    }

    public void hideRateHeader() {
        showRateHeader = false;
        mAdapter.notifyItemRemoved(0);
        if (mCallback != null) {
            mCallback.onRateHeaderHidden();
        }
    }

    public void rateAppAndHideRateHeader(View view) {
        Context context = view.getContext();
        String url = Const.GOOGLE_PLAY_PREFIX_HTTPS + context.getPackageName();
        Utils.openLink(context, url);
        PatternLockUtils.setIfShowRatingHeader(context, false);
        hideRateHeader();

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.MAIN_PAGE,
                Const.Analytic.Action.RATE_APP
        );
    }

}
