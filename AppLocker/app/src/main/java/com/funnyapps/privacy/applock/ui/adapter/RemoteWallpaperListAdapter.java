package com.funnyapps.privacy.applock.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.funnyapps.privacy.applock.databinding.ItemWallpaperBinding;
import com.funnyapps.privacy.applock.entity.WallPaper;
import com.funnyapps.privacy.applock.ui.adapter.viewholder.AdViewHolder;
import com.funnyapps.privacy.applock.ui.viewmodel.RemoteWallpaperViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lq on 2016/11/28.
 */

public class RemoteWallpaperListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final int ITEM_TYPE_NORMAL = 1;
    static final int ITEM_TYPE_AD = 2;

    private List<WallPaper> mDataList = new ArrayList<>();
    private RemoteWallpaperViewModel mViewModel;
    private View mNativeAdView;

    public RemoteWallpaperListAdapter(RemoteWallpaperViewModel viewModel) {
        mViewModel = viewModel;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && hasAdHeader()) {
            return ITEM_TYPE_AD;
        }
        return ITEM_TYPE_NORMAL;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_NORMAL: {
                ItemWallpaperBinding binding = ItemWallpaperBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new RemoteWallpaperViewHolder(binding);
            }
            case ITEM_TYPE_AD: {
                View view = mNativeAdView;
                if (view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                }
                return new AdViewHolder(view);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder rawHolder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ITEM_TYPE_NORMAL: {
                WallPaper wallPaper = getItemData(position);
                RemoteWallpaperViewHolder holder = (RemoteWallpaperViewHolder) rawHolder;
                holder.binding.setWallpaper(wallPaper);
                holder.binding.setViewModel(mViewModel);
                holder.binding.executePendingBindings();
            }
            break;
        }
    }

    @Override
    public int getItemCount() {
        int adCount = 0;
        if (hasAdHeader()) {
            adCount = 1;
        }
        return adCount + mDataList.size();
    }

    public void setNativeAdView(View adView) {
        this.mNativeAdView = adView;
    }

    private boolean hasAdHeader() {
        return mNativeAdView != null;
    }

    private WallPaper getItemData(int position) {
        if (hasAdHeader()) {
            position--;
        }
        return mDataList.get(position);
    }

    public void clearData() {
        int oldCount = getItemCount();
        mDataList.clear();
        notifyItemRangeRemoved(hasAdHeader() ? 1 : 0, oldCount);
    }

    public List<WallPaper> getData() {
        return mDataList;
    }

    public void appendData(List<WallPaper> data) {
        if (data != null && !data.isEmpty()) {
            int oldCount = getItemCount();
            mDataList.addAll(data);
            notifyItemRangeInserted(oldCount, data.size());
            mViewModel.recordAppliedTheme(getData());
        }
    }

    private static class RemoteWallpaperViewHolder extends RecyclerView.ViewHolder {

        ItemWallpaperBinding binding;

        private RemoteWallpaperViewHolder(ItemWallpaperBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}