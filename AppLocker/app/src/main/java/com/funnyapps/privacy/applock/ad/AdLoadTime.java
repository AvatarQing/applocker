/*
 * Copyright (c) 2018 Huami Inc. All Rights Reserved.
 */

package com.funnyapps.privacy.applock.ad;

import android.content.Context;
import android.content.SharedPreferences;

import com.abc.common.tool.Debug;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author liuqing@huami.com
 */
public class AdLoadTime {

    private static final String TAG = "AdLoadTime";

    private static final String PREF_NAME = "AdLoadTime";
    public static final String INTERSTITIAL_IN_LOCKED_PAGE = "INTERSTITIAL_IN_LOCKED_PAGE";

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static void recordAdShowTime(Context context, String key) {
        getPrefs(context)
                .edit()
                .putLong(key, System.currentTimeMillis())
                .apply();
    }

    public static boolean canShowAd(Context context, String key, long interval) {
        long lastShowTime = getPrefs(context).getLong(key, 0);
        long now = System.currentTimeMillis();
        long diff = now - lastShowTime;
        boolean canShowAd = diff > interval;
        if (!canShowAd) {
            long remainTime = interval - diff;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(remainTime);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(remainTime) - minutes * 60;
            Debug.li(TAG, String.format(Locale.CHINA, "广告还剩 %02d分%02d秒 才可显示，广告位：%s", minutes, seconds, key));
        }
        return canShowAd;
    }
}
