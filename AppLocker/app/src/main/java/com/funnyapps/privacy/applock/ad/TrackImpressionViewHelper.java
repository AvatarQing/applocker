package com.funnyapps.privacy.applock.ad;

import android.view.View;

import com.abc.common.tool.Debug;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.sdk.AppLovinPostbackListener;

/**
 * @author AvatarQing
 */
class TrackImpressionViewHelper implements View.OnAttachStateChangeListener {

    private AppLovinNativeAd nativeAd;

    TrackImpressionViewHelper(AppLovinNativeAd nativeAd) {
        this.nativeAd = nativeAd;
    }

    @Override
    public void onViewAttachedToWindow(View v) {
        if (nativeAd != null) {
            nativeAd.trackImpression(new AppLovinPostbackListener() {
                @Override
                public void onPostbackSuccess(String url) {
                    Debug.ld(AppLovinAds.TAG, "Impression Tracked! " + url);
                }

                @Override
                public void onPostbackFailure(String url, int errorCode) {
                    Debug.lw(AppLovinAds.TAG, "Impression Failed to Track! errorCode:" + errorCode);
                }
            });
        }
    }

    @Override
    public void onViewDetachedFromWindow(View v) {

    }
}
