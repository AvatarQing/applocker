package com.funnyapps.privacy.applock.receiver;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class MyDeviceAdminReceiver extends DeviceAdminReceiver {

    public static final String ACTION_ADMIN_ENABLED = "com.boutique.tools.applocker.receiver.ACTION_ADMIN_ENABLED";
    public static final String ACTION_ADMIN_DISABLED = "com.boutique.tools.applocker.receiver.ACTION_ADMIN_DISABLED";

    @Override
    public void onDisabled(Context context, Intent intent) {
        Log.i(getLogTag(), "onDisabled");
        super.onDisabled(context, intent);

        Intent notifyIntent = new Intent(ACTION_ADMIN_DISABLED);
        LocalBroadcastManager.getInstance(context).sendBroadcast(notifyIntent);
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        Log.i(getLogTag(), "onEnabled");
        super.onEnabled(context, intent);

        Intent notifyIntent = new Intent(ACTION_ADMIN_ENABLED);
        LocalBroadcastManager.getInstance(context).sendBroadcast(notifyIntent);
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }
}
