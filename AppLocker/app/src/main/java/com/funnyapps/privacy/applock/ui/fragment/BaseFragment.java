package com.funnyapps.privacy.applock.ui.fragment;

import android.support.annotation.IdRes;
import android.view.View;

/**
 * Created by AvatarQing on 2016/6/13.
 */
public class BaseFragment extends RxFragment {

    private boolean isDestroyed = false;

    @Override
    public void onResume() {
        isDestroyed = false;
        super.onResume();
    }

    @Override
    public void onDestroy() {
        isDestroyed = true;
        super.onDestroy();
    }

    protected boolean isDestroyed() {
        return isDestroyed;
    }

    protected <T extends View> T $(@IdRes int id) {
        return (T) getView().findViewById(id);
    }

    protected <T extends View> T $(View view, @IdRes int id) {
        return (T) view.findViewById(id);
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }
}