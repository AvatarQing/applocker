package com.funnyapps.privacy.applock.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.ToastUtils;
import com.funnyapps.privacy.applock.wrapper.SimpleTextWatcher;

/**
 * 此页面中回答密码提示问题
 * Created by AvatarQing on 2016/6/27.
 */
public class AnswerSecurityQuestionActivity extends BaseActivity implements View.OnClickListener {

    private static class ViewHolder {
        TextView question;
        TextView title;
        EditText et_answer;
        Button btn_ok;
    }

    private ViewHolder mHolder = new ViewHolder();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_security_question);
        findViews();
        initViews();
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.ANSWER_SECURITY_QUESTION;
    }

    private void findViews() {
        mHolder.question = $(R.id.question);
        mHolder.et_answer = $(R.id.et_answer);
        mHolder.btn_ok = $(R.id.btn_ok);
        mHolder.title = $(R.id.title);
    }

    private void initViews() {
        mHolder.title.setText(R.string.answer_security_question);

        // 密码提示问题
        String question = PatternLockUtils.getSecurityQuestion(this);
        mHolder.question.setText(question);

        mHolder.et_answer.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                checkCanGoToNext();
            }
        });

        mHolder.btn_ok.setOnClickListener(this);
    }

    private void checkCanGoToNext() {
        String answer = getInputText(mHolder.et_answer);
        boolean filled = !TextUtils.isEmpty(answer);
        mHolder.btn_ok.setEnabled(filled);
    }

    private void checkInputAnswerCorrectness() {
        String answer = PatternLockUtils.getSecurityAnswer(this);
        String inputAnswer = mHolder.et_answer.getText().toString();
        boolean inputCorrect = TextUtils.equals(answer, inputAnswer);
        if (inputCorrect) {
            returnBack();
        } else {
            ToastUtils.showToast(R.string.toast_security_answer_wrong);
        }
    }

    private void returnBack() {
        setResult(RESULT_OK);
        finish();
    }

    private String getInputText(EditText editText) {
        return editText.getText().toString();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                checkInputAnswerCorrectness();
                break;
        }
    }
}