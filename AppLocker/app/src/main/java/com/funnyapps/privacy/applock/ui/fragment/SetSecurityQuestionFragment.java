package com.funnyapps.privacy.applock.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by AvatarQing on 2016/8/14.
 */
public class SetSecurityQuestionFragment extends BaseFragment {

    @BindView(R.id.et_question)
    EditText et_question;
    @BindView(R.id.et_answer)
    EditText et_answer;

    SetSecurityQuestionCallback mSetSecurityQuestionCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_set_security_question, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.btn_skip)
    void skip() {
        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.GUIDE_PAGE,
                Const.Analytic.Action.SKIP_SET_SECURITY_QUESTION
        );
        if (mSetSecurityQuestionCallback != null) {
            mSetSecurityQuestionCallback.onGoToNext();
        }
    }

    @OnClick(R.id.btn_next)
    void goToNext() {
        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.GUIDE_PAGE,
                Const.Analytic.Action.SET_SECURITY_QUESTION
        );

        String question = getInputText(et_question);
        String answer = getInputText(et_answer);
        PatternLockUtils.setSecurityQuestion(getContext(), question);
        PatternLockUtils.setSecurityAnswer(getContext(), answer);
        if (mSetSecurityQuestionCallback != null) {
            mSetSecurityQuestionCallback.onGoToNext();
        }
    }

    public void setSetSecurityQuestionCallback(SetSecurityQuestionCallback callback) {
        this.mSetSecurityQuestionCallback = callback;
    }

    private String getInputText(EditText editText) {
        return editText.getText().toString();
    }

    public interface SetSecurityQuestionCallback {
        void onGoToNext();
    }

}