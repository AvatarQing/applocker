package com.funnyapps.privacy.applock.ui.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.receiver.MyDeviceAdminReceiver;

/**
 * @author LiuQing
 */
public class RequestDeviceAdminDialogFragment extends BaseDialogFragment {

    private static final String PREF_NAME_REQUEST_DEVICE_ADMIN = "PREF_NAME_REQUEST_DEVICE_ADMIN";
    private static final String PREF_KEY_REQUEST_TIMES = "PREF_KEY_REQUEST_TIMES";

    private static final int MAX_REQUEST_TIME = 3;

    public static void showIfNeed(FragmentActivity activity) {
        SharedPreferences sp = activity.getSharedPreferences(PREF_NAME_REQUEST_DEVICE_ADMIN, Context.MODE_PRIVATE);
        int showTimes = sp.getInt(PREF_KEY_REQUEST_TIMES, 0);

        ComponentName componentName = new ComponentName(activity, MyDeviceAdminReceiver.class);
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) activity.getSystemService(Context.DEVICE_POLICY_SERVICE);
        boolean isAdminActive = devicePolicyManager.isAdminActive(componentName);
        if (!isAdminActive && showTimes < MAX_REQUEST_TIME) {
            show(activity);
            showTimes++;
            sp.edit().putInt(PREF_KEY_REQUEST_TIMES, showTimes).apply();
        }
    }

    public static void show(FragmentActivity activity) {
        RequestDeviceAdminDialogFragment dialogFragment = new RequestDeviceAdminDialogFragment();
        dialogFragment.show(activity.getSupportFragmentManager(), "RequestDeviceAdminDialogFragment");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
                .setTitle(R.string.title_permission_tip)
                .setMessage(R.string.uninstall_protect_permission_desc)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goToActivatePage();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .create();
    }

    private void goToActivatePage() {
        try {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, new ComponentName(getActivity(), MyDeviceAdminReceiver.class));
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, getString(R.string.device_admin_explantation));
            startActivityForResult(intent, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}