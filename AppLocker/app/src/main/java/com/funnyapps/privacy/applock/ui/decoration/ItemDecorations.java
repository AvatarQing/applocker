package com.funnyapps.privacy.applock.ui.decoration;

/**
 * Created by lq on 2016/12/19.
 */

public class ItemDecorations {

    public static SpacingDecoration simpleSpacingDecoration(float spacing) {
        return spacingDecoration(spacing, spacing, true);
    }

    public static SpacingDecoration spacingDecoration(float hSpacing, float vSpacing, boolean includeEdge) {
        return new SpacingDecoration((int) hSpacing, (int) vSpacing, includeEdge);
    }

}