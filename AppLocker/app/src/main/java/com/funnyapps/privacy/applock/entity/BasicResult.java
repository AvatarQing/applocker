package com.funnyapps.privacy.applock.entity;

/**
 * Created by AvatarQing on 2016/4/27.
 */
public class BasicResult {

    private AdsBean ads;
    private UpgradeBean upgrade;
    private SplashBean splash;
    private PromotionBean promotion;

    public AdsBean getAds() {
        return ads;
    }

    public void setAds(AdsBean ads) {
        this.ads = ads;
    }

    public UpgradeBean getUpgrade() {
        return upgrade;
    }

    public void setUpgrade(UpgradeBean upgrade) {
        this.upgrade = upgrade;
    }

    public SplashBean getSplash() {
        return splash;
    }

    public void setSplash(SplashBean splash) {
        this.splash = splash;
    }

    public PromotionBean getPromotion() {
        return promotion;
    }

    public void setPromotion(PromotionBean promotion) {
        this.promotion = promotion;
    }

    public static class PromotionBean {
        private int enabled = 0;

        public int getEnabled() {
            return enabled;
        }

        public void setEnabled(int enabled) {
            this.enabled = enabled;
        }
    }

    public static class AdsBean {
        private int versionCode;
        private String admobInterId;
        private String admobBannerId;
        private String fbInterId;
        private String fbNativeId;
        private String fbBannerId;

        public String getFbBannerId() {
            return fbBannerId;
        }

        public void setFbBannerId(String fbBannerId) {
            this.fbBannerId = fbBannerId;
        }

        public int getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(int versionCode) {
            this.versionCode = versionCode;
        }

        public String getAdmobInterId() {
            return admobInterId;
        }

        public void setAdmobInterId(String admobInterId) {
            this.admobInterId = admobInterId;
        }

        public String getAdmobBannerId() {
            return admobBannerId;
        }

        public void setAdmobBannerId(String admobBannerId) {
            this.admobBannerId = admobBannerId;
        }

        public String getFbInterId() {
            return fbInterId;
        }

        public void setFbInterId(String fbInterId) {
            this.fbInterId = fbInterId;
        }

        public String getFbNativeId() {
            return fbNativeId;
        }

        public void setFbNativeId(String fbNativeId) {
            this.fbNativeId = fbNativeId;
        }
    }

    public static class UpgradeBean {
        private int version;
        private String body;

        public int getVersion() {
            return version;
        }

        public void setVersion(int version) {
            this.version = version;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }

    public static class SplashBean {
        private int type;
        private String image;
        private String utm;
        private String packageX;
        private String url;
        private int enabled;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUtm() {
            return utm;
        }

        public void setUtm(String utm) {
            this.utm = utm;
        }

        public String getPackage() {
            return packageX;
        }

        public void setPackage(String packageX) {
            this.packageX = packageX;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getEnabled() {
            return enabled;
        }

        public void setEnabled(int enabled) {
            this.enabled = enabled;
        }
    }

    @Override
    public String toString() {
        return "BasicResult{" +
                "ads=" + ads +
                ", upgrade=" + upgrade +
                ", splash=" + splash +
                ", promotion=" + promotion +
                '}';
    }
}
