package com.funnyapps.privacy.applock.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.funnyapps.privacy.applock.databinding.HeaderRateBinding;
import com.funnyapps.privacy.applock.entity.AppInfo;
import com.funnyapps.privacy.applock.ui.adapter.viewholder.AdViewHolder;
import com.funnyapps.privacy.applock.ui.viewmodel.IAppListItemViewModel;
import com.funnyapps.privacy.applock.ui.viewmodel.MainAppListItemViewModel;
import com.funnyapps.privacy.applock.ui.viewmodel.RateHeaderViewModel;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;

/**
 * Created by AvatarQing on 2016/11/8.
 */

public class MainAppListAdapter extends AppListAdapter implements RateHeaderViewModel.Callback {

    private static final int ITEM_TYPE_RATE = 0;
    private static final int ITEM_TYPE_AD = 1;
    private static final int ITEM_TYPE_APP = 2;

    private MainAppListItemViewModel mAppViewModel;
    private RateHeaderViewModel mRateHeaderViewModel;
    private View mNativeAdView;
    private boolean canShowNativeAd = false;

    public MainAppListAdapter(Context context) {
        mAppViewModel = new MainAppListItemViewModel();
        mRateHeaderViewModel = new RateHeaderViewModel(context, this);
        mRateHeaderViewModel.setCallback(this);
        canShowNativeAd = !PatternLockUtils.needShowRatingHeader(context);
    }

    @Override
    IAppListItemViewModel getAppListItemViewModel() {
        return mAppViewModel;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            if (mRateHeaderViewModel.isShowRatingHeader()) {
                return ITEM_TYPE_RATE;
            } else if (needShowAd()) {
                return ITEM_TYPE_AD;
            }
        }
        return ITEM_TYPE_APP;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_RATE: {
                HeaderRateBinding binding = HeaderRateBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new RateViewHolder(binding);
            }
            case ITEM_TYPE_AD: {
                View adView = mNativeAdView;
                if (adView != null && adView.getParent() != null) {
                    ViewGroup adContainerView = (ViewGroup) adView.getParent();
                    adContainerView.removeView(adView);
                }
                if (adView != null) {
                    RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    adView.setLayoutParams(lp);
                }
                return new AdViewHolder(adView);
            }
            case ITEM_TYPE_APP: {
                return super.onCreateViewHolder(parent, viewType);
            }
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder rawHolder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ITEM_TYPE_RATE: {
                RateViewHolder holder = (RateViewHolder) rawHolder;
                holder.binding.setViewModel(mRateHeaderViewModel);
                holder.binding.executePendingBindings();
            }
            break;
            case ITEM_TYPE_APP: {
                super.onBindViewHolder(rawHolder, position);
            }
            break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        int rateHeaderCount = 0;
        int adHeaderCount = 0;
        if (mRateHeaderViewModel.isShowRatingHeader()) {
            rateHeaderCount = 1;
        }
        if (needShowAd()) {
            adHeaderCount = 1;
        }
        return rateHeaderCount + adHeaderCount + super.getItemCount();
    }

    @Override
    AppInfo getAppInfo(int position) {
        int offset = 0;
        if (mRateHeaderViewModel.isShowRatingHeader()) {
            offset = 1;
        }
        if (needShowAd()) {
            offset = 1;
        }
        return super.getAppInfo(position - offset);
    }

    private boolean needShowAd() {
        return canShowNativeAd && mNativeAdView != null;
    }

    public void setNativeAdView(View nativeAdView) {
        this.mNativeAdView = nativeAdView;
    }

    @Override
    public void onRateHeaderHidden() {
        canShowNativeAd = true;
        if (needShowAd()) {
            notifyItemInserted(0);
        }
    }

    private static class RateViewHolder extends RecyclerView.ViewHolder {
        HeaderRateBinding binding;

        RateViewHolder(HeaderRateBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}