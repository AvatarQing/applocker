package com.funnyapps.privacy.applock.receiver;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.text.TextUtils;

import com.funnyapps.privacy.applock.service.MonitorService;

public class GeneralReceiver extends BaseBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            return;
        }
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)
                || action.equals(Intent.ACTION_BATTERY_CHANGED)
                || action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            MonitorService.start(context);
        }
    }
}