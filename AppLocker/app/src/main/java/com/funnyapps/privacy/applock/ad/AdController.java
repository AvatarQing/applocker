/*
 * Copyright (c) 2018 Huami Inc. All Rights Reserved.
 */

package com.funnyapps.privacy.applock.ad;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.abc.common.entity.CloudResult;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.entity.AdConfig;
import com.funnyapps.privacy.applock.utils.DataUtils;

import java.util.concurrent.TimeUnit;

/**
 * @author liuqing@huami.com
 */
public class AdController {

    private static final String TAG = "AdController";

    @Nullable
    public static AdConfig getAvailableAdConfig(@NonNull String adPlace) {
        CloudResult<AdConfig> data = DataUtils.getCacheAdConfig();
        AdConfig matchedConfig = null;
        if (data != null && data.results != null && !data.results.isEmpty()) {
            for (int i = 0; i < data.results.size(); i++) {
                AdConfig adConfig = data.results.get(i);
                if (TextUtils.equals(adConfig.adPlace, adPlace)) {
                    long lastDisplayTime = DataUtils.getAdLastDisplayTime(adPlace);
                    int displayedCount = DataUtils.getAdDisplayedCount(adPlace);

                    // 上次广告展示时间不是今天了，就重置数据
                    if (!isToday(lastDisplayTime)) {
                        lastDisplayTime = 0;
                        displayedCount = 0;
                        DataUtils.setAdDisplayCount(adPlace, displayedCount);
                    }

                    // 本次广告展示必须与上次广告展示间隔指定时间
                    long now = System.currentTimeMillis();
                    boolean isTimeAllowed = now - lastDisplayTime >= adConfig.interval;

                    // 每天实际展示广告的次数必须在指定次数之内
                    // 配置次数若小于等于0，表示不限次数
                    boolean isFrequencyAllowed = true;
                    if (adConfig.frequency > 0) {
                        isFrequencyAllowed = displayedCount < adConfig.frequency;
                    }

                    if (isTimeAllowed && isFrequencyAllowed) {
                        matchedConfig = adConfig;
                        Debug.li(TAG, "读取到了缓存的远程广告配置，可以顺利展示，已展示次数：" + displayedCount + "，广告位：" + adPlace + "\n" + adConfig);
                        break;
                    }
                    if (!isFrequencyAllowed) {
                        Debug.li(TAG, "读取到了缓存的远程广告配置，已达到每天的展示次数了，广告位：" + adPlace + "\n" + adConfig);
                    } else {
                        Debug.li(TAG, "读取到了缓存的远程广告配置，时间未到，广告位：" + adPlace + "\n" + adConfig);
                    }
                }
            }
        }
        return matchedConfig;
    }

    public static void recordAdDisplay(@NonNull String adPlace) {
        // 增加广告展示次数
        int displayedCount = DataUtils.getAdDisplayedCount(adPlace);
        DataUtils.setAdDisplayCount(adPlace, displayedCount + 1);
        // 记录广告展示时间
        long now = System.currentTimeMillis();
        DataUtils.setAdLastDisplayTime(adPlace, now);
    }

    private static boolean isToday(long time) {
        long now = System.currentTimeMillis();
        long dayMillis = TimeUnit.DAYS.toMillis(1);
        return now / dayMillis == time / dayMillis;
    }

}
