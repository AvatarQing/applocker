package com.funnyapps.privacy.applock.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.funnyapps.privacy.applock.MyApplication;
import com.funnyapps.privacy.applock.ad.AppLovinAds;
import com.funnyapps.privacy.applock.ui.adapter.AppListAdapter;
import com.funnyapps.privacy.applock.ui.adapter.MainAppListAdapter;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Set;

/**
 * Created by AvatarQing on 2016/11/8.
 */

public class MainAppListFragment extends AppListFragment {

    MainAppListAdapter mAdapter;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Tracker tracker = MyApplication.getInstance().getDefaultTracker();
            tracker.setScreenName(Const.Analytic.ScreenName.MAIN_APP_LIST);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadAd();
    }

    @Override
    AppListAdapter getAdapter() {
        if (mAdapter == null) {
            mAdapter = new MainAppListAdapter(getActivity());
        }
        return mAdapter;
    }

    @Override
    Set<String> getLockedApps() {
        return PatternLockUtils.getLockedApps(getContext());
    }

    private void loadAd() {
        AppLovinAds.get().loadNativeAd();
        AppLovinAds.get().getNativeAdLoadResultLiveData().observe(this, loaded -> {
            if (loaded != null && loaded) {
                if (mAdapter != null) {
                    View adView = AppLovinAds.get().getNewNativeAdView();
                    if (adView != null) {
                        mAdapter.setNativeAdView(adView);
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

}