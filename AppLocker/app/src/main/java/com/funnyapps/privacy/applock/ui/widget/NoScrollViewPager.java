package com.funnyapps.privacy.applock.ui.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by lixi on 2015/9/23.
 */
public class NoScrollViewPager extends ViewPager {

    private boolean isEnableScroll = true;

    public void enableScroll(boolean enable) {
        this.isEnableScroll = enable;
    }

    public NoScrollViewPager(Context context) {
        super(context);
    }

    public NoScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isEnableScroll) {
            return super.onTouchEvent(event);
        } else {
            return false;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (isEnableScroll) {
            return super.onInterceptTouchEvent(event);
        } else {
            return false;
        }
    }
}
