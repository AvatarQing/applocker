package com.funnyapps.privacy.applock.event;

import com.funnyapps.privacy.applock.enums.WallpaperType;

/**
 * Created by lq on 2016/12/7.
 */

public class ShowInterstitialAdAfterSetWallpaperEvent {
    public WallpaperType wallpaperType;

    public ShowInterstitialAdAfterSetWallpaperEvent(WallpaperType wallpaperType) {
        this.wallpaperType = wallpaperType;
    }
}
