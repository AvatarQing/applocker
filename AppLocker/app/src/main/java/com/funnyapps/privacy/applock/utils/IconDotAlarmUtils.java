package com.funnyapps.privacy.applock.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.BuildConfig;
import com.funnyapps.privacy.applock.receiver.IconDotAlarmReceiver;

import java.util.Calendar;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/10/18 </li>
 * </ul>
 */

public class IconDotAlarmUtils {
    private static final String TAG = IconDotAlarmUtils.class.getSimpleName();
    private static final String ALARM_RECEIVER_ACTION = BuildConfig.APPLICATION_ID + ".ICON_DOT";

    private static final int REQUEST_CODE_ICON_DOT = 1;
    private static final int REMIND_HOUR = 8;
    private static final int REMIND_MINUTE = 0;
    private static final int REMIND_SECOND = 0;

    public static void setAlarm(Context context) {
        Intent intent = new Intent(context, IconDotAlarmReceiver.class)
                .setAction(ALARM_RECEIVER_ACTION);

        PendingIntent sender = PendingIntent.getBroadcast(context, REQUEST_CODE_ICON_DOT, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        if (currentHour >= REMIND_HOUR) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        calendar.set(Calendar.HOUR_OF_DAY, REMIND_HOUR);
        calendar.set(Calendar.MINUTE, REMIND_MINUTE);
        calendar.set(Calendar.SECOND, REMIND_SECOND);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            am.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
        } else {
            am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
        }

        Debug.i(TAG, "IconDotAlarmTime:" + calendar.getTimeInMillis());
    }
}
