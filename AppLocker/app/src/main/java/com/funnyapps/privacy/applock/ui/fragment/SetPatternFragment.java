package com.funnyapps.privacy.applock.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.Const;
import com.haibison.android.lockpattern.widget.LockPatternView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by AvatarQing on 2016/8/14.
 */
public class SetPatternFragment extends BaseFragment implements LockPatternView.OnPatternListener {

    @BindView(R.id.btn_spacing)
    View btn_spacing;
    @BindView(R.id.btn_redraw)
    View btn_redraw;
    @BindView(R.id.btn_next)
    TextView btn_next;
    @BindView(R.id.lock_pattern)
    LockPatternView mPatternView;
    @BindView(R.id.state_info)
    TextView mMessageText;

    Subscription mClearPatternTask;
    List<LockPatternView.Cell> pattern;
    Stage stage;
    PatternSetCallback mPatternSetCallback;
    @StringRes
    int mConfirmOkBtnText = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_set_pattern, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mPatternView.setOnPatternListener(this);
        updateStage(Stage.Draw);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeClearPatternRunnable();
    }

    protected int getMinPatternSize() {
        return 4;
    }

    public void reset() {
        updateStage(Stage.Draw);
    }

    void updateStage(Stage newStage) {
        stage = newStage;

        if (stage == Stage.DrawTooShort) {
            mMessageText.setText(getString(stage.messageId, getMinPatternSize()));
        } else {
            mMessageText.setText(stage.messageId);
        }

        if (stage.patternEnabled) {
            mPatternView.enableInput();
        } else {
            mPatternView.disableInput();
        }

        btn_redraw.setVisibility(stage.showLeftButton ? View.VISIBLE : View.GONE);
        btn_next.setVisibility(stage.showRightButton ? View.VISIBLE : View.GONE);
        btn_spacing.setVisibility(stage.showLeftButton && stage.showRightButton ? View.VISIBLE : View.GONE);
        btn_next.setText(R.string.btn_next);

        switch (stage) {
            case Draw:
                // clearPattern() resets display mode to DisplayMode.Correct.
                mPatternView.clearPattern();
                break;
            case DrawTooShort:
                mPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                postClearPatternRunnable();
                break;
            case DrawValid:
                break;
            case Confirm:
                mPatternView.clearPattern();
                break;
            case ConfirmWrong:
                mPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                postClearPatternRunnable();
                break;
            case ConfirmCorrect:
                if (mConfirmOkBtnText > 0) {
                    btn_next.setText(mConfirmOkBtnText);
                }
                break;
        }
    }

    public interface PatternSetCallback {
        void onRedraw();

        void tryToConfirmPattern();

        void onPatternConfirmed(List<LockPatternView.Cell> pattern);
    }

    @OnClick(R.id.btn_redraw)
    void redraw() {
        pattern = null;
        updateStage(Stage.Draw);
        if (mPatternSetCallback != null) {
            mPatternSetCallback.onRedraw();
        }
    }

    @OnClick(R.id.btn_next)
    void goToNext() {
        switch (stage) {
            case DrawValid:
                updateStage(Stage.Confirm);
                if (mPatternSetCallback != null) {
                    mPatternSetCallback.tryToConfirmPattern();
                }
                break;
            case ConfirmCorrect:
                if (mPatternSetCallback != null) {
                    mPatternSetCallback.onPatternConfirmed(pattern);
                }
                break;
        }
    }

    public void setConfirmOkBtnText(@StringRes int text) {
        this.mConfirmOkBtnText = text;
    }

    // >>>>>>> LockPatternView.OnPatternListener
    @Override
    public void onPatternStart() {
        removeClearPatternRunnable();

        mMessageText.setText(R.string.pl_recording_pattern);
        mPatternView.setDisplayMode(LockPatternView.DisplayMode.Correct);
    }

    @Override
    public void onPatternCleared() {
        removeClearPatternRunnable();
    }

    @Override
    public void onPatternCellAdded(List<LockPatternView.Cell> pattern) {
    }

    @Override
    public void onPatternDetected(List<LockPatternView.Cell> newPattern) {
        switch (stage) {
            case Draw:
            case DrawTooShort:
                if (newPattern.size() < getMinPatternSize()) {
                    updateStage(Stage.DrawTooShort);
                } else {
                    pattern = new ArrayList<>(newPattern);
                    updateStage(Stage.DrawValid);
                }
                break;
            case Confirm:
            case ConfirmWrong:
                if (newPattern.equals(pattern)) {
                    updateStage(Stage.ConfirmCorrect);
                } else {
                    updateStage(Stage.ConfirmWrong);
                }
                break;
            default:
                throw new IllegalStateException("Unexpected stage " + stage + " when "
                        + "entering the pattern.");
        }
    }
    // <<<<<<< LockPatternView.OnPatternListener

    protected void removeClearPatternRunnable() {
        if (mClearPatternTask != null && !mClearPatternTask.isUnsubscribed()) {
            mClearPatternTask.unsubscribe();
            mClearPatternTask = null;
        }
    }

    protected void postClearPatternRunnable() {
        removeClearPatternRunnable();
        mClearPatternTask = Observable.timer(Const.CLEAR_PATTERN_DELAY_MILLI, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        mPatternView.clearPattern();
                    }
                });
    }

    public enum Stage {
        Draw(R.string.pl_draw_pattern, true, false, false),
        DrawTooShort(R.string.pl_pattern_too_short, true, false, false),
        DrawValid(R.string.pl_pattern_recorded, false, true, true),
        Confirm(R.string.pl_confirm_pattern, true, true, false),
        ConfirmWrong(R.string.pl_wrong_pattern, true, true, false),
        ConfirmCorrect(R.string.pl_pattern_confirmed, false, true, true),;

        public final int messageId;
        public final boolean patternEnabled;
        public final boolean showLeftButton;
        public final boolean showRightButton;

        Stage(int messageId, boolean patternEnabled, boolean showLeftButton, boolean showRightButton) {
            this.messageId = messageId;
            this.patternEnabled = patternEnabled;
            this.showLeftButton = showLeftButton;
            this.showRightButton = showRightButton;
        }
    }

    public void setPatternSetCallback(PatternSetCallback callback) {
        this.mPatternSetCallback = callback;
    }

}