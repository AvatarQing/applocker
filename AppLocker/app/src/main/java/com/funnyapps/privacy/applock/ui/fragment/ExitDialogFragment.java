package com.funnyapps.privacy.applock.ui.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.funnyapps.privacy.applock.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lq on 2016/12/7.
 */

public class ExitDialogFragment extends BaseDialogFragment {

    @BindView(R.id.ad_container)
    FrameLayout ad_container;

    Builder mBuilder;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        createContentView(dialog);
        configDialogSize(dialog);
        return dialog;
    }

    private void createContentView(Dialog dialog) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(
                R.layout.dialog_exit,
                (ViewGroup) dialog.getWindow().getDecorView(),
                false);
        dialog.setContentView(dialogView);
        ButterKnife.bind(this, dialogView);
        if (mBuilder != null && mBuilder.nativeAdView != null) {
            if (mBuilder.nativeAdView.getParent() != null) {
                try {
                    ViewGroup parentView = (ViewGroup) mBuilder.nativeAdView.getParent();
                    parentView.removeView(mBuilder.nativeAdView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            ad_container.addView(mBuilder.nativeAdView);
        } else {
            ad_container.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_cancel)
    void closeDialog() {
        dismiss();
    }

    @OnClick(R.id.btn_ok)
    void exit() {
        dismiss();
        getActivity().finish();
    }

    public void setBuilder(Builder builder) {
        mBuilder = builder;
    }

    public static class Builder {
        View nativeAdView;

        public Builder setNativeAdView(View nativeAdView) {
            this.nativeAdView = nativeAdView;
            return this;
        }

        public ExitDialogFragment create() {
            ExitDialogFragment dialogFragment = new ExitDialogFragment();
            dialogFragment.setBuilder(this);
            return dialogFragment;
        }
    }
}