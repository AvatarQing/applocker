package com.funnyapps.privacy.applock;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.abc.common.entity.CloudResult;
import com.abc.common.tool.Debug;
import com.applovin.sdk.AppLovinSdk;
import com.funnyapps.privacy.applock.ad.AppLovinAds;
import com.funnyapps.privacy.applock.analytics.AppExceptionParser;
import com.funnyapps.privacy.applock.entity.AdConfig;
import com.funnyapps.privacy.applock.service.MonitorService;
import com.funnyapps.privacy.applock.ui.activity.SwitchLauncherWallpaperActivity;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.DataUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.web.WebClient;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.liulishuo.filedownloader.FileDownloader;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.orhanobut.logger.LogAdapter;
import com.orhanobut.logger.Logger;

import java.util.Locale;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MyApplication extends Application {

    private static MyApplication sApplication = null;
    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        Debug.enable(true);
        Logger.addLogAdapter(
                new LogAdapter() {
                    @Override
                    public boolean isLoggable(int priority, String tag) {
                        return BuildConfig.DEBUG;
                    }

                    @Override
                    public void log(int priority, String tag, String message) {
                        Thread thread = Thread.currentThread();
                        String threadInfo = String.format(Locale.CHINA, "[Thread: %s, %s]", thread.getId(), thread.getName());
                        Log.println(priority, tag, message + "             >>>>>> " + threadInfo);
                    }
                }
        );
        initAds();
        initNotificationChannel();
        MonitorService.start(this);
        initImageLoader();
        createAppShortcut();
        FileDownloader.init(getApplicationContext());
        getDefaultTracker();
        initGoogleAnalytics();
        listenActivities();
        loadAdConfigIfNeed();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static MyApplication getInstance() {
        return sApplication;
    }

    private void initAds() {
        AppLovinSdk.initializeSdk(this);
        AppLovinAds.get().init(this);
        AppLovinAds.get().loadBannerAd();
        AppLovinAds.get().loadInterstitialAd();
        AppLovinAds.get().loadNativeAd();
    }

    private void initNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (nm == null) {
                return;
            }
            NotificationChannel channel;

            channel = new NotificationChannel(Const.NT_CHANNEL_MONITOR, getString(R.string.nt_channel_monitor), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null, null);
            nm.createNotificationChannel(channel);

            channel = new NotificationChannel(Const.NT_CHANNEL_IMPROVE, getString(R.string.nt_channel_improve), NotificationManager.IMPORTANCE_DEFAULT);
            //是否在桌面icon右上角展示小红点
            channel.enableLights(true);
            //小红点颜色
            channel.setLightColor(Color.GREEN);
            //是否在久按桌面图标时显示此渠道的通知
            channel.setShowBadge(true);
            nm.createNotificationChannel(channel);
        }
    }

    void loadAdConfigIfNeed() {
        Observable
                .fromCallable(DataUtils::getCacheAdConfig)
                .filter(data -> data != null && data.results != null && !data.results.isEmpty())
                .isEmpty()
                .flatMap((Func1<Boolean, Observable<CloudResult<AdConfig>>>) isEmpty -> {
                    if (isEmpty) {
                        return WebClient.getLeanCloudService().loadAdConfig();
                    }
                    return null;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        DataUtils::cacheAdConfig,
                        Throwable::printStackTrace);
    }

    private void initGoogleAnalytics() {
        ExceptionReporter myHandler = new ExceptionReporter(
                getDefaultTracker(),
                Thread.getDefaultUncaughtExceptionHandler(),
                this
        );
        myHandler.setExceptionParser(new AppExceptionParser(this));
        Thread.setDefaultUncaughtExceptionHandler(myHandler);
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    void initImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions
                .Builder()
                .showImageOnLoading(R.drawable.placeholder)
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        int memoryCacheSize = Utils.calculateMemoryCacheSize(this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration
                .Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new LruMemoryCache(memoryCacheSize))
                .memoryCacheSize(memoryCacheSize)
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .build();
        ImageLoader.getInstance().init(config);
    }

    void createAppShortcut() {
        if (!DataUtils.hasCreatedShortcut()) {
            Context appContext = this;
            //创建快捷方式的Intent
            Intent shortcutIntent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
            //不允许重复创建
            shortcutIntent.putExtra("duplicate", false);
            //需要现实的名称
            shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, appContext.getString(R.string.shortcut_name));
            //快捷图片
            Parcelable icon = Intent.ShortcutIconResource.fromContext(appContext, R.drawable.ic_launcher);
            shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);

            //点击快捷图片，运行的程序主入口
            String action = getPackageName() + ".switchlauncherwallpaper";
            Intent launcherIntent = new Intent(appContext, SwitchLauncherWallpaperActivity.class)
                    .setAction(action)
                    .setPackage(getPackageName())
                    .addCategory(Intent.CATEGORY_DEFAULT)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, launcherIntent);
            //发送广播
            appContext.sendBroadcast(shortcutIntent);

            DataUtils.setHasCreatedShortcut(true);

            Debug.li(getLogTag(), "createAppShortcut");
        }
    }

    void listenActivities() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                Debug.i(activity.getClass().getSimpleName(), "onActivityCreated()");
            }

            @Override
            public void onActivityStarted(Activity activity) {
                Debug.i(activity.getClass().getSimpleName(), "onActivityStarted()");
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Debug.i(activity.getClass().getSimpleName(), "onActivityResumed()");
            }

            @Override
            public void onActivityPaused(Activity activity) {
                Debug.i(activity.getClass().getSimpleName(), "onActivityPaused()");
            }

            @Override
            public void onActivityStopped(Activity activity) {
                Debug.i(activity.getClass().getSimpleName(), "onActivityStopped()");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                Debug.i(activity.getClass().getSimpleName(), "onActivitySaveInstanceState()");
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                Debug.i(activity.getClass().getSimpleName(), "onActivityDestroyed()");
            }
        });
    }

    private String getLogTag() {
        return getClass().getSimpleName();
    }
}