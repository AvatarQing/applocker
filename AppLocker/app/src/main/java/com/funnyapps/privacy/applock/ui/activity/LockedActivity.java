package com.funnyapps.privacy.applock.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;

import com.abc.common.entity.CloudResult;
import com.abc.common.tool.Debug;
import com.alibaba.fastjson.JSON;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.ad.AdController;
import com.funnyapps.privacy.applock.ad.AdLoadTime;
import com.funnyapps.privacy.applock.ad.AppLovinAds;
import com.funnyapps.privacy.applock.entity.AdConfig;
import com.funnyapps.privacy.applock.entity.ThemeConfig;
import com.funnyapps.privacy.applock.entity.WallPaper;
import com.funnyapps.privacy.applock.enums.WallpaperType;
import com.funnyapps.privacy.applock.function.Consumer;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.ThemeUtils;
import com.funnyapps.privacy.applock.utils.ToastUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.utils.WallpaperUtils;
import com.haibison.android.lockpattern.widget.LockPatternView;
import com.jakewharton.rxbinding.view.RxView;
import com.trello.rxlifecycle.android.ActivityEvent;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okio.BufferedSource;
import okio.Okio;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 锁定别的应用时的页面
 */
public class LockedActivity extends BasePatternActivity {

    public static final String EXTRA_LOCKED_APP_PACKAGE_NAME = "EXTRA_LOCKED_APP_PACKAGE_NAME";
    private static final int REQUEST_CODE_ANSWER_SECURITY_QUESTION = 111;

    @BindView(R.id.lock_pattern)
    LockPatternView mPatternView;
    @BindView(R.id.app_icon)
    ImageView app_icon;
    @BindView(R.id.btn_forget_password)
    View btn_forget_password;
    @BindView(R.id.banner_ad_container_large)
    ViewGroup banner_ad_container_large;
    @BindView(R.id.banner_ad_container_small)
    ViewGroup banner_ad_container_small;
    @BindView(R.id.root_container)
    ViewGroup root_container;
    @BindView(R.id.bottom_button_bar)
    ViewGroup bottom_button_bar;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.clock)
    View clock;
    @BindView(R.id.btn_switch_wallpaper)
    View btn_switch_wallpaper;

    String mLockedAppPkgName;
    int mSwitchWallPaperButtonClickTimes = 0;
    boolean isLoadingWallPaper = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locked);
        ButterKnife.bind(this);
        parseArgument();
        initViews();
        applyTheme();
        loadAd();
        ThemeUtils.getPres(this).registerOnSharedPreferenceChangeListener(mOnThemeChangedListener);
        // TODO: 2016/11/20 主题和壁纸，谁先设置就应用谁

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.LOCKED_APP,
                Const.Analytic.Action.APP,
                mLockedAppPkgName
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        showWallPaperIfNeed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ThemeUtils.getPres(this).unregisterOnSharedPreferenceChangeListener(mOnThemeChangedListener);
    }

    @Override
    protected void addStatusBarPadding() {
    }

    @Override
    protected String getScreenName() {
        return Const.Analytic.ScreenName.LOCK;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }

    void loadAd() {
        if (AdLoadTime.canShowAd(this, AdLoadTime.INTERSTITIAL_IN_LOCKED_PAGE, TimeUnit.MINUTES.toMillis(30))) {
            AppLovinAds.get().getDisposableInterstitialAdLoadResultLiveData().observe(this, loaded -> {
                if (loaded != null && loaded) {
                    AppLovinAds.get().showInterstitialAd();
                    AdLoadTime.recordAdShowTime(getApplicationContext(), AdLoadTime.INTERSTITIAL_IN_LOCKED_PAGE);
                }
            });
        }

        Consumer<View> showBottomBannerTask = bannerAdView -> {
            if (bannerAdView != null) {
                banner_ad_container_small.removeAllViews();
                banner_ad_container_small.addView(bannerAdView);
                banner_ad_container_small.setVisibility(View.VISIBLE);
            }
        };
        Consumer<View> showTopNativeTask = nativeAdView -> {
            if (nativeAdView != null) {
                banner_ad_container_large.removeAllViews();
                banner_ad_container_large.addView(nativeAdView);
                banner_ad_container_large.setVisibility(View.VISIBLE);
                clock
                        .animate()
                        .alpha(0)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                banner_ad_container_large
                                        .animate()
                                        .scaleX(1)
                                        .scaleY(1)
                                        .setInterpolator(new BounceInterpolator())
                                        .setDuration(800)
                                        .start();
                            }
                        })
                        .start();
            }
        };
        Runnable observeBannerAdTask = () -> {
            AppLovinAds.get().getDisposableBannerAdLoadResultLiveData().observe(this, loaded -> {
                if (loaded != null && loaded) {
                    View bannerAdView = AppLovinAds.get().getNewBannerAdView();
                    showBottomBannerTask.accept(bannerAdView);
                }
            });
        };
        boolean showTopNativeAd = new Random().nextDouble() < 0.3f;
        showTopNativeAd = false;
        if (showTopNativeAd) {
            AppLovinAds.get().getNativeAdLoadResultLiveData().observe(this, loaded -> {
                if (loaded != null && loaded) {
                    View adView = AppLovinAds.get().getNewNativeAdView();
                    showTopNativeTask.accept(adView);
                } else {
                    observeBannerAdTask.run();
                }
            });
        } else {
            observeBannerAdTask.run();
        }

        AppLovinAds.get().loadBannerAd();
        AppLovinAds.get().loadInterstitialAd();
    }

    void parseArgument() {
        mLockedAppPkgName = getIntent().getStringExtra(EXTRA_LOCKED_APP_PACKAGE_NAME);
    }

    void initViews() {
        if (!TextUtils.isEmpty(mLockedAppPkgName)) {
            try {
                Drawable appIcon = getPackageManager().getApplicationIcon(mLockedAppPkgName);
                app_icon.setImageDrawable(appIcon);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (PatternLockUtils.hasSetSecurityQuestion(this)) {
            btn_forget_password.setVisibility(View.VISIBLE);
        } else {
            btn_forget_password.setVisibility(View.INVISIBLE);
        }
        initSwitchWallPaperButton();
    }

    @Override
    public void onBackPressed() {
        goToLauncherPage();
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_ANSWER_SECURITY_QUESTION: {
                if (resultCode == RESULT_OK) {
                    goToSetPatternPage();
                }
            }
            break;
        }
    }

    @Override
    protected LockPatternView getPatternView() {
        return mPatternView;
    }

    @Override
    public void onPatternStart() {
        removeClearPatternRunnable();

        // Set display mode to correct to ensure that pattern can be in stealth mode.
        mPatternView.setDisplayMode(LockPatternView.DisplayMode.Correct);
    }

    @Override
    public void onPatternCellAdded(List<LockPatternView.Cell> pattern) {
    }

    @Override
    public void onPatternDetected(List<LockPatternView.Cell> pattern) {
        if (PatternLockUtils.isPatternCorrect(pattern, this)) {
            AdConfig adConfig = AdController.getAvailableAdConfig(Const.AdPlace.INTERSTITIAL_AFTER_UNLOCK);
            if (adConfig != null
                    && TextUtils.equals(adConfig.platform, Const.AdPlatform.APPLOVIN)
                    && AppLovinAds.get().isInterstitialAdLoaded()) {
                AppLovinAds.get().showInterstitialAd();
                AdController.recordAdDisplay(Const.AdPlace.INTERSTITIAL_AFTER_UNLOCK);
            }
            finish();
        } else {
            mPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
            postClearPatternRunnable();
        }
    }

    @Override
    public void onPatternCleared() {
        removeClearPatternRunnable();
    }

    @OnClick(R.id.btn_forget_password)
    void onForgotPassword() {
        Intent intent = new Intent(this, AnswerSecurityQuestionActivity.class);
        startActivityForResult(intent, REQUEST_CODE_ANSWER_SECURITY_QUESTION);
    }

    private void goToSetPatternPage() {
        Intent intent = new Intent(this, ChangePatternActivity.class);
        startActivity(intent);
    }

    private void goToLauncherPage() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    SharedPreferences.OnSharedPreferenceChangeListener mOnThemeChangedListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (TextUtils.equals(key, Const.PrefKey.APPLIED_THEME_PACKAGE_NAME)) {
                applyTheme();
            }
        }
    };

    private void applyTheme() {
        Debug.li(getLogTag(), "applyTheme");
        String themePkgName = ThemeUtils.getAppliedThemePackageName(this);
        if (TextUtils.isEmpty(themePkgName)) {
            Debug.li(getLogTag(), "Cannot set theme, No applied theme");
            return;
        }
        if (!Utils.isAppInstall(this, themePkgName)) {
            Debug.li(getLogTag(), "Cannot set theme, Theme apk " + themePkgName + " is not installed");
            return;
        }
        try {
            Context themeContext = createPackageContext(themePkgName, CONTEXT_IGNORE_SECURITY);
            AssetManager themeAm = themeContext.getAssets();
            // 读取主题配置文件中的内容
            String configJson = null;
            try {
                InputStream is = themeAm.open(Const.THEME_CONFIG_FILE_NAME);
                BufferedSource source = Okio.buffer(Okio.source(is));
                configJson = source.readUtf8();
                source.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(configJson)) {
                Debug.li(getLogTag(), "Cannot apply theme, cannot read theme config file");
                return;
            }
            // 文本内容映射为配置类
            ThemeConfig themeConfig = JSON.parseObject(configJson, ThemeConfig.class);
            Debug.li(getLogTag(), "" + themeConfig);
            if (themeConfig == null) {
                return;
            }

            // 根据配置读取主题资源 -------------
            // 背景
            Bitmap bgBitmap = getBitmapFromAsset(themeAm, themeConfig.background);
            int bgColor = Color.parseColor(themeConfig.background_color);

            // 连接线
            int patternPathNormalColor = Color.parseColor(themeConfig.pattern.track_color);
            int patternPathErrorColor = Color.parseColor(themeConfig.pattern.track_color_on_error);

            // 点（内部小圆）
            String touchedButton = null;
            if (!themeConfig.pattern.cell_pressed.isEmpty()) {
                touchedButton = themeConfig.pattern.cell_pressed.get(0).image1;
            }

            // 应用读取出来的主题配置 -------------
            // 背景
            Drawable bgDrawable = new BitmapDrawable(getResources(), bgBitmap);
            root_container.setBackgroundColor(bgColor);
            root_container.setBackgroundDrawable(bgDrawable);
            // 密码盘
            new LockPatternView.ThemeAssetBuilder(themeAm)
                    .setNormalPathColor(patternPathNormalColor)
                    .setWrongPathColor(patternPathErrorColor)
                    .setDefaultButton(themeConfig.pattern.cell_normal_image)
                    .setWrongButton(themeConfig.pattern.cell_image_on_error)
                    .setTouchedButton(touchedButton)
                    .setDefaultCircle(themeConfig.pattern.cell_normal_circle_image)
                    .setCorrectCircle(themeConfig.pattern.cell_pressed_circle_image)
                    .setWrongCircle(themeConfig.pattern.cell_circle_image_on_error)
                    .showCircleDefault(themeConfig.pattern.cell_normal_has_circle)
                    .showCircleCorrect(themeConfig.pattern.cell_pressed_has_circle)
                    .showCircleWrong(themeConfig.pattern.cell_has_circle_on_error)
                    .apply(mPatternView);
            clearHeaderBackgroundColor();
            Debug.li(getLogTag(), "theme applied \n");
        } catch (Exception e) {
            e.printStackTrace();
            Debug.lw(getLogTag(), "" + e);
        }
    }

    private void clearHeaderBackgroundColor() {
        header.setBackgroundColor(Color.TRANSPARENT);
        mBarTintManager.setStatusBarTintEnabled(false);
    }

    private Bitmap getBitmapFromAsset(AssetManager am, String assetName) {
        try {
            InputStream is = am.open(assetName);
            return BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    String getWallpaperUrl() {
        return WallpaperUtils.getAppliedWallpaperUrl(getApplicationContext());
    }

    WallpaperType getWallpaperType() {
        return WallpaperUtils.getAppliedWallpaperType(getApplicationContext());
    }

    void showWallPaperIfNeed() {
        Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getWallpaperUrl();
                    }
                })
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String imageUrl) {
                        return imageUrl != null && !TextUtils.isEmpty(imageUrl);
                    }
                })
                .compose(getDrawableFromImageUrl())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(provideSetWallPaperAction());
    }

    Action1<Drawable> provideSetWallPaperAction() {
        return new Action1<Drawable>() {
            @Override
            public void call(Drawable drawable) {
                Debug.li(getLogTag(), "设置壁纸：" + drawable);
                clearHeaderBackgroundColor();
                root_container.setBackgroundDrawable(drawable);
            }
        };
    }

    Observable.Transformer<String, Drawable> getDrawableFromImageUrl() {
        return new Observable.Transformer<String, Drawable>() {
            @Override
            public Observable<Drawable> call(Observable<String> sourceObservable) {
                return sourceObservable
                        .observeOn(Schedulers.io())
                        .map(new Func1<String, Bitmap>() {
                            @Override
                            public Bitmap call(String wallPaperUrl) {
                                WallpaperType wallpaperType = getWallpaperType();
                                if (wallpaperType == null) {
                                    return null;
                                }
                                try {
                                    switch (wallpaperType) {
                                        case LOCAL:
                                            Debug.li(getLogTag(), "正在加载图片" + wallPaperUrl);
                                            return Utils.loadImageSyncFromFile(wallPaperUrl);
                                        case REMOTE:
                                            Debug.li(getLogTag(), "正在下载图片" + wallPaperUrl);
                                            return Utils.loadImageSync(wallPaperUrl);
                                    }
                                } catch (Exception e) {
                                    throw Exceptions.propagate(e);
                                }
                                return null;
                            }
                        })
                        .filter(new Func1<Bitmap, Boolean>() {
                            @Override
                            public Boolean call(Bitmap bitmap) {
                                return bitmap != null;
                            }
                        })
                        .map(new Func1<Bitmap, Drawable>() {
                            @Override
                            public Drawable call(Bitmap bitmap) {
                                return new BitmapDrawable(getResources(), bitmap);
                            }
                        });
            }
        };
    }

    void initSwitchWallPaperButton() {
        RxView.clicks(btn_switch_wallpaper)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        if (isLoadingWallPaper) {
                            ToastUtils.showToast(R.string.toast_switching_wallpaper);
                            return;
                        }
                        mSwitchWallPaperButtonClickTimes++;
                        Debug.li(getLogTag(), "clickTimes:" + mSwitchWallPaperButtonClickTimes);
                        boolean hasLoadedInterAd = AppLovinAds.get().isInterstitialAdLoaded();
                        if (mSwitchWallPaperButtonClickTimes % 3 != 0
                                || !hasLoadedInterAd) {
                            switchWallpaper();
                        } else {
                            AppLovinAds.get().showInterstitialAd();
                        }
                    }
                });
    }

    void switchWallpaper() {
        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.LOCKED_PAGE,
                Const.Analytic.Action.SWITCH_WALLPAPER
        );
        WallpaperUtils.getRemoteWallpapers()
                .compose(this.<CloudResult<WallPaper>>bindUntilEvent(ActivityEvent.DESTROY))
                .switchMap(new Func1<CloudResult<WallPaper>, Observable<String>>() {
                    @Override
                    public Observable<String> call(CloudResult<WallPaper> result) {
                        int randomIndex = new Random().nextInt(result.results.size());
                        return Observable.just(result.results.get(randomIndex).imageUrl);
                    }
                })
                .doOnNext(new Action1<String>() {
                    @Override
                    public void call(String wallPaper) {
                        WallpaperUtils.applyWallpaper(getApplicationContext(), wallPaper, WallpaperType.REMOTE);
                    }
                })
                .compose(getDrawableFromImageUrl())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        isLoadingWallPaper = true;
                        ToastUtils.showToast(R.string.toast_switching_wallpaper);
                    }
                })
                .subscribe(
                        provideSetWallPaperAction(),
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                isLoadingWallPaper = false;
                                Debug.lw(getLogTag(), throwable.getMessage());
                                throwable.printStackTrace();
                            }
                        },
                        new Action0() {
                            @Override
                            public void call() {
                                isLoadingWallPaper = false;
                                ToastUtils.hideToast();
                            }
                        });
    }
}