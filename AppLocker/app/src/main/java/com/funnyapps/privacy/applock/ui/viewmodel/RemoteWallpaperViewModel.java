package com.funnyapps.privacy.applock.ui.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.entity.WallPaper;
import com.funnyapps.privacy.applock.enums.WallpaperType;
import com.funnyapps.privacy.applock.ui.activity.LockedPreviewActivity;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.utils.WallpaperUtils;

import java.util.List;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by lq on 2016/11/29.
 */

public class RemoteWallpaperViewModel {

    private WallPaper mLastAppliedWallpaper;

    public void onItemClick(View view, final WallPaper wallpaper) {
        Debug.li(getLogTag(), "applied wallpaper : " + wallpaper);
        final Context context = view.getContext();
        if (!TextUtils.isEmpty(wallpaper.jumpLink)
                && (wallpaper.jumpLink.startsWith(Const.GOOGLE_PLAY_PREFIX_HTTPS)
                || wallpaper.jumpLink.startsWith(Const.GOOGLE_PLAY_PREFIX_HTTP)
                || wallpaper.jumpLink.startsWith(Const.GOOGLE_PLAY_PREFIX_MARKET))) {
            goToGooglePlayPage(context, wallpaper.jumpLink);
        } else {
            goToPreviewPage(context, wallpaper);
        }

        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.WALLPAPER,
                Const.Analytic.Action.PREVIEW_REMOTE_WALLPAPER,
                wallpaper.imageUrl
        );
    }

    private void goToGooglePlayPage(Context context, String url) {
        Utils.openLink(context, url);
    }

    private void goToPreviewPage(Context context, WallPaper wallpaper) {
        Intent intent = new Intent(context, LockedPreviewActivity.class)
                .putExtra(LockedPreviewActivity.EXTRA_PREVIEW_WALLPAPER_TYPE, WallpaperType.REMOTE.ordinal())
                .putExtra(LockedPreviewActivity.EXTRA_PREVIEW_WALLPAPER_URL, wallpaper.imageUrl);
        context.startActivity(intent);
    }

    public void refreshAppliedWallpaper(Context context, List<WallPaper> data) {
        final String appliedWallpaperUrl = WallpaperUtils.getAppliedWallpaperUrl(context);
        Observable.from(data)
                .filter(new Func1<WallPaper, Boolean>() {
                    @Override
                    public Boolean call(WallPaper wallPaper) {
                        return TextUtils.equals(appliedWallpaperUrl, wallPaper.imageUrl);
                    }
                })
                .first()
                .subscribe(
                        new Action1<WallPaper>() {
                            @Override
                            public void call(WallPaper wallPaper) {
                                wallPaper.isApplied.set(true);
                                mLastAppliedWallpaper = wallPaper;
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                            }
                        }
                );
    }

    public void recordAppliedTheme(List<WallPaper> data) {
        Observable.from(data)
                .filter(new Func1<WallPaper, Boolean>() {
                    @Override
                    public Boolean call(WallPaper wallPaper) {
                        return wallPaper.isApplied.get();
                    }
                })
                .first()
                .subscribe(
                        new Action1<WallPaper>() {
                            @Override
                            public void call(WallPaper wallPaper) {
                                mLastAppliedWallpaper = wallPaper;
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                            }
                        }
                );
    }

    public void clearAppliedWallpaper() {
        if (mLastAppliedWallpaper != null) {
            mLastAppliedWallpaper.isApplied.set(false);
            mLastAppliedWallpaper = null;
        }
    }

    private String getLogTag() {
        return getClass().getSimpleName();
    }

}