package com.funnyapps.privacy.applock.service;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.abc.common.entity.CloudResult;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.entity.UpgradeConfig;
import com.funnyapps.privacy.applock.receiver.ApkReceiver;
import com.funnyapps.privacy.applock.task.RefreshTimerTask;
import com.funnyapps.privacy.applock.ui.activity.LockedActivity;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.DataUtils;
import com.funnyapps.privacy.applock.utils.IconDotAlarmUtils;
import com.funnyapps.privacy.applock.utils.NotiManager;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;
import com.funnyapps.privacy.applock.utils.Utils;
import com.funnyapps.privacy.applock.web.WebClient;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MonitorService extends DaemonService {

    public static final String EXTRA_COMMAND = "EXTRA_COMMAND";
    public static final String EXTRA_PACKAGE_NAME = "EXTRA_PACKAGE_NAME";
    public static final int COMMAND_REFRESH_READ_LOCKED_APPS = 1;
    public static final int COMMAND_FETCH_UPGRADE_INFO_IF_NEED = 2;
    public static final int COMMAND_ASK_LOCK_NEW_INSTALL_APP_IF_NEED = 3;

    private Timer mTimer = new Timer();
    private RefreshTimerTask mRefreshTask = null;
    private Set<String> mLockedApps;
    private Set<String> mKillWhiteList;
    private Map<String, Boolean> mUnlockRecordMap = new HashMap<>();
    private long mScreenOffStartTime = 0;
    private boolean needClearUnlockedRecords = false;
    private ApkReceiver mApkReceiver = new ApkReceiver();

    public static void start(Context context) {
        try {
            Intent newIntent = new Intent(context, MonitorService.class);
            context.startService(newIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestRefreshReadLockedApps(Context context) {
        Intent newIntent = new Intent(context, MonitorService.class)
                .putExtra(EXTRA_COMMAND, COMMAND_REFRESH_READ_LOCKED_APPS);
        context.startService(newIntent);
    }

    public static void requestFetchUpgradeInfoIfNeed(Context context) {
        Intent newIntent = new Intent(context, MonitorService.class)
                .putExtra(EXTRA_COMMAND, COMMAND_FETCH_UPGRADE_INFO_IF_NEED);
        context.startService(newIntent);
    }

    public static void requestAskLockNewInstalledAppIfNeed(Context context, String packageName) {
        Intent newIntent = new Intent(context, MonitorService.class)
                .putExtra(EXTRA_PACKAGE_NAME, packageName)
                .putExtra(EXTRA_COMMAND, COMMAND_ASK_LOCK_NEW_INSTALL_APP_IF_NEED);
        context.startService(newIntent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        refreshReadLockedApps();
        registerBroadcastReceivers();
        startRefreshTimer();
        initKillWhiteList();
        mRefreshTask.setRefreshCallback(mRefreshCallback);
        IconDotAlarmUtils.setAlarm(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            int command = intent.getIntExtra(EXTRA_COMMAND, 0);
            switch (command) {
                case COMMAND_REFRESH_READ_LOCKED_APPS:
                    refreshReadLockedApps();
                    break;
                case COMMAND_FETCH_UPGRADE_INFO_IF_NEED:
                    loadUpgradeInfo();
                    break;
                case COMMAND_ASK_LOCK_NEW_INSTALL_APP_IF_NEED: {
                    String packageName = intent.getStringExtra(EXTRA_PACKAGE_NAME);
                    askLockNewInstalledAppIfNeed(packageName);
                    break;
                }
                default:
                    super.onStartCommand(intent, flags, startId);
                    break;
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseRefreshTimer();
        unregisterBroadcastReceivers();
    }

    private void registerBroadcastReceivers() {
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(Intent.ACTION_SCREEN_ON);
        iFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenReceiver, iFilter);

        iFilter = new IntentFilter();
        iFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        iFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        iFilter.addDataScheme("package");
        registerReceiver(mScreenReceiver, iFilter);
    }

    private void unregisterBroadcastReceivers() {
        unregisterReceiver(mScreenReceiver);
    }

    private void initKillWhiteList() {
        mKillWhiteList = new HashSet<>();
        mKillWhiteList.add(getPackageName());
        mKillWhiteList.add("com.android.packageinstaller");
        mKillWhiteList.add("com.google.android.packageinstaller");
        mKillWhiteList.add("android");

        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        List<ResolveInfo> resolveInfoList = getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : resolveInfoList) {
            mKillWhiteList.add(info.activityInfo.packageName);
        }
    }

    private void askLockNewInstalledAppIfNeed(final String newInstallAppPackageName) {
        Observable
                .fromCallable(new Callable<Set<String>>() {
                    @Override
                    public Set<String> call() throws Exception {
                        return PatternLockUtils.getLockedApps(getApplicationContext());
                    }
                })
                .filter(new Func1<Set<String>, Boolean>() {
                    @Override
                    public Boolean call(Set<String> strings) {
                        return strings != null && !strings.isEmpty();
                    }
                })
                .flatMap(new Func1<Set<String>, Observable<String>>() {
                    @Override
                    public Observable<String> call(Set<String> set) {
                        return Observable.from(set);
                    }
                })
                .contains(newInstallAppPackageName)
                .toSingle()
                .subscribe(new SingleSubscriber<Boolean>() {
                    @Override
                    public void onSuccess(Boolean isAlreadyLocked) {
                        Debug.li(getLogTag(), "newInstallAppPackageName:" + newInstallAppPackageName + "\n"
                                + "isAlreadyLocked:" + isAlreadyLocked + "\n"
                        );
                        if (!isAlreadyLocked) {
                            NotiManager.sendAskLockNewInstalledAppNotification(getApplicationContext(), newInstallAppPackageName);
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        error.printStackTrace();
                        Debug.lw(getLogTag(), error.toString());
                    }
                });
    }

    private void loadUpgradeInfo() {
        final int currentVersionCode = Utils.getVersionCode(this);
        Func1<CloudResult<UpgradeConfig>, Boolean> filter = new Func1<CloudResult<UpgradeConfig>, Boolean>() {
            @Override
            public Boolean call(CloudResult<UpgradeConfig> data) {
                return data != null && data.results != null && !data.results.isEmpty();
            }
        };
        Observable
                .fromCallable(new Callable<CloudResult<UpgradeConfig>>() {
                    @Override
                    public CloudResult<UpgradeConfig> call() throws Exception {
                        return DataUtils.getCacheUpgradeConfig();
                    }
                })
                .filter(filter)
                .switchIfEmpty(
                        WebClient.getLeanCloudService()
                                .loadUpgradeConfig()
                                .doOnNext(new Action1<CloudResult<UpgradeConfig>>() {
                                    @Override
                                    public void call(CloudResult<UpgradeConfig> config) {
                                        DataUtils.cacheUpgradeConfig(config);
                                    }
                                })
                )
                .filter(filter)
                .flatMap(new Func1<CloudResult<UpgradeConfig>, Observable<UpgradeConfig>>() {
                    @Override
                    public Observable<UpgradeConfig> call(CloudResult<UpgradeConfig> data) {
                        return Observable.from(data.results);
                    }
                })
                .filter(new Func1<UpgradeConfig, Boolean>() {
                    @Override
                    public Boolean call(UpgradeConfig config) {
                        return config.versionCode > currentVersionCode;
                    }
                })
                .first()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UpgradeConfig>() {
                    @Override
                    public void call(UpgradeConfig config) {
                        Debug.li(getLogTag(), "弹出升级通知");
                        NotiManager.sendUpgradeAppNotification(getApplicationContext(), config);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Debug.lw(getLogTag(), throwable.getMessage());
                        throwable.printStackTrace();
                    }
                });
    }

    private void refreshReadLockedApps() {
        mLockedApps = PatternLockUtils.getLockedApps(this);
    }

    private void startRefreshTimer() {
        releaseRefreshTimer();
        mTimer = new Timer();
        if (mRefreshTask == null) {
            mRefreshTask = new RefreshTimerTask(this);
        }
        final int period = 500;
        mTimer.scheduleAtFixedRate(mRefreshTask, 0, period);
    }

    private void releaseRefreshTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        mTimer = null;
    }

    private void jumpToLockPage(String lockedAppPackgeName) {
        if (!PatternLockUtils.hasPattern(this)) {
            Debug.li(getLogTag(), "尝试锁定" + lockedAppPackgeName + ",但是密码都没设置，锁定个毛");
            return;
        }
        Context context = getApplicationContext();
        Intent intent = new Intent(context, LockedActivity.class)
                .putExtra(LockedActivity.EXTRA_LOCKED_APP_PACKAGE_NAME, lockedAppPackgeName)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private BroadcastReceiver mScreenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mApkReceiver.onReceive(context, intent);

            String action = intent.getAction();
            if (!TextUtils.isEmpty(action)) {
                if (action.equals(Intent.ACTION_SCREEN_ON)) {
                    Debug.li(getLogTag(), "ACTION_SCREEN_ON");
                } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                    Debug.li(getLogTag(), "ACTION_SCREEN_OFF");
                    mScreenOffStartTime = System.currentTimeMillis();
                    switch (PatternLockUtils.getLockFrequency(context)) {
                        case Const.LockFrequency.SCREEN_OFF: {
                            clearUnlockRecords();
                        }
                        break;
                        case Const.LockFrequency.AFTER_SCREEN_OFF_FOR_3MINS: {
                            needClearUnlockedRecords = true;
                        }
                        break;
                        default:
                            break;
                    }
                }
            }
        }
    };

    private RefreshTimerTask.RefreshCallback mRefreshCallback = new RefreshTimerTask.RefreshCallback() {

        @Override
        public void onTopAppChanged(String preTopAppPkgName, String curTopAppPkgName) {
            Context context = getApplicationContext();
            int lockFrequency = PatternLockUtils.getLockFrequency(context);
            if (lockFrequency == Const.LockFrequency.AFTER_SCREEN_OFF_FOR_3MINS
                    && isElapsedSomeTimeAfterScreenOff()
                    && needClearUnlockedRecords) {
                clearUnlockRecords();
                needClearUnlockedRecords = false;
            }

            boolean isEnterTargetApp = mLockedApps != null && mLockedApps.contains(curTopAppPkgName);

            // If user has unlocked just now
            boolean isEnterTargetAppFromLockPage = Utils.isSelf(context, preTopAppPkgName) && isEnterTargetApp;
            if (isEnterTargetAppFromLockPage) {
                Debug.li(getLogTag(), "User has unlocked just now");
                mUnlockRecordMap.put(curTopAppPkgName, true);
                return;
            }

            boolean isUserLeaveOurApp = mKillWhiteList.contains(preTopAppPkgName)
                    && !Utils.isSelf(context, curTopAppPkgName)
                    && !mKillWhiteList.contains(curTopAppPkgName);
            if (isUserLeaveOurApp && PatternLockUtils.hasPattern(context)) {
                Debug.li(getLogTag(), "User leaves our app");
                LocalBroadcastManager.getInstance(context).sendBroadcast(
                        new Intent(Const.BroadcastAction.CLOSE_ALL_PAGE)
                );
                return;
            }

            // If user enter target app from somewhere
            if (isEnterTargetApp) {
                Debug.li(getLogTag(), "User enter target app from somewhere." + "\n");
                // Check if can lock
                if (isUnlockedBefore(curTopAppPkgName)) {
                    switch (lockFrequency) {
                        case Const.LockFrequency.SCREEN_OFF: {
                            Debug.li(getLogTag(), curTopAppPkgName + "已解锁过一次，屏幕关闭后才会启用锁定");
                        }
                        return;
                        case Const.LockFrequency.AFTER_SCREEN_OFF_FOR_3MINS: {
                            Debug.li(getLogTag(), curTopAppPkgName + "已解锁过一次，屏幕关闭3分钟后才会启用锁定");
                        }
                        return;
                        default:
                            break;
                    }
                }
                // Lock app
                jumpToLockPage(curTopAppPkgName);
                Debug.li(getLogTag(), "Open Lock Page");
            }
        }
    };

    private void clearUnlockRecords() {
        Debug.li(getLogTag(), "clearUnlockRecords");
        mUnlockRecordMap.clear();
    }

    /** 距离上次屏幕开启后是否解锁过一次 */
    private boolean isUnlockedBefore(String packageName) {
        return mUnlockRecordMap.get(packageName) != null
                && mUnlockRecordMap.get(packageName);
    }

    /** 距离上次锁屏是否已超过3分钟 */
    private boolean isElapsedSomeTimeAfterScreenOff() {
        if (mScreenOffStartTime == 0) {
            return false;
        }
        long now = System.currentTimeMillis();
        long interval = now - mScreenOffStartTime;
        boolean passedTime = interval > 3 * 60 * 1000;
        if (!passedTime) {
            Debug.li(getLogTag(), "距离上次锁屏是否已过去了" + Utils.formatMinuteAndSeconds(interval));
        }
        return passedTime;
    }

}