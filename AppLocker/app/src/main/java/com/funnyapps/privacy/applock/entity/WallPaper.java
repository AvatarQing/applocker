package com.funnyapps.privacy.applock.entity;

import android.databinding.ObservableBoolean;

/**
 * Created by lq on 2016/11/19.
 */

public class WallPaper extends BaseCloudItem {

    public String imageUrl;
    public String jumpLink;
    public ObservableBoolean isApplied = new ObservableBoolean(false);

    @Override
    public String toString() {
        return "WallPaper{" +
                "imageUrl='" + imageUrl + '\'' +
                '}';
    }
}