package com.funnyapps.privacy.applock.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;

import com.funnyapps.privacy.applock.MyApplication;
import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.Const;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.SystemBarTintManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public abstract class BaseActivity extends RxActivity {

    protected SystemBarTintManager mBarTintManager;
    private boolean isDestroyed = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBarTintManager = new SystemBarTintManager(this);
        initStatusBarColor();
        registerLocalBroadcast();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        addBtnBackClickListenerIfNeed();
        sendScreenHitAnalyticEvent();
        addStatusBarPadding();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isDestroyed = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        isDestroyed = true;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocalReceiver);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Debug.li(getLogTag(), "requestCode:" + requestCode + "\n"
                + "resultCode:" + resultCode + "\n"
                + "data:" + data);
    }

    protected boolean isActivityDestroyed() {
        return isDestroyed;
    }

    protected void addBtnBackClickListenerIfNeed() {
        View btnBack = $(R.id.btn_back);
        if (btnBack != null) {
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    protected <T extends View> T $(@IdRes int id) {
        return (T) super.findViewById(id);
    }

    protected <T extends View> T $(View view, @IdRes int id) {
        return (T) view.findViewById(id);
    }

    void sendScreenHitAnalyticEvent() {
        Tracker tracker = MyApplication.getInstance().getDefaultTracker();
        tracker.setScreenName(getScreenName());
        if (TextUtils.isEmpty(getScreenName())) {
            return;
        }
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    protected String getScreenName() {
        return null;
    }

    protected void initStatusBarColor() {
        mBarTintManager.setStatusBarTintEnabled(true);
        mBarTintManager.setStatusBarTintColor(getStatusBarColor());
        if (SystemBarTintManager.isMIUI()) {
            SystemBarTintManager.setMiuiStatusBarDarkMode(this, true);
        } else if (SystemBarTintManager.isFlyme()) {
            SystemBarTintManager.setMeizuStatusBarDarkMode(this, true);
        }
    }

    protected int getStatusBarColor() {
        return getResources().getColor(R.color.blue_dark);
    }

    protected void addStatusBarPadding() {
        if (mBarTintManager != null
                && mBarTintManager.isStatusBarAvailable()
                && mBarTintManager.isStatusBarTintEnabled()) {
            View contentView = $(android.R.id.content);
            if (contentView != null) {
                int oldPaddingTop = contentView.getPaddingTop();
                int newPaddingTop = oldPaddingTop + mBarTintManager.getConfig().getStatusBarHeight();
                contentView.setPadding(
                        contentView.getPaddingLeft(),
                        newPaddingTop,
                        contentView.getPaddingRight(),
                        contentView.getPaddingBottom()
                );
            }
        }
    }

    protected void registerLocalBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.BroadcastAction.CLOSE_ALL_PAGE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mLocalReceiver, intentFilter);
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }

    protected BroadcastReceiver mLocalReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TextUtils.isEmpty(action)) {
                return;
            }
            if (TextUtils.equals(action, Const.BroadcastAction.CLOSE_ALL_PAGE)) {
                finish();
            }
        }
    };
}
