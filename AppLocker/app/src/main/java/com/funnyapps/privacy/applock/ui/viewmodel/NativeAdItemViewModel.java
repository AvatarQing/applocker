package com.funnyapps.privacy.applock.ui.viewmodel;

import android.view.View;

/**
 * Created by lq on 2017/1/12.
 */

public class NativeAdItemViewModel {

    public final View nativeAdView;

    public NativeAdItemViewModel(View view) {
        this.nativeAdView = view;
    }
}
