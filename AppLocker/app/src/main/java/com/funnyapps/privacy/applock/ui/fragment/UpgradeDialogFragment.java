package com.funnyapps.privacy.applock.ui.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.funnyapps.privacy.applock.utils.NotiManager;
import com.funnyapps.privacy.applock.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lq on 2016/11/21.
 */

public class UpgradeDialogFragment extends BaseDialogFragment {

    @BindView(R.id.message)
    TextView message;

    Builder mBuilder;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        createContentView(dialog);
        configDialogSize(dialog);
        return dialog;
    }

    private void createContentView(Dialog dialog) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(
                R.layout.dialog_upgrade,
                (ViewGroup) dialog.getWindow().getDecorView(),
                false);
        dialog.setContentView(dialogView);
        ButterKnife.bind(this, dialogView);

        if (mBuilder != null) {
            message.setText(mBuilder.message);
        }
    }

    @OnClick(R.id.btn_cancel)
    void closeDialog() {
        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.MAIN_PAGE,
                Const.Analytic.Action.UPGRADE_APP_LATER
        );

        dismiss();
    }

    @OnClick(R.id.btn_upgrade)
    void upgradeApp() {
        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.MAIN_PAGE,
                Const.Analytic.Action.UPGRADE_APP
        );
        NotiManager.cancelUpgradeNotification(getActivity());
        String url = Const.GOOGLE_PLAY_PREFIX_HTTPS + getActivity().getPackageName();
        Utils.openLink(getActivity(), url);
        dismiss();
    }

    public void setBuilder(Builder builder) {
        mBuilder = builder;
    }

    public static class Builder {
        String message;

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public UpgradeDialogFragment create() {
            UpgradeDialogFragment dialogFragment = new UpgradeDialogFragment();
            dialogFragment.setBuilder(this);
            return dialogFragment;
        }
    }
}