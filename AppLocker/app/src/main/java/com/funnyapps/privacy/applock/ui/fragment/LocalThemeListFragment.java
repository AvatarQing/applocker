package com.funnyapps.privacy.applock.ui.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;
import com.funnyapps.privacy.applock.entity.LocalTheme;
import com.funnyapps.privacy.applock.ui.adapter.LocalThemeListAdapter;
import com.funnyapps.privacy.applock.utils.AnalyticUtils;
import com.funnyapps.privacy.applock.utils.Const;
import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.utils.ThemeUtils;
import com.funnyapps.privacy.applock.utils.ToastUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by AvatarQing on 2016/8/21.
 */
public class LocalThemeListFragment extends BaseFragment {

    @BindView(R.id.grid_view)
    GridView grid_view;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.loading_progress)
    View loading_progress;

    LocalThemeListAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter intentFilter = new IntentFilter(Const.BroadcastAction.THEME_APPLIED);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mLocalReceiver, intentFilter);

        IntentFilter wideIntentFilter = new IntentFilter();
        wideIntentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        wideIntentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        wideIntentFilter.addDataScheme("package");
        getActivity().registerReceiver(mWideReceiver, wideIntentFilter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_theme_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadThemes();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mLocalReceiver);
        getActivity().unregisterReceiver(mWideReceiver);
    }

    void initViews() {
        mAdapter = new LocalThemeListAdapter();
        mAdapter.setAppliedTheme(ThemeUtils.getAppliedThemePackageName(getActivity()));
        grid_view.setAdapter(mAdapter);
        grid_view.setEmptyView(empty_text);
        empty_text.setText(R.string.no_installed_theme);
        mAdapter.setOnThemeClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalTheme item = (LocalTheme) v.getTag(R.id.data);
                showConfirmApplyThemeDialog(item.packageName);
            }
        });
    }

    void loadThemes() {
        showLoadingView();
        mAdapter.clearData();

        final PackageManager pm = getContext().getPackageManager();
        Intent intent = new Intent(Const.THEME_MAIN_ACTIVITY_ACTION);
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (resolveInfos == null) {
            return;
        }
        List<LocalTheme> themeList = new ArrayList<>();
        for (int i = 0; i < resolveInfos.size(); i++) {
            ResolveInfo item = resolveInfos.get(i);
            String themePackageName = item.activityInfo.packageName;
            String themeName = pm.getApplicationLabel(item.activityInfo.applicationInfo).toString();
            Drawable themePreviewImage = getThemePreviewImage(themePackageName);

            LocalTheme theme = new LocalTheme();
            theme.preview = themePreviewImage;
            theme.name = themeName;
            theme.packageName = item.activityInfo.packageName;
            themeList.add(theme);
        }
        Collections.sort(themeList, new Comparator<LocalTheme>() {
            @Override
            public int compare(LocalTheme lhs, LocalTheme rhs) {
                try {
                    PackageInfo lpi = pm.getPackageInfo(lhs.packageName, PackageManager.GET_PERMISSIONS);
                    PackageInfo rpi = pm.getPackageInfo(lhs.packageName, PackageManager.GET_PERMISSIONS);
                    return lpi.firstInstallTime > rpi.firstInstallTime ? 1 : -1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        Debug.li(getLogTag(), "themeList:\n" + themeList);

        mAdapter.appendData(themeList);
        hideLoadingView();
    }

    Drawable getThemePreviewImage(String themePackageName) {
        try {
            PackageManager pm = getContext().getPackageManager();
            Resources r = pm.getResourcesForApplication(themePackageName);
            String resType = "drawable";
            int bgResId = r.getIdentifier(Const.ThemeDrawableName.PREVIEW, resType, themePackageName);
            return r.getDrawable(bgResId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    void showConfirmApplyThemeDialog(final String themePackageName) {
        if (TextUtils.equals(themePackageName, mAdapter.getAppliedTheme())) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.dialog_msg_confirm_use_this_theme)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        applyTheme(themePackageName);
                    }
                })
                .create()
                .show();
    }

    void applyTheme(String themePackageName) {
        ThemeUtils.applyTheme(getActivity(), themePackageName);
        ToastUtils.showToast(R.string.toast_applied_theme);
        mAdapter.setAppliedTheme(themePackageName);
        mAdapter.notifyDataSetChanged();
        LocalBroadcastManager.getInstance(getContext())
                .sendBroadcast(
                        new Intent(Const.BroadcastAction.THEME_APPLIED)
                );
        AnalyticUtils.sendGoogleAnalyticEvent(
                Const.Analytic.Category.THEME,
                Const.Analytic.Action.APPLY_THEME,
                themePackageName
        );
    }

    private void showLoadingView() {
        grid_view.setVisibility(View.GONE);
        empty_text.setVisibility(View.GONE);
        loading_progress.setVisibility(View.VISIBLE);
    }

    private void hideLoadingView() {
        grid_view.setVisibility(View.VISIBLE);
        loading_progress.setVisibility(View.GONE);
    }

    BroadcastReceiver mLocalReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Debug.li(getLogTag(), "action:" + action);
            if (TextUtils.equals(action, Const.BroadcastAction.THEME_APPLIED)) {
                if (mAdapter != null) {
                    mAdapter.setAppliedTheme(ThemeUtils.getAppliedThemePackageName(context));
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    };

    BroadcastReceiver mWideReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Debug.li(getLogTag(), "action:" + action);
            if (TextUtils.equals(action, Intent.ACTION_PACKAGE_ADDED)) {
                loadThemes();
            } else if (TextUtils.equals(action, Intent.ACTION_PACKAGE_REMOVED)) {
                loadThemes();
            }
        }
    };
}