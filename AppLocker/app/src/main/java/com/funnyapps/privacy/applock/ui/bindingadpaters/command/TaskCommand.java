package com.funnyapps.privacy.applock.ui.bindingadpaters.command;

import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Created by lq on 2016/12/20.
 */

public class TaskCommand<T> {

    private Action0 mAction0;
    private Action1<T> mAction1;

    public TaskCommand() {
    }

    public void setTask0(Action0 task) {
        this.mAction0 = task;
    }

    public void setTask1(Action1<T> task) {
        this.mAction1 = task;
    }

    public void execute() {
        if (mAction0 != null) {
            mAction0.call();
        }
    }

    public void execute(T param) {
        if (mAction1 != null) {
            mAction1.call(param);
        }
    }

}