package com.funnyapps.privacy.applock.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.abc.common.tool.Debug;
import com.funnyapps.privacy.applock.service.MonitorService;
import com.funnyapps.privacy.applock.utils.BadgeUtil;
import com.funnyapps.privacy.applock.utils.PatternLockUtils;

/**
 * Created by lq on 2016/11/23.
 */

public class ApkReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            return;
        }
        String packageName = intent.getDataString();
        if (!TextUtils.isEmpty(packageName)) {
            packageName = packageName.replace("package:", "");
        }
        if (TextUtils.equals(action, Intent.ACTION_PACKAGE_ADDED)) {
            Debug.li(getLogTag(), "安装了:" + packageName + "包名的程序");
            if (PatternLockUtils.isAskLockNewInstalledAppEnabled(context)) {
                MonitorService.requestAskLockNewInstalledAppIfNeed(context, packageName);
            }
            BadgeUtil.setBadgeCount(context, 1);
        } else if (TextUtils.equals(action, Intent.ACTION_PACKAGE_REMOVED)) {
            Debug.li(getLogTag(), "卸载了:" + packageName + "包名的程序");
        }
    }

    private String getLogTag() {
        return getClass().getSimpleName();
    }
}