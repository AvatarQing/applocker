package com.funnyapps.privacy.applock.ui.decoration;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by AvatarQing on 2016/9/1.
 */
public class SpacingDecoration extends RecyclerView.ItemDecoration {
    private int mHorizontalSpacing = 0;
    private int mVerticalSpacing = 0;
    private boolean mIncludeEdge = false;

    public SpacingDecoration(int hSpacing, int vSpacing, boolean includeEdge) {
        mHorizontalSpacing = hSpacing;
        mVerticalSpacing = vSpacing;
        mIncludeEdge = includeEdge;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        // Only handle the vertical situation
        int position = parent.getChildAdapterPosition(view);
        if (parent.getLayoutManager() instanceof GridLayoutManager) {
            GridLayoutManager layoutManager = (GridLayoutManager) parent.getLayoutManager();
            GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) view.getLayoutParams();
            int spanCount = layoutManager.getSpanCount();
            int spanIndex = lp.getSpanIndex();
            int spanSize = lp.getSpanSize();
            int rowIndex = layoutManager.getSpanSizeLookup().getSpanGroupIndex(position, spanCount);
            getGridItemOffsets(outRect, spanCount, spanIndex, spanSize, rowIndex);
        } else if (parent.getLayoutManager() instanceof LinearLayoutManager) {
            outRect.left = mHorizontalSpacing;
            outRect.right = mHorizontalSpacing;
            if (mIncludeEdge) {
                if (position == 0) {
                    outRect.top = mVerticalSpacing;
                }
                outRect.bottom = mVerticalSpacing;
            } else {
                if (position > 0) {
                    outRect.top = mVerticalSpacing;
                }
            }
        }
    }

    private void getGridItemOffsets(Rect outRect, int spanCount, int spanIndex, int spanSize, int rowIndex) {
        if (mIncludeEdge) {
            outRect.left = mHorizontalSpacing * (spanCount - spanIndex) / spanCount;
            outRect.right = mHorizontalSpacing * (spanIndex + spanSize) / spanCount;
            if (rowIndex == 0) {
                outRect.top = mVerticalSpacing;
            }
            outRect.bottom = mVerticalSpacing;
        } else {
            outRect.left = mHorizontalSpacing * spanIndex / spanCount;
            outRect.right = mHorizontalSpacing * (spanCount - spanSize - spanIndex) / spanCount;
            if (rowIndex > 0) {
                outRect.top = mVerticalSpacing;
            }
        }
    }
}