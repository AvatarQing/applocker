package com.funnyapps.privacy.applock.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.funnyapps.privacy.applock.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by AvatarQing on 2016/8/14.
 */
public class SetPatternStepView extends LinearLayout {

    @BindView(R.id.step_title)
    TextView step_title;
    @BindView(R.id.step_container)
    LinearLayout step_container;

    View[] mStepCircleViews;
    View[] mStepLineViews;

    int mStepCount = 4;
    int mCurrentStep = 0;

    public SetPatternStepView(Context context) {
        super(context);
        init(null);
    }

    public SetPatternStepView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SetPatternStepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.set_pattern_step, this);
        ButterKnife.bind(this);

        int stepCount = mStepCount;
        int currentStep = mCurrentStep;
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.SetPatternStepView);
            String stepTitle = a.getString(R.styleable.SetPatternStepView_sps_step_title);
            stepCount = a.getInt(R.styleable.SetPatternStepView_sps_step_count, mStepCount);
            currentStep = a.getInt(R.styleable.SetPatternStepView_sps_current_step, mStepCount);
            a.recycle();

            setStepTitle(stepTitle);
        }
        setStepCount(stepCount);
        setCurrentStep(currentStep);
    }

    public void setStepTitle(String title) {
        step_title.setText(title);
    }

    public void setStepTitle(@StringRes int title) {
        step_title.setText(title);
    }

    public void setStepCount(int count) {
        mStepCount = count;
        mCurrentStep = 0;
        updateStepUi();
    }

    void updateStepUi() {
        int stepCount = mStepCount;
        if (stepCount <= 0) {
            return;
        }
        step_container.removeAllViews();
        mStepCircleViews = new View[stepCount];
        mStepLineViews = new View[stepCount - 1];
        LayoutInflater inflater = LayoutInflater.from(getContext());
        for (int i = 0; i < stepCount; i++) {
            TextView stepCircleView = (TextView) inflater.inflate(R.layout.step_circle, step_container, false);
            stepCircleView.setText(String.valueOf(i + 1));
            step_container.addView(stepCircleView);
            mStepCircleViews[i] = stepCircleView;
            if (i < stepCount - 1) {
                View stepLineView = inflater.inflate(R.layout.step_line, step_container, false);
                step_container.addView(stepLineView);
                mStepLineViews[i] = stepLineView;
            }
        }
    }

    /**
     * 设置当前所在的步骤
     *
     * @param stepIndex 当前所在的步骤号，从1开始
     */
    public void setCurrentStep(int stepIndex) {
        mCurrentStep = stepIndex;
        for (int i = 1; i <= mStepCount; i++) {
            mStepCircleViews[i - 1].setSelected(i <= stepIndex);
            if (i <= mStepLineViews.length) {
                mStepLineViews[i - 1].setSelected(i <= stepIndex - 1);
            }
        }
    }

    public int getCurrentStep() {
        return mCurrentStep;
    }
}