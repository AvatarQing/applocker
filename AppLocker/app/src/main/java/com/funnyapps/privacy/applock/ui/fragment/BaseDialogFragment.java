package com.funnyapps.privacy.applock.ui.fragment;

import android.app.Dialog;
import android.content.res.Configuration;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by lq on 2016/11/21.
 */

public class BaseDialogFragment extends DialogFragment {

    public String getLogTag() {
        return getClass().getSimpleName();
    }

    protected void configDialogSize(Dialog dialog) {
        configDialogSize(dialog, 5f / 6f);
    }

    protected void configDialogSize(Dialog dialog, float proportion) {
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER);

        int orientation = getResources().getConfiguration().orientation;
        DisplayMetrics screenMetrics = new DisplayMetrics();
        dialogWindow.getWindowManager().getDefaultDisplay()
                .getMetrics(screenMetrics);
        if (proportion <= 0f) {
            proportion = 5f / 6f;
        }
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // 横屏状态下以高度为基准
            lp.height = (int) (screenMetrics.heightPixels * proportion);
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        } else {
            // 竖屏状态下以宽度为基准
            lp.width = (int) (screenMetrics.widthPixels * proportion);
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        }
        dialogWindow.setAttributes(lp);
    }
}
