package com.funnyapps.privacy.applock.event;

import com.funnyapps.privacy.applock.enums.WallpaperType;

/**
 * Created by lq on 2016/11/22.
 */

public class WallpaperAppliedEvent {
    public String imageUrl;
    public WallpaperType type;

    public WallpaperAppliedEvent(String imageUrl, WallpaperType type) {
        this.imageUrl = imageUrl;
        this.type = type;
    }
}