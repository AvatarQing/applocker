package com.abc.common.entity;

import java.util.List;

/**
 * Created by lq on 2016/11/19.
 */

public class CloudResult<T> {
    public List<T> results;

    @Override
    public String toString() {
        return "CloudResult{" +
                "results=" + results +
                '}';
    }
}