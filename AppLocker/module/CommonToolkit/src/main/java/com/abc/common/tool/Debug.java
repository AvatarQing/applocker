package com.abc.common.tool;

import android.util.Log;

import com.orhanobut.logger.Logger;

/**
 * Created by AvatarQing on 2016/6/1.
 */
public class Debug {
    private static boolean enableLog = BuildConfig.DEBUG;

    public static boolean isLogEnabled() {
        return enableLog;
    }

    public static void enable(boolean enable) {
        enableLog = enable;
    }

    public static void ld(String tag, String message) {
        if (!isLogEnabled()) {
            return;
        }
        Logger.t(tag).d(message);
    }

    public static void li(String tag, String message) {
        if (!isLogEnabled()) {
            return;
        }
        Logger.t(tag).i(message);
    }

    public static void i(String tag, String message) {
        if (!isLogEnabled()) {
            return;
        }
        Log.i(tag, message);
    }

    public static void lw(String tag, String message) {
        if (!isLogEnabled()) {
            return;
        }
        Logger.t(tag).w(message);
    }

    public static void le(String tag, String message) {
        if (!isLogEnabled()) {
            return;
        }
        Logger.t(tag).e(message);
    }

    public static void le(String tag, Throwable throwable, String message) {
        if (!isLogEnabled()) {
            return;
        }
        Logger.t(tag).e(throwable, message);
    }

    public static void json(String tag, String json) {
        if (!isLogEnabled()) {
            return;
        }
        Logger.t(tag).json(json);
    }
}