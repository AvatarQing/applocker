package com.abc.common.tool;

import android.app.Application;
import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

/**
 * Created by AvatarQing on 2016/7/6.
 */
public class ToastUtils {

    private static Toast sLastToast;
    private static Application sApplication;

    public static void init(Application application) {
        sApplication = application;
    }

    private static Application getApplicationContext() {
        return sApplication;
    }

    public static void showToast(@StringRes int info) {
        Context appContext = getApplicationContext();
        showToast(appContext.getString(info));
    }

    public static void showToast(String info) {
        Context appContext = getApplicationContext();
        Toast toast = sLastToast;
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(appContext, info, Toast.LENGTH_LONG);
        toast.show();
        sLastToast = toast;
    }

    public static void hideToast() {
        if (sLastToast != null) {
            sLastToast.cancel();
        }
    }
}