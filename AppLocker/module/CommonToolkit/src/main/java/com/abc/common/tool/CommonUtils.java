package com.abc.common.tool;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.TypedValue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by lq on 2017/1/5.
 */

public class CommonUtils {
    public static int dp2px(Context context, float dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, context.getResources().getDisplayMetrics());
    }

    public static boolean isNetworkConnected(Context context) {
        boolean result = false;
        int ansPermission = context
                .checkCallingOrSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE);
        int internetPermission = context
                .checkCallingOrSelfPermission(Manifest.permission.INTERNET);
        if (ansPermission == PackageManager.PERMISSION_GRANTED
                && internetPermission == PackageManager.PERMISSION_GRANTED) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            if (networkInfo != null) {
                int type = networkInfo.getType();
                switch (type) {
                    case ConnectivityManager.TYPE_MOBILE:
                    case ConnectivityManager.TYPE_WIFI:
                        if (networkInfo.isAvailable()
                                && networkInfo.isConnected()) {
                            result = true;
                        }
                        break;
                }
            }
        }
        return result;
    }

    public static boolean isJsonString(String text) {
        if (!TextUtils.isEmpty(text)) {
            try {
                if (text.startsWith("{")) {
                    new JSONObject(text);
                    return true;
                }
                if (text.startsWith("[")) {
                    new JSONArray(text);
                    return true;
                }
            } catch (JSONException e) {
            }
        }
        return false;
    }

    public static int calculateMemoryCacheSize(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        boolean largeHeap = (context.getApplicationInfo().flags & ApplicationInfo.FLAG_LARGE_HEAP) != 0;
        int memoryClass = am.getMemoryClass();
        if (largeHeap && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            memoryClass = am.getLargeMemoryClass();
        }
        // Target ~15% of the available heap.
        return 1024 * 1024 * memoryClass / 7;
    }

    public static String formatInterval(final long intervalInMills) {
        final long hr = TimeUnit.MILLISECONDS.toHours(intervalInMills);
        final long min = TimeUnit.MILLISECONDS.toMinutes(intervalInMills - TimeUnit.HOURS.toMillis(hr));
        final long sec = TimeUnit.MILLISECONDS.toSeconds(intervalInMills - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
        final long ms = TimeUnit.MILLISECONDS.toMillis(intervalInMills - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min) - TimeUnit.SECONDS.toMillis(sec));
        return String.format(Locale.getDefault(), "%02d:%02d:%02d.%03d", hr, min, sec, ms);
    }
}