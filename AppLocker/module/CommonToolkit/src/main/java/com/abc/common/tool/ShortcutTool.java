package com.abc.common.tool;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;

/**
 * Created by lq on 2017/1/5.
 */

public class ShortcutTool {

    private static final String PREF_NAME_SHORTCUT = "PREF_NAME_SHORTCUT";

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME_SHORTCUT, Context.MODE_PRIVATE);
    }

    private static boolean hasCreatedShortcut(Context context, String key) {
        return getPrefs(context).getBoolean(key, false);
    }

    private static void setHasCreatedShortcut(Context context, String key, boolean created) {
        getPrefs(context).edit().putBoolean(key, created).apply();
    }

    public static void createAppShortcut(Context context, String prefKey, String shortcutName, @DrawableRes int iconResId, Intent launcherIntent) {
        if (!hasCreatedShortcut(context, prefKey)) {
            //创建快捷方式的Intent
            Intent shortcutIntent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
            //不允许重复创建
            shortcutIntent.putExtra("duplicate", false);
            //需要现实的名称
            shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, shortcutName);
            //快捷图片
            Parcelable icon = Intent.ShortcutIconResource.fromContext(context, iconResId);
            shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);
            //点击快捷图片，运行的程序主入口
            shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, launcherIntent);
            //发送广播
            context.sendBroadcast(shortcutIntent);
            // 标记已经创建过一次，以后不再创建，因为无法判断是否已经创建过，所以只在第一次创建
            setHasCreatedShortcut(context, prefKey, true);
            Debug.li(getLogTag(), "createAppShortcut");
        }
    }

    private static String getLogTag() {
        return ShortcutTool.class.getSimpleName();
    }
}
