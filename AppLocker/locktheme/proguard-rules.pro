# 本应用的配置
-dontwarn
-ignorewarnings
-keepattributes *Annotation*
-keepattributes Signature
-keep class com.funny.tools.applocker.theme.entity.** {*;}

#  Google  Play  Services  library
-keep class * extends  java.util.ListResourceBundle {
    protected Object[][] getContents();
}
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}
-keepnames @com.google.android.gms.common.annotation.KeepName class  *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}
-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

# If you are using the Google Mobile Ads SDK, add the following:
# Preserve GMS ads classes
-keep class com.google.android.gms.ads.** { *;}
-dontwarn com.google.android.gms.ads.**