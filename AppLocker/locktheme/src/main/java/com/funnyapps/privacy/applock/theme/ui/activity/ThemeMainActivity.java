package com.funnyapps.privacy.applock.theme.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

import com.funnyapps.privacy.applock.theme.R;
import com.funnyapps.privacy.applock.theme.utils.Const;
import com.funnyapps.privacy.applock.theme.utils.Utils;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ThemeMainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_preview)
    void goToPreviewPage() {
        Intent intent = new Intent(this, ThemePreviewActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_continue)
    void openOrDownloadLockerApp() {
        Intent intent = new Intent(Const.ACTIVITY_ACTION_LOCAL_THEME_LIST)
                .addCategory(Intent.CATEGORY_DEFAULT);
        List<ResolveInfo> resolveInfos = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (resolveInfos != null && !resolveInfos.isEmpty()) {
            startActivity(intent);
            return;
        }

        String packageName = getAppLockerPackageName();
        if (Utils.isAppInstall(this, packageName)) {
            Utils.openApp(this, packageName);
        } else {
            String link = Const.GOOGLE_PLAY_PREFIX_HTTPS + packageName;
            Utils.openLink(this, link);
        }
    }

    String getAppLockerPackageName() {
        return getString(R.string.app_locker_package_name);
    }

}
