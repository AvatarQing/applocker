package com.funnyapps.privacy.applock.theme.ui.activity;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.funnyapps.privacy.applock.theme.R;
import com.funnyapps.privacy.applock.theme.entity.ThemeConfig;
import com.funnyapps.privacy.applock.theme.utils.Const;
import com.funnyapps.privacy.applock.theme.utils.Debug;
import com.haibison.android.lockpattern.widget.LockPatternView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okio.BufferedSource;
import okio.Okio;

/**
 * Created by AvatarQing on 2016/8/22.
 */
public class ThemePreviewActivity extends Activity implements LockPatternView.OnPatternListener {

    @BindView(R.id.root_container)
    ViewGroup root_container;
    @BindView(R.id.lock_pattern)
    LockPatternView lock_pattern;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_preview);
        ButterKnife.bind(this);
        initViews();
        applyTheme();
    }

    private void initViews() {
        lock_pattern.setOnPatternListener(this);
    }

    private void applyTheme() {
        Debug.li(getLogTag(), "applyTheme");
        try {
            AssetManager themeAm = getAssets();
            // 读取主题配置文件中的内容
            String configJson = null;
            try {
                InputStream is = themeAm.open(Const.THEME_CONFIG_FILE_NAME);
                BufferedSource source = Okio.buffer(Okio.source(is));
                configJson = source.readUtf8();
                source.close();
                Debug.i(getLogTag(), "configJson:");
                Debug.json(getLogTag(), "" + configJson);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(configJson)) {
                Debug.li(getLogTag(), "Cannot apply theme, cannot read theme config file");
                return;
            }
            // 文本内容映射为配置类
            ThemeConfig themeConfig = JSON.parseObject(configJson, ThemeConfig.class);
            Debug.li(getLogTag(), "" + themeConfig);
            if (themeConfig == null) {
                return;
            }

            // 根据配置读取主题资源 -------------
            // 背景
            Bitmap bgBitmap = getBitmapFromAsset(themeAm, themeConfig.background);
            int bgColor = Color.parseColor(themeConfig.background_color);

            // 连接线
            int patternPathNormalColor = Color.parseColor(themeConfig.pattern.track_color);
            int patternPathErrorColor = Color.parseColor(themeConfig.pattern.track_color_on_error);

            // 点（内部小圆）
            String touchedButton = null;
            if (!themeConfig.pattern.cell_pressed.isEmpty()) {
                touchedButton = themeConfig.pattern.cell_pressed.get(0).image1;
            }

            // 应用读取出来的主题配置 -------------
            // 背景
            Drawable bgDrawable = new BitmapDrawable(getResources(), bgBitmap);
            root_container.setBackgroundColor(bgColor);
            root_container.setBackgroundDrawable(bgDrawable);
            // 密码盘
            new LockPatternView.ThemeAssetBuilder(themeAm)
                    .setNormalPathColor(patternPathNormalColor)
                    .setWrongPathColor(patternPathErrorColor)
                    .setDefaultButton(themeConfig.pattern.cell_normal_image)
                    .setWrongButton(themeConfig.pattern.cell_image_on_error)
                    .setTouchedButton(touchedButton)
                    .setDefaultCircle(themeConfig.pattern.cell_normal_circle_image)
                    .setCorrectCircle(themeConfig.pattern.cell_pressed_circle_image)
                    .setWrongCircle(themeConfig.pattern.cell_circle_image_on_error)
                    .showCircleDefault(themeConfig.pattern.cell_normal_has_circle)
                    .showCircleCorrect(themeConfig.pattern.cell_pressed_has_circle)
                    .showCircleWrong(themeConfig.pattern.cell_has_circle_on_error)
                    .apply(lock_pattern);
            Debug.li(getLogTag(), "theme applied \n");
        } catch (Exception e) {
            e.printStackTrace();
            Debug.lw(getLogTag(), "" + e);
        }
    }

    private Bitmap getBitmapFromAsset(AssetManager am, String assetName) {
        try {
            InputStream is = am.open(assetName);
            return BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPatternStart() {

    }

    @Override
    public void onPatternCleared() {

    }

    @Override
    public void onPatternCellAdded(List<LockPatternView.Cell> pattern) {

    }

    @Override
    public void onPatternDetected(List<LockPatternView.Cell> pattern) {
        finish();
    }

    public String getLogTag() {
        return getClass().getSimpleName();
    }
}