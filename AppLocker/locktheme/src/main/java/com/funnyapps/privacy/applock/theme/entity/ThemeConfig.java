package com.funnyapps.privacy.applock.theme.entity;

import java.util.List;

/**
 * Created by lq on 2016/8/26.
 */
public class ThemeConfig {

    public String id;
    public int version;
    public String background;
    public String background_color;
    public String text_color;
    public String divider_color;
    public PatternBean pattern;

    public static class PatternBean {
        /** 圆点间连接线正常状态的颜色 */
        public String track_color;
        /** 圆点间连接线错误状态的颜色 */
        public String track_color_on_error;

        /** 内部小点：正常状态 */
        public String cell_normal_image;
        /** 内部小点：错误状态 */
        public String cell_image_on_error;
        /** 内部小点：按下状态 */
        public List<CellPressedBean> cell_pressed;

        /** 外部圆：普通状态 */
        public String cell_normal_circle_image;
        /** 外部圆：按下状态 */
        public String cell_pressed_circle_image;
        /** 外部圆：错误状态 */
        public String cell_circle_image_on_error;

        /** 是否需要绘制普通状态下的外部圆 */
        public boolean cell_normal_has_circle = true;
        /** 是否需要绘制按下状态下的外部圆 */
        public boolean cell_pressed_has_circle = true;
        /** 是否需要绘制错误状态下的外部圆 */
        public boolean cell_has_circle_on_error = true;

        @Override
        public String toString() {
            return "PatternBean{" +
                    "\n \ttrack_color='" + track_color + '\'' +
                    ",\n \ttrack_color_on_error='" + track_color_on_error + '\'' +
                    ",\n \tcell_normal_image='" + cell_normal_image + '\'' +
                    ",\n \tcell_image_on_error='" + cell_image_on_error + '\'' +
                    ",\n \tcell_normal_circle_image='" + cell_normal_circle_image + '\'' +
                    ",\n \tcell_pressed_circle_image='" + cell_pressed_circle_image + '\'' +
                    ",\n \tcell_circle_image_on_error='" + cell_circle_image_on_error + '\'' +
                    ",\n \tcell_normal_has_circle=" + cell_normal_has_circle +
                    ",\n \tcell_pressed_has_circle=" + cell_pressed_has_circle +
                    ",\n \tcell_has_circle_on_error=" + cell_has_circle_on_error +
                    ",\n \tcell_pressed=" + cell_pressed +
                    "\n \t}";
        }

        public static class CellPressedBean {
            /** 图标文件名 */
            public String image1;
            /** 出现的概率，取值0到100，所有概率总和为100 */
            public int prob;

            @Override
            public String toString() {
                return "\n \t\tCellPressedBean{" +
                        "\n \t\t\timage1='" + image1 + '\'' +
                        ",\n \t\t\tprob=" + prob +
                        "\n \t\t}";
            }
        }
    }

    @Override
    public String toString() {
        return "ThemeConfig{" +
                "\n id='" + id + '\'' +
                ",\n version=" + version +
                ",\n background='" + background + '\'' +
                ",\n background_color='" + background_color + '\'' +
                ",\n text_color='" + text_color + '\'' +
                ",\n divider_color='" + divider_color + '\'' +
                ",\n pattern=" + pattern +
                "\n}";
    }
}