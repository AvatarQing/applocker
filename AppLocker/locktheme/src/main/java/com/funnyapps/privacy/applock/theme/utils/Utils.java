package com.funnyapps.privacy.applock.theme.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

/**
 * Created by AvatarQing on 2016/8/21.
 */
public class Utils {

    /** 用第三方程序打开指定链接 */
    public static void openLink(Context context, String link) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(link));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // 判断是否是GooglePlay的链接
        if (link.startsWith(Const.GOOGLE_PLAY_PREFFIX_HTTP)
                || link.startsWith(Const.GOOGLE_PLAY_PREFIX_HTTPS)
                || link.startsWith(Const.GOOGLE_PLAY_PREFFIX_MARKET)) {
            // 如果安装了GooglePlay
            if (isAppInstall(context, Const.GOOGLE_PLAY_PACKAGE_NAME)) {
                // 就用GooglePlay打开链接
                intent.setPackage(Const.GOOGLE_PLAY_PACKAGE_NAME);
            }
        }

        try {
            context.startActivity(intent);
        } catch (Exception e) {
            // 做一个异常捕获，防止没有第三方程序可以打开这个链接
            e.printStackTrace();
        }
    }

    public static void openApp(Context context, String packageName) {
        if (isAppInstall(context, packageName)) {
            try {
                Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /** 判断应用是否已经安装 */
    public static boolean isAppInstall(Context context, String packageName) {
        boolean installed = false;
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, 0);
            if (packageInfo != null) {
                installed = true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return installed;
    }
}