package com.funnyapps.privacy.applock.theme.utils;

/**
 * Created by AvatarQing on 2016/8/21.
 */
public class Const {

    /** 主题配置文件 */
    public static final java.lang.String THEME_CONFIG_FILE_NAME = "index.json";
    public static final String ACTIVITY_ACTION_LOCAL_THEME_LIST = "com.funny.tools.applocker.ui.LOCAL_THEME_LIST";

    /** GooglePlay包名 */
    public static final String GOOGLE_PLAY_PACKAGE_NAME = "com.android.vending";
    /** GooglePlay地址http前缀 */
    public static final String GOOGLE_PLAY_PREFFIX_HTTP = "http://play.google.com/store/apps/details?id=";
    /** GooglePlay地址https前缀 */
    public static final String GOOGLE_PLAY_PREFIX_HTTPS = "https://play.google.com/store/apps/details?id=";
    /** GooglePlay地址market前缀 */
    public static final String GOOGLE_PLAY_PREFFIX_MARKET = "market://details?id=";
}