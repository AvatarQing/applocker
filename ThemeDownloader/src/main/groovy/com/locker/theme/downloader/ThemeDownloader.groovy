package com.locker.theme.downloader

import groovy.json.JsonSlurper
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okio.BufferedSink
import okio.Okio

// 先从网络获取主题列表json
def themeListUrl = "http://cmscdn.ksmobile.net/applock/themes/list-460.json"
OkHttpClient client = new OkHttpClient.Builder().build()
def requestNewUrl = { String url ->
    Request request = new Request.Builder()
            .url(url)
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.154 Safari/537.36 LBBROWSER")
            .build();
    return client.newCall(request).execute()
}
String json = requestNewUrl(themeListUrl).body().string()

// 解析的准备工作
String themeRootPath = [args][0] ?: "D:\\themes"
new File(themeRootPath).mkdirs()

def downloadFile = { Response response, String destPath ->
    File downloadedFile = new File(destPath);
    BufferedSink sink = Okio.buffer(Okio.sink(downloadedFile));
    sink.writeAll(response.body().source());
    sink.close();
}
def ant = new AntBuilder()
def unzip = { srcZip, destDirPath ->
    ant.unzip(src: "$srcZip",
            dest: "$destDirPath",
            overwrite: "true")
    File unusedDir = new File(destDirPath, "__MACOSX")
    ant.delete(dir: "$unusedDir", failonerror: false)
}

// 解析json
def root = new JsonSlurper().parseText(json)
root.t.each {
    String id = it.id
    String previewImageUrl = it.t
    String themeZipUrl = it.p
    String themeEnName = it.n.default
    String themeZhName = it.n."zh-cn" ?: it.n."zh-tw" ?: themeEnName
    String pkgSuffix = it.pkg ? it.pkg.substring(1 + it.pkg.lastIndexOf(".")) : id.replaceAll("-", "")

    File themeDir = new File(themeRootPath, id)
    themeDir.mkdirs()

    // 下载主题图片资源zip文件
    File zipFile = new File(themeDir, "assets/p.zip")
    zipFile.parentFile.mkdirs()
    Response zipResponse = requestNewUrl(themeZipUrl)
    downloadFile(zipResponse, zipFile.absolutePath)
    unzip(zipFile, zipFile.parent)
    zipFile.delete()
    sleep(200)

    // 下载预览图
    File previewImageFile = new File(themeDir, "res/drawable-hdpi/preview.png")
    previewImageFile.parentFile.mkdirs()
    Response previewImageResponse = requestNewUrl(previewImageUrl)
    downloadFile(previewImageResponse, previewImageFile.absolutePath)
    sleep(200)

    // 主题包信息
    println "write ${id}'s appinfo.txt.  themeZhName:${themeZhName}"
    Properties appInfo = new Properties()
    appInfo["themeId"] = id
    appInfo["packageName"] = "com.lock.apps.privacy.theme." + pkgSuffix
    appInfo["versionCode"] = "1"
    appInfo["versionName"] = "1.0"
    appInfo["appName_zh"] = themeZhName
    appInfo["appName_en"] = themeEnName
    appInfo["appLockerPackageName"] = "com.lock.apps.privacy.free"
    File appInfoFile = new File(themeDir, "configs/appinfo.txt")
    appInfoFile.parentFile.mkdirs()
    appInfo.store(new FileWriter(appInfoFile), null)

    // 每个请求停顿一段时间，以免被禁
    sleep(500)
}
