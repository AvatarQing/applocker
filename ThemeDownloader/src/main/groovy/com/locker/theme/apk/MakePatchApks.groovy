package com.locker.theme.apk

import okio.BufferedSource
import okio.Okio

File projectDir = new File([args][0] ?: "G:\\Develop\\AsProjects\\AppLocker")
File themesDir = new File(projectDir, "themes")
File replacementDir = new File(projectDir, "ThemeReplacement")
File assembleDir = new File(projectDir, "AppLocker\\locktheme")
def ant = new AntBuilder()
themesDir.eachDir {
    println "-------- ${it.name} -----------"
    println "copy ${it}"
    println "to ${replacementDir}"

    ant.delete(dir: "${new File(replacementDir, "assets")}", failonerror: false)
    ant.copydir(
            src: it,
            dest: replacementDir,
            forceoverwrite: true,
            excludes: "*.apk"
    )

    // copy configs
    ant.copydir(
            src: new File(replacementDir, "configs"),
            dest: new File(assembleDir, "configs"),
            forceoverwrite: true,
    )

    // copy assets
    ant.delete(dir: "${new File(assembleDir, "src\\main\\assets")}", failonerror: false)
    ant.copydir(
            src: new File(replacementDir, "assets"),
            dest: new File(assembleDir, "src\\main\\assets"),
            forceoverwrite: true,
    )

    // copy res
    ant.copydir(
            src: new File(replacementDir, "res"),
            dest: new File(assembleDir, "src\\release\\res"),
            forceoverwrite: true,
    )

    // Generate apk
    Process p = "cmd /c gradle assembleRelease".execute(null, assembleDir)
    p.consumeProcessErrorStream(System.err)
    BufferedSource source = Okio.buffer(Okio.source(p.in))
    String line
    while ((line = source.readUtf8Line()) != null) {
        println line
    }

    // copy apk
    File appInfoFile = new File(it, "configs\\appinfo.txt")
    Properties appInfo = new Properties()
    appInfo.load(new FileReader(appInfoFile))
    String themeId = appInfo["themeId"]
    ant.copydir(
            src: new File(assembleDir, "build\\outputs\\apk"),
            dest: it,
            includes: "${themeId}*release.apk",
            forceoverwrite: true,
    )
}

// copy res
ant.copydir(
        src: new File(assembleDir, "build\\outputs\\apk"),
        dest: new File(projectDir, "FinalThemeApks"),
        forceoverwrite: true,
        includes: "*release.apk"
)