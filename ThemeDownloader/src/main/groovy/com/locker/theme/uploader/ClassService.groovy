package com.locker.theme.uploader

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
/**
 * Created by lq on 2016/8/29.
 */
interface ClassService {
    @Headers("Content-Type: application/json")
    @POST("classes/theme")
    Call<ObjectResult> addTheme(@Body Theme theme);

    @Headers("Content-Type: application/json")
    @POST("classes/theme")
    Call<String> linkPreviewImage(@Body LinkFileBody body);
}