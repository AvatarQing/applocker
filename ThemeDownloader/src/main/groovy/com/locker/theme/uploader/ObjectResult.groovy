package com.locker.theme.uploader

/**
 * Created by lq on 2016/8/29.
 */
class ObjectResult {
    String createdAt
    String updatedAt
    String objectId

    @Override
    public String toString() {
        return """\
ObjectResult{
    createdAt='$createdAt',
    createdAt='$updatedAt',
    objectId='$objectId'
}"""
    }
}
