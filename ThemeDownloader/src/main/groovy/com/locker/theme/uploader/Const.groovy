package com.locker.theme.uploader

/**
 * Created by lq on 2016/8/29.
 */
class Const {
    static final String APP_ID = "3qOqg7poBCNYpoO74ppm0g65-gzGzoHsz"
    static final String APP_KEY = "pDmgld1fMzBt51sNJvxanJLX"
    static final String BASE_URL = "https://api.leancloud.cn/1.1/"
    static final String BASE_CLASS_URL = BASE_URL + "classes/"

    class Header {
        static final String LC_ID = "X-LC-Id"
        static final String LC_KEY = "X-LC-Key"
        static final String CONTENT_TYPE = "Content-Type"
    }

    class ContentType {
        static final String JSON = "application/json"
    }

    class Table {
        static final String THEME = "theme"
    }
}
