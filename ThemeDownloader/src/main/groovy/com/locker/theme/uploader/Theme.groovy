package com.locker.theme.uploader

/**
 * Created by lq on 2016/8/29.
 */
class Theme {
    String themeId
    String themeNameEn
    String themeNameZh
    String packageName

    @Override
    public String toString() {
        return "Theme{" +
                "themeId='" + themeId + '\'' +
                ", themeNameEn='" + themeNameEn + '\'' +
                ", themeNameZh='" + themeNameZh + '\'' +
                ", packageName='" + packageName + '\'' +
                '}';
    }
}
