package com.locker.theme.uploader

import com.google.gson.Gson
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// 初始化网络工具
Interceptor headerInterceptor = new Interceptor() {
    @Override
    okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request()
        request = request.newBuilder()
                .header(Const.Header.LC_ID, Const.APP_ID)
                .header(Const.Header.LC_KEY, Const.APP_KEY)
                .build()
        okhttp3.Response response = chain.proceed(request)
        println "intercept url:" + response.request().url()
        return response
    }
}
OkHttpClient client = new OkHttpClient.Builder()
        .addNetworkInterceptor(headerInterceptor)
        .build()
Retrofit classRetrofit = new Retrofit.Builder()
        .baseUrl(Const.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()
ClassService classService = classRetrofit.create(ClassService.class)

// 关联图片到对象
String themeObjectId = "57c4429f6be3ff005843f0f2"
String imageFileObjectId = "57c43cd60a2b58006cfffb0c"
LinkFileBody linkImageBody = new LinkFileBody()
linkImageBody.preview = new LinkFileBody.Picture()
linkImageBody.preview.__type = "File"
linkImageBody.preview.id = imageFileObjectId
Gson gson = new Gson()
String linkImageBodyString = gson.toJson(linkImageBody)
println linkImageBodyString
RequestBody body = RequestBody.create(MediaType.parse("application/json"), linkImageBodyString);
Request request = new Request.Builder()
        .url("${Const.BASE_CLASS_URL}theme/${themeObjectId}")
        .addHeader(Const.Header.CONTENT_TYPE, Const.ContentType.JSON)
        .put(body)
        .build()
okhttp3.Response response = client.newCall(request).execute()
println response.body().string()

//Response<String> linkImageResult = classService.linkPreviewImage(linkImageBody).execute()
//println "linkImageBodyString:${linkImageBodyString}"
//println "linkImageResult:${linkImageResult.body()}"
//println "link preview image error:${linkImageResult.errorBody()?.string()}"