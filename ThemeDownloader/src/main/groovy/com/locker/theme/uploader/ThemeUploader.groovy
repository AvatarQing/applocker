package com.locker.theme.uploader

import com.google.gson.Gson
import okhttp3.*
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// 初始化网络工具
Interceptor headerInterceptor = new Interceptor() {
    @Override
    okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request()
        request = request.newBuilder()
                .header(Const.Header.LC_ID, Const.APP_ID)
                .header(Const.Header.LC_KEY, Const.APP_KEY)
                .build()
        okhttp3.Response response = chain.proceed(request)
        println "intercept url:" + response.request().url()
        return response
    }
}
OkHttpClient client = new OkHttpClient.Builder()
        .addNetworkInterceptor(headerInterceptor)
        .build()
Retrofit classRetrofit = new Retrofit.Builder()
        .baseUrl(Const.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()
ClassService classService = classRetrofit.create(ClassService.class)
Gson gson = new Gson()
def uploadFile = { String contentType, File file, String newFileName ->
    RequestBody formBody = new MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("file", newFileName, RequestBody.create(MediaType.parse(contentType), file))
            .build();
    Request request = new Request.Builder().url("${Const.BASE_URL}files/${newFileName}").post(formBody).build();
    okhttp3.Response response = client.newCall(request).execute();
    String resultJson = response.body().string()
    FileResult result = gson.fromJson(resultJson, FileResult.class)
    return result
}
def uploadImageFile = uploadFile.curry("image/png")
def uploadApkFile = uploadFile.curry("application/vnd.android.package-archive")

def linkFileToTheme = { String fieldName, String themeObjectId, String imageFileObjectId ->
    String jsonBody = "{\"${fieldName}\":{\"id\":\"${imageFileObjectId}\",\"__type\":\"File\"}}"
    RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonBody);
    Request request = new Request.Builder()
            .url("${Const.BASE_CLASS_URL}theme/${themeObjectId}")
            .addHeader(Const.Header.CONTENT_TYPE, Const.ContentType.JSON)
            .put(requestBody)
            .build()
    okhttp3.Response response = client.newCall(request).execute()
    String resultJson = response.body().string()

    ObjectResult result = gson.fromJson(resultJson, ObjectResult.class)
    return result
}

def linkPreviewToTheme = linkFileToTheme.curry("preview")
def linkApkToTheme = linkFileToTheme.curry("apk")

// 遍历主题目录
File themesDir = new File(([args][0]) ?: "G:\\Develop\\AsProjects\\AppLocker\\themes")
themesDir.eachDir {
    println '*' * 20
    File appinfoFile = new File(it, "configs\\appinfo.txt")
    Properties appinfoProp = new Properties()
    appinfoProp.load(new FileReader(appinfoFile))

    Theme theme = new Theme()
    theme.themeId = appinfoProp.themeId
    theme.themeNameEn = appinfoProp.appName_en
    theme.themeNameZh = appinfoProp.appName_zh
    theme.packageName = appinfoProp.packageName
    println theme

    // 创建对象
    Response<ObjectResult> response = classService.addTheme(theme) execute()
    println "create object error:${response.errorBody()?.string()}"
    ObjectResult themeResult = response.body()
    println "${theme.themeId} created. ${themeResult}"

    // 上传预览图
    File imageFile = new File(it, "res\\drawable-hdpi\\preview.png")
    String newImageName = "${theme.themeId}_${imageFile.name}"
    FileResult previewFileResult = uploadImageFile(imageFile, newImageName)
    println "Preview image file uploaded. ${previewFileResult}"
    sleep(2000)

    // 关联图片到对象
    ObjectResult linkPreviewResult = linkPreviewToTheme(themeResult.objectId, previewFileResult.objectId)
    println "linkPreviewResult:${linkPreviewResult}"

    // 上传apk
    File apkFile = new File(it, "${theme.themeId}_1_1.0_release.apk")
    FileResult apkFileResult = uploadApkFile(apkFile, apkFile.name)
    println "Apk file uploaded. ${apkFileResult}"
    sleep(2000)

    // 关联图片到对象
    ObjectResult linkApkResult = linkApkToTheme(themeResult.objectId, apkFileResult.objectId)
    println "linkApkResult:${linkApkResult}"
}
/**
 通过 REST API 上传文件受到三个限制：
 上传最大文件大小有 10 M 的限制
 每个应用每秒最多上传 1 个文件
 每个应用每分钟最多上传 30 个文件
 */