package com.locker.theme.uploader

/**
 * Created by lq on 2016/8/29.
 */
class FileResult extends ObjectResult {
    int size
    String bucket
    String url
    String name

    @Override
    public String toString() {
        return """\
FileResult{
    size=$size,
    bucket='$bucket',
    url='$url',
    name='$name',
    super=${super.toString()}
}"""
    }
}
